package com.fintech.pangu.authentication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 统一认证服务
 *
 * @author Trust_FreeDom
 */
@SpringBootApplication
public class PanGuUnifiedAuthenticationServer {
    
    public static void main(String[] args) {
        SpringApplication.run(PanGuUnifiedAuthenticationServer.class, args);
    }

}
