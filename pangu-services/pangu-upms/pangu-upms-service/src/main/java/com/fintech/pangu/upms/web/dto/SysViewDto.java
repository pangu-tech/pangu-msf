package com.fintech.pangu.upms.web.dto;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class SysViewDto {

    private Long id;

    private Long parentId;

    private String viewName;

    private String viewType;

    private String viewIcon;

    private String viewPath;

    private String viewComponent;

    private Integer viewOrder;

    private Long permissionId;
}
