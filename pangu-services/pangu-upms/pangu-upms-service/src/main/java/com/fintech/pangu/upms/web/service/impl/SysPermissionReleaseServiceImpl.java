package com.fintech.pangu.upms.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dao.SysPermissionReleaseDao;
import com.fintech.pangu.upms.web.dto.SysPermissionReleaseDto;
import com.fintech.pangu.upms.web.entity.SysPermissionRelease;
import com.fintech.pangu.upms.web.mapper.PermissionReleaseMapper;
import com.fintech.pangu.upms.web.service.SysPermissionReleaseService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 权限发布 服务实现类
 * </p>
 *
 * @author Admin
 * @since 2019-11-04
 */
@Service
public class SysPermissionReleaseServiceImpl implements SysPermissionReleaseService {

    @Autowired
    private PermissionReleaseMapper permissionReleaseMapper;

    @Autowired
    private SysPermissionReleaseDao sysPermissionReleaseDao;

    @Override
    public IPage<SysPermissionReleaseDto> listForPage(SysPermissionReleaseDto sysPermissionReleaseDto) {
        // 构建查询条件
        QueryWrapper<SysPermissionRelease> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(StringUtils.isNotEmpty(sysPermissionReleaseDto.getPublisher())
                        ,SysPermissionRelease::getPublisher
                        ,sysPermissionReleaseDto.getPublisher())
                .between(StringUtils.isNotEmpty(sysPermissionReleaseDto.getReleaseStartTime())
                        &&StringUtils.isNotEmpty(sysPermissionReleaseDto.getReleaseEndTime())
                        ,SysPermissionRelease::getReleaseTime
                        ,sysPermissionReleaseDto.getReleaseStartTime()
                        ,sysPermissionReleaseDto.getReleaseEndTime());

        // 分页查询
        Page page = new Page(sysPermissionReleaseDto.getCurrent(),sysPermissionReleaseDto.getSize());
        IPage<SysPermissionRelease> permissionReleasePage = sysPermissionReleaseDao.selectPage(page,queryWrapper);

        // 结果转换 po --> dto
        List<SysPermissionReleaseDto> permissionReleaseList =  permissionReleaseMapper.po2DtoList(permissionReleasePage.getRecords());

        // 构建返回
        IPage<SysPermissionReleaseDto> permissionReleases = PageUtil.builder(permissionReleaseList,permissionReleasePage);
        return permissionReleases;
    }

    @Override
    public void add(SysPermissionReleaseDto sysPermissionReleaseDto) {
        SysPermissionRelease sysPermissionRelease = permissionReleaseMapper.dto2Po(sysPermissionReleaseDto);
        sysPermissionReleaseDao.insert(sysPermissionRelease);
    }
}
