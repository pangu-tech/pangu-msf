package com.fintech.pangu.upms.web.service;

import com.fintech.pangu.upms.web.dto.SysRolePermissionDto;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
public interface SysRolePermissionService{

    void addRolePermission(SysRolePermissionDto sysRolePermissionDto);
}
