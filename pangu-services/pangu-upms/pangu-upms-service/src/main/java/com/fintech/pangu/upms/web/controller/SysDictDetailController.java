package com.fintech.pangu.upms.web.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateAndModifyDictDetailRequest;
import com.fintech.pangu.api.bean.qo.DeleteDictDetailRequest;
import com.fintech.pangu.api.bean.qo.FetchDictDetailRequest;
import com.fintech.pangu.api.bean.vo.FetchDictDetailResult;
import com.fintech.pangu.api.interfaces.AuthSysDictDetail;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dto.SysDictDetailDto;
import com.fintech.pangu.upms.web.mapper.DictDetailMapper;
import com.fintech.pangu.upms.web.service.SysDictDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 数据字典详情表 控制器
 * </p>
 *
 * @author Admin
 * @since 2019-10-30
 */
@RestController
public class SysDictDetailController implements AuthSysDictDetail {

    @Autowired
    private DictDetailMapper dictDetailMapper;

    @Autowired
    private SysDictDetailService sysDictDetailService;

    @Override
    public BaseResponse<IPage<FetchDictDetailResult>> listForPage(FetchDictDetailRequest fetchDictDetailRequest) {
        // req --> dto
        SysDictDetailDto sysDictDetailDto = dictDetailMapper.fetchReq2Dto(fetchDictDetailRequest);
        // 分页查询
        IPage<SysDictDetailDto> dictDetailPage = sysDictDetailService.listForPage(sysDictDetailDto);
        // dto --> result
        List<FetchDictDetailResult> dictDetailList = dictDetailMapper.dto2ResultList(dictDetailPage.getRecords());
        // 构建返回
        IPage<FetchDictDetailResult> dictDetails = PageUtil.builder(dictDetailList,dictDetailPage);

        return new BaseResponse<>(dictDetails);
    }

    @Override
    public BaseResponse<Void> add(@RequestBody CreateAndModifyDictDetailRequest createAndModifyDictDetailRequest) {
        SysDictDetailDto sysDictDetailDto = dictDetailMapper.createAndModify2Dto(createAndModifyDictDetailRequest);
        sysDictDetailService.add(sysDictDetailDto);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> update(@PathVariable Long id, @RequestBody CreateAndModifyDictDetailRequest createAndModifyDictDetailRequest) {
        SysDictDetailDto sysDictDetailDto = dictDetailMapper.createAndModify2Dto(createAndModifyDictDetailRequest);
        sysDictDetailDto.setId(id);
        sysDictDetailService.update(sysDictDetailDto);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> delete(@PathVariable Long id) {
        sysDictDetailService.delete(id);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> deleteBatch(@RequestBody DeleteDictDetailRequest deleteDictDetailRequest) {
        SysDictDetailDto sysDictDetailDto = dictDetailMapper.deleteReq2Dto(deleteDictDetailRequest);
        sysDictDetailService.deleteBatch(sysDictDetailDto);
        return BaseResponse.success();
    }
}
