package com.fintech.pangu.upms.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dao.SysPermissionDao;
import com.fintech.pangu.upms.web.dao.SysTenantDao;
import com.fintech.pangu.upms.web.dao.SysViewDao;
import com.fintech.pangu.upms.web.dto.SysTenantDto;
import com.fintech.pangu.upms.web.entity.SysPermission;
import com.fintech.pangu.upms.web.entity.SysTenant;
import com.fintech.pangu.upms.web.entity.SysView;
import com.fintech.pangu.upms.web.mapper.TenantMapper;
import com.fintech.pangu.upms.web.service.SysTenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 系统租户表 服务实现类
 * </p>
 *
 * @author Admin
 * @since 2019-10-17
 */
@Service
public class SysTenantServiceImpl implements SysTenantService {

    @Autowired
    private TenantMapper tenantMapper;

    @Autowired
    private SysTenantDao sysTenantDao;

    @Autowired
    private SysPermissionDao sysPermissionDao;

    @Autowired
    private SysViewDao sysViewDao;

    @Override
    public IPage<SysTenantDto> listForPage(SysTenantDto sysTenantDto) {
        // 构建查询条件
        QueryWrapper<SysTenant> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(!sysTenantDto.getTenantName().isEmpty(),SysTenant::getTenantName,sysTenantDto.getTenantName())
                .eq(!sysTenantDto.getTenantId().isEmpty(),SysTenant::getTenantId,sysTenantDto.getTenantId());

        // 分页查询
        Page page = new Page(sysTenantDto.getCurrent(),sysTenantDto.getSize());
        IPage<SysTenant> tenantPage = sysTenantDao.selectPage(page,queryWrapper);

        // 结果转换 po --> dto
        List<SysTenantDto> tenantList =  tenantMapper.po2DtoList(tenantPage.getRecords());

        // 构建返回
        IPage<SysTenantDto> tenants = PageUtil.builder(tenantList,tenantPage);

        return tenants;
    }

    @Override
    public List<SysTenantDto> listForUserTenants(SysTenantDto sysTenantDto) {
        List<SysTenant> userTenantList = sysTenantDao.listForUserTenants(sysTenantDto.getUserId());
        List<SysTenantDto> userTenantDtos = tenantMapper.po2DtoList(userTenantList);
        return userTenantDtos;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SysTenantDto sysTenantDto) {
        SysTenant sysTenant = tenantMapper.dto2Po(sysTenantDto);
        sysTenantDao.insert(sysTenant);

        SysPermission sysPermission = new SysPermission();
        sysPermission.setPermissionName(sysTenant.getTenantName());
        sysPermission.setPermissionType("root");
        sysPermission.setParentId(0L);
        sysPermission.setTenantId(sysTenant.getTenantId());
        sysPermissionDao.insert(sysPermission);

        SysView sysView = new SysView();
        sysView.setParentId(0L);
        sysView.setPermissionId(sysPermission.getId());
        sysView.setViewType("root");
        sysView.setViewName(sysTenant.getTenantName());
        sysView.setTenantId(sysTenant.getTenantId());
        sysView.setViewOrder(1);
        sysViewDao.insert(sysView);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(SysTenantDto sysTenantDto) {
        SysTenant sysTenant = tenantMapper.dto2Po(sysTenantDto);
        sysTenantDao.updateById(sysTenant);

        SysPermission sysPermission = new SysPermission();
        sysPermission.setPermissionName(sysTenant.getTenantName());

        sysPermissionDao.update(sysPermission,new UpdateWrapper<SysPermission>()
                .eq("TENANT_ID",sysTenant.getTenantId())
                .eq("PERMISSION_TYPE","root"));

        SysView sysView = new SysView();
        sysView.setViewName(sysTenant.getTenantName());

        sysViewDao.update(sysView,new UpdateWrapper<SysView>()
                .eq("TENANT_ID",sysTenant.getTenantId())
                .eq("VIEW_TYPE","root"));
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(Long id) {
        sysTenantDao.deleteById(id);
    }
}
