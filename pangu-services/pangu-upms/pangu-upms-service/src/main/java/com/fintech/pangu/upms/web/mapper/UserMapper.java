package com.fintech.pangu.upms.web.mapper;

import com.fintech.pangu.api.bean.qo.FetchUserRequest;
import com.fintech.pangu.api.bean.vo.FetchUserResult;
import com.fintech.pangu.upms.web.dto.SysUserDto;
import com.fintech.pangu.upms.web.entity.SysUser;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel="spring")
public interface UserMapper {

    SysUserDto fetchReq2Dto(FetchUserRequest fetchUserRequest);

    List<SysUserDto> po2Dto(List<SysUser> sysUserList);

    List<FetchUserResult> dto2Result(List<SysUserDto> sysUserDtoList);
}
