package com.fintech.pangu.upms.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fintech.pangu.commons.entity.PanguBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.sql.Timestamp;

/**
 * <p>
 * 权限发布
 * </p>
 *
 * @author Admin
 * @since 2019-11-04
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_permission_release")
public class SysPermissionRelease extends PanguBaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("PUBLISHER")
    private String publisher;

    @TableField("DESCRIPTION")
    private String description;

    @TableField("RELEASE_TIME")
    private Timestamp releaseTime;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    private String createdBy;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    private String updatedBy;


}
