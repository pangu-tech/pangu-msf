package com.fintech.pangu.upms.web.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateAndModifyRoleRequest;
import com.fintech.pangu.api.bean.qo.FetchRoleRequest;
import com.fintech.pangu.api.bean.vo.FetchRoleResult;
import com.fintech.pangu.api.interfaces.AuthSysRole;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dto.SysRoleDto;
import com.fintech.pangu.upms.web.mapper.RoleMapper;
import com.fintech.pangu.upms.web.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制器
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
@RestController
public class SysRoleController implements AuthSysRole {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private SysRoleService sysRoleService;

    @Override
    public BaseResponse<FetchRoleResult> roleForId(@PathVariable Long id) {
        SysRoleDto sysRoleDto = sysRoleService.roleForId(id);
        // dto --> result
        FetchRoleResult fetchRoleResult = roleMapper.dto2Result(sysRoleDto);
        return new BaseResponse<>(fetchRoleResult);
    }

    @Override
    public BaseResponse<IPage<FetchRoleResult>> listForPage(FetchRoleRequest fetchRoleRequest) {
        // req --> dto
        SysRoleDto sysRoleDto = roleMapper.fetchReq2Dto(fetchRoleRequest);
        // 分页查询
        IPage<SysRoleDto> rolePage = sysRoleService.listForPage(sysRoleDto);
        // dto --> result
        List<FetchRoleResult> roleList = roleMapper.dto2ResultList(rolePage.getRecords());
        // 构建返回
        IPage<FetchRoleResult> roles = PageUtil.builder(roleList,rolePage);

        return new BaseResponse<>(roles);
    }

    @Override
    public BaseResponse<Void> add(@RequestBody CreateAndModifyRoleRequest createAndModifyRoleRequest) {
        // req --> dto
        SysRoleDto sysRoleDto = roleMapper.createAndModify2Dto(createAndModifyRoleRequest);
        // 新增
        sysRoleService.add(sysRoleDto);

        return  BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> update(@PathVariable Long id, @RequestBody CreateAndModifyRoleRequest createAndModifyRoleRequest) {
        // req --> dto
        SysRoleDto sysRoleDto = roleMapper.createAndModify2Dto(createAndModifyRoleRequest);
        sysRoleDto.setId(id);
        sysRoleService.update(sysRoleDto);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> delete(@PathVariable Long id) {
        sysRoleService.delete(id);
        return BaseResponse.success();
    }
}
