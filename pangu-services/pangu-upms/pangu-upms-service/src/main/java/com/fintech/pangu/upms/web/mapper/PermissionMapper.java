package com.fintech.pangu.upms.web.mapper;

import com.fintech.pangu.api.bean.qo.CreateAndModifyPermissionRequest;
import com.fintech.pangu.api.bean.vo.FetchPermissionResult;
import com.fintech.pangu.upms.web.dto.SysPermissionDto;
import com.fintech.pangu.upms.web.entity.SysPermission;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Description:
 */
@Mapper(componentModel="spring")
public interface PermissionMapper {

    SysPermissionDto createAndModify2Dto(CreateAndModifyPermissionRequest createAndModifyPermissionRequest);

    SysPermission dto2Po(SysPermissionDto sysPermissionDto);

    List<SysPermissionDto> po2DtoList(List<SysPermission> sysPermissionList);

    List<FetchPermissionResult> dto2ResultList(List<SysPermissionDto> sysPermissionDtos);
}
