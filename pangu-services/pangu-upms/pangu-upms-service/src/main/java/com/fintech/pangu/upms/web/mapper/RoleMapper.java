package com.fintech.pangu.upms.web.mapper;

import com.fintech.pangu.api.bean.qo.CreateAndModifyRoleRequest;
import com.fintech.pangu.api.bean.qo.FetchRoleRequest;
import com.fintech.pangu.api.bean.vo.FetchRoleResult;
import com.fintech.pangu.upms.web.dto.SysRoleDto;
import com.fintech.pangu.upms.web.entity.SysRole;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Description:
 */
@Mapper(componentModel="spring")
public interface RoleMapper {

    SysRoleDto fetchReq2Dto(FetchRoleRequest fetchRoleRequest);

    SysRoleDto createAndModify2Dto(CreateAndModifyRoleRequest createAndModifyRoleRequest);

    SysRole dto2Po(SysRoleDto sysRoleDto);

    SysRoleDto po2Dto(SysRole sysRole);

    List<SysRoleDto> po2DtoList(List<SysRole> sysRoleList);

    FetchRoleResult dto2Result(SysRoleDto sysRoleDto);

    List<FetchRoleResult> dto2ResultList(List<SysRoleDto> sysRoleDtoList);
}
