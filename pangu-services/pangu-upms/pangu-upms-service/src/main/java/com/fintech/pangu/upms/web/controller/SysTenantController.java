package com.fintech.pangu.upms.web.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateAndModifyTenantRequest;
import com.fintech.pangu.api.bean.qo.FetchTenantRequest;
import com.fintech.pangu.api.bean.vo.FetchTenantResult;
import com.fintech.pangu.api.interfaces.AuthSysTenant;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dto.SysTenantDto;
import com.fintech.pangu.upms.web.mapper.TenantMapper;
import com.fintech.pangu.upms.web.service.SysTenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 系统租户表 控制器
 * </p>
 *
 * @author Admin
 * @since 2019-10-17
 */
@RestController
public class SysTenantController implements AuthSysTenant {

    @Autowired
    private TenantMapper tenantMapper;

    @Autowired
    private SysTenantService sysTenantService;

    @Override
    public BaseResponse<IPage<FetchTenantResult>> listForPage(FetchTenantRequest fetchTenantRequest) {
        SysTenantDto sysTenantDto = tenantMapper.fetchReq2Dto(fetchTenantRequest);
        IPage<SysTenantDto> tenantPage = sysTenantService.listForPage(sysTenantDto);
        List<FetchTenantResult> tenantList = tenantMapper.dto2ResultList(tenantPage.getRecords());
        IPage<FetchTenantResult> tenants = PageUtil.builder(tenantList,tenantPage);
        return new BaseResponse<>(tenants);
    }

    @Override
    public BaseResponse<List<FetchTenantResult>> listForUserTenant(FetchTenantRequest fetchTenantRequest) {
        SysTenantDto sysTenantDto = tenantMapper.fetchReq2Dto(fetchTenantRequest);
        List<SysTenantDto> userTenantList = sysTenantService.listForUserTenants(sysTenantDto);
        List<FetchTenantResult> userTenants = tenantMapper.dto2ResultList(userTenantList);
        return new BaseResponse<>(userTenants);
    }

    @Override
    public BaseResponse<Void> add(@RequestBody CreateAndModifyTenantRequest createAndModifyTenantRequest) {
        SysTenantDto sysTenantDto = tenantMapper.createAndModify2Dto(createAndModifyTenantRequest);
        sysTenantService.add(sysTenantDto);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> update(@PathVariable Long id, @RequestBody CreateAndModifyTenantRequest createAndModifyTenantRequest) {
        SysTenantDto sysTenantDto = tenantMapper.createAndModify2Dto(createAndModifyTenantRequest);
        sysTenantDto.setId(id);
        sysTenantService.update(sysTenantDto);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> delete(@PathVariable Long id) {
        sysTenantService.delete(id);
        return BaseResponse.success();
    }
}
