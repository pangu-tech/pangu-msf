package com.fintech.pangu.upms.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fintech.pangu.commons.entity.PanguBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 视图表
 * </p>
 *
 * @author Admin
 * @since 2019-10-22
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_view")
public class SysView extends PanguBaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("PERMISSION_ID")
    private Long permissionId;

    @TableField("VIEW_NAME")
    private String viewName;

    /**
     * menu-菜单 button-按钮
     */
    @TableField("VIEW_TYPE")
    private String viewType;

    @TableField("VIEW_ICON")
    private String viewIcon;

    @TableField("VIEW_PATH")
    private String viewPath;

    @TableField("VIEW_COMPONENT")
    private String viewComponent;

    @TableField("PARENT_ID")
    private Long parentId;

    @TableField("VIEW_ORDER")
    private Integer viewOrder;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    private String createdBy;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    private String updatedBy;


}
