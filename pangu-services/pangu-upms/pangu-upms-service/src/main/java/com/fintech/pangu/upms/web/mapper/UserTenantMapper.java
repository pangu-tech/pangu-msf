package com.fintech.pangu.upms.web.mapper;

import com.fintech.pangu.api.bean.qo.CreateAndModifyUserTenantRequest;
import com.fintech.pangu.upms.web.dto.SysUserTenantDto;
import org.mapstruct.Mapper;

/**
 * @Description:
 */
@Mapper(componentModel="spring")
public interface UserTenantMapper {

    SysUserTenantDto createAndModify2Dto(CreateAndModifyUserTenantRequest createAndModifyUserTenantRequest);

}
