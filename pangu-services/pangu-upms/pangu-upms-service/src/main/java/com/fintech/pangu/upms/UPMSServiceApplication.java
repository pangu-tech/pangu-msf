package com.fintech.pangu.upms;

import com.fintech.pangu.apollo.annotation.EnablePanGuApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnablePanGuApolloConfig
@SpringBootApplication
public class UPMSServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UPMSServiceApplication.class, args);
    }

}
