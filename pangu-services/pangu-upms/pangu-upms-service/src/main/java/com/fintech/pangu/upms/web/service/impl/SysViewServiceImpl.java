package com.fintech.pangu.upms.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fintech.pangu.upms.config.MultiTenantUtil;
import com.fintech.pangu.upms.web.dao.SysViewDao;
import com.fintech.pangu.upms.web.dto.SysViewDto;
import com.fintech.pangu.upms.web.entity.SysView;
import com.fintech.pangu.upms.web.mapper.ViewMapper;
import com.fintech.pangu.upms.web.service.SysViewService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 视图表 服务实现类
 * </p>
 *
 * @author Admin
 * @since 2019-10-16
 */
@Service
public class SysViewServiceImpl implements SysViewService {

    @Autowired
    private ViewMapper viewMapper;

    @Autowired
    private SysViewDao sysViewDao;

    @Override
    public List<SysViewDto> viewTreeData() {
        String tenantId = MultiTenantUtil.get();
        QueryWrapper<SysView> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(StringUtils.isNotEmpty(tenantId),SysView::getTenantId,tenantId);
        List<SysView> sysViews = sysViewDao.selectList(queryWrapper);
        return viewMapper.po2DtoList(sysViews);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Long addView(SysViewDto sysViewDto) {
        SysView sysView = viewMapper.dto2Po(sysViewDto);
        sysView.setTenantId(MultiTenantUtil.get());
        sysViewDao.insert(sysView);
        return sysView.getId();
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateView(SysViewDto sysViewDto) {
        SysView sysView = viewMapper.dto2Po(sysViewDto);
        sysView.setTenantId(MultiTenantUtil.get());
        sysViewDao.updateById(sysView);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteView(Long id) {
        sysViewDao.deleteById(id);
    }
}
