package com.fintech.pangu.upms.web.dto;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class SysDictDetailDto {

    private long size = 10;

    private long current = 1;

    private Long id;

    private Long dictId;

    private String dictDetailName;

    private String dictDetailValue;

    private String dictDetailIds;
}
