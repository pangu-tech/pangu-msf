package com.fintech.pangu.upms.web.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateUserRoleRequest;
import com.fintech.pangu.api.bean.qo.DeleteUserRoleRequest;
import com.fintech.pangu.api.bean.qo.FetchUserRoleRequest;
import com.fintech.pangu.api.bean.vo.FetchUserRoleResult;
import com.fintech.pangu.api.interfaces.AuthSysUserRole;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dto.SysUserRoleDto;
import com.fintech.pangu.upms.web.mapper.UserRoleMapper;
import com.fintech.pangu.upms.web.service.SysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制器
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
@RestController
public class SysUserRoleController implements AuthSysUserRole {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private SysUserRoleService sysUserRoleService;

    @Override
    public BaseResponse<IPage<FetchUserRoleResult>> listForPage(FetchUserRoleRequest fetchUserRoleRequest) {
        // req --> dto
        SysUserRoleDto sysUserRoleDto = userRoleMapper.fetchReq2Dto(fetchUserRoleRequest);

        IPage<SysUserRoleDto> pages = sysUserRoleService.listForPage(sysUserRoleDto);

        // dto --> result
        List<FetchUserRoleResult> userRoleList = userRoleMapper.dto2ResultList(pages.getRecords());

        IPage<FetchUserRoleResult> userRolePages = PageUtil.builder(userRoleList,pages);
        return new BaseResponse<>(userRolePages);
    }

    @Override
    public BaseResponse<IPage<FetchUserRoleResult>> roleUserListForPage(FetchUserRoleRequest fetchUserRoleRequest) {
        // req --> dto
        SysUserRoleDto sysUserRoleDto = userRoleMapper.fetchReq2Dto(fetchUserRoleRequest);

        IPage<SysUserRoleDto> pages = sysUserRoleService.roleUserListForPage(sysUserRoleDto);

        // dto --> result
        List<FetchUserRoleResult> userRoleList = userRoleMapper.dto2ResultList(pages.getRecords());

        IPage<FetchUserRoleResult> userRolePages = PageUtil.builder(userRoleList,pages);
        return new BaseResponse<>(userRolePages);
    }

    @Override
    public BaseResponse<Void> addUserRoles(@RequestBody CreateUserRoleRequest createUserRoleRequest) {
        SysUserRoleDto sysUserRoleDto = userRoleMapper.createReq2Dto(createUserRoleRequest);
        sysUserRoleService.addUserRoles(sysUserRoleDto);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> deleteUserRoles(@RequestBody DeleteUserRoleRequest deleteUserRoleRequest) {
        SysUserRoleDto sysUserRoleDto = userRoleMapper.deleteReq2Dto(deleteUserRoleRequest);
        sysUserRoleService.deleteUserRoles(sysUserRoleDto);
        return BaseResponse.success();
    }
}
