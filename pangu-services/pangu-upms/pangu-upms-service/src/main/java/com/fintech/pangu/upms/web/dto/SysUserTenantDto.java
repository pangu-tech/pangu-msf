package com.fintech.pangu.upms.web.dto;

import lombok.Data;

import java.util.List;

/**
 * @Description:
 */
@Data
public class SysUserTenantDto {

    private Long userId;

    private List<Long> tenantIds;
}
