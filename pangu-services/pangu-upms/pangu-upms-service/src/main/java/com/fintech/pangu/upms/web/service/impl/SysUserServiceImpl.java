package com.fintech.pangu.upms.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dao.SysUserDao;
import com.fintech.pangu.upms.web.dto.SysUserDto;
import com.fintech.pangu.upms.web.entity.SysUser;
import com.fintech.pangu.upms.web.mapper.UserMapper;
import com.fintech.pangu.upms.web.service.SysUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
@Service
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SysUserDao sysUserDao;

    @Override
    public IPage<SysUserDto> listForPage(SysUserDto sysUserDto) {
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper();
        queryWrapper.lambda()
                .like(StringUtils.isNotEmpty(sysUserDto.getUserName()),SysUser::getUserName,sysUserDto.getUserName())
                .eq(StringUtils.isNotEmpty(sysUserDto.getLoginName()),SysUser::getLoginName,sysUserDto.getLoginName())
                .eq(StringUtils.isNotEmpty(sysUserDto.getUserNo()),SysUser::getUserNo,sysUserDto.getUserNo())
                .eq(StringUtils.isNotEmpty(sysUserDto.getEmail()),SysUser::getEmail,sysUserDto.getEmail())
                .eq(StringUtils.isNotEmpty(sysUserDto.getMobile()),SysUser::getMobile,sysUserDto.getMobile());

        Page page = new Page(sysUserDto.getCurrent(),sysUserDto.getSize());
        IPage<SysUser> userList = sysUserDao.selectPage(page, queryWrapper);

        List<SysUserDto> list = userMapper.po2Dto(userList.getRecords());

        IPage<SysUserDto> userPage = PageUtil.builder(list,userList);

        return userPage;
    }

}
