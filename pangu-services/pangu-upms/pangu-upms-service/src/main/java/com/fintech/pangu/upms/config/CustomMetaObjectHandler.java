package com.fintech.pangu.upms.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * @Description:
 */
@Component
public class CustomMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        this.setInsertFieldValByName("tenantId",MultiTenantUtil.get(),metaObject);
        this.setInsertFieldValByName("validateState","1",metaObject);
        this.setInsertFieldValByName("createdTime",new Timestamp(System.currentTimeMillis()),metaObject);
        this.setInsertFieldValByName("updatedTime",new Timestamp(System.currentTimeMillis()),metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setInsertFieldValByName("updatedTime",new Timestamp(System.currentTimeMillis()),metaObject);
    }
}
