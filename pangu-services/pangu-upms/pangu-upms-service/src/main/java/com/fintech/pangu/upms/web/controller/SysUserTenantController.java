package com.fintech.pangu.upms.web.controller;


import com.fintech.pangu.api.bean.qo.CreateAndModifyUserTenantRequest;
import com.fintech.pangu.api.interfaces.AuthSysUserTenant;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.upms.web.dto.SysUserTenantDto;
import com.fintech.pangu.upms.web.mapper.UserTenantMapper;
import com.fintech.pangu.upms.web.service.SysUserTenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  控制器
 * </p>
 *
 * @author Admin
 * @since 2019-10-30
 */
@RestController
public class SysUserTenantController implements AuthSysUserTenant {

    @Autowired
    private UserTenantMapper userTenantMapper;

    @Autowired
    private SysUserTenantService sysUserTenantService;

    @Override
    public BaseResponse<Void> add(@RequestBody CreateAndModifyUserTenantRequest createAndModifyUserTenantRequest) {
        SysUserTenantDto sysUserTenantDto = userTenantMapper.createAndModify2Dto(createAndModifyUserTenantRequest);
        sysUserTenantService.addUserTenant(sysUserTenantDto);
        return BaseResponse.success();
    }
}
