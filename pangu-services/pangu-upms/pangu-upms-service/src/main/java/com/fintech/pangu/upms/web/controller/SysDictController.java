package com.fintech.pangu.upms.web.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateAndModifyDictRequest;
import com.fintech.pangu.api.bean.qo.FetchDictRequest;
import com.fintech.pangu.api.bean.vo.FetchDictResult;
import com.fintech.pangu.api.interfaces.AuthSysDict;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dto.SysDictDto;
import com.fintech.pangu.upms.web.mapper.DictMapper;
import com.fintech.pangu.upms.web.service.SysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 数据字典表 控制器
 * </p>
 *
 * @author Admin
 * @since 2019-10-30
 */
@RestController
public class SysDictController implements AuthSysDict {

    @Autowired
    private DictMapper dictMapper;

    @Autowired
    private SysDictService sysDictService;

    @Override
    public BaseResponse<IPage<FetchDictResult>> listForPage(FetchDictRequest fetchDictRequest) {
        // req --> dto
        SysDictDto sysDictDto = dictMapper.fetchReq2Dto(fetchDictRequest);
        // 分页查询
        IPage<SysDictDto> dictPage = sysDictService.listForPage(sysDictDto);
        // dto --> result
        List<FetchDictResult> dictList = dictMapper.dto2ResultList(dictPage.getRecords());
        // 构建返回
        IPage<FetchDictResult> dicts = PageUtil.builder(dictList,dictPage);

        return new BaseResponse<>(dicts);
    }

    @Override
    public BaseResponse<Void> add(@RequestBody CreateAndModifyDictRequest createAndModifyDictRequest) {
        SysDictDto sysDictDto = dictMapper.createAndModify2Dto(createAndModifyDictRequest);
        sysDictService.add(sysDictDto);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> update(@PathVariable Long id, @RequestBody CreateAndModifyDictRequest createAndModifyDictRequest) {
        SysDictDto sysDictDto = dictMapper.createAndModify2Dto(createAndModifyDictRequest);
        sysDictDto.setId(id);
        sysDictService.update(sysDictDto);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> delete(@PathVariable Long id) {
        sysDictService.delete(id);
        return BaseResponse.success();
    }
}
