package com.fintech.pangu.upms.web.mapper;

import com.fintech.pangu.api.bean.qo.CreateAndModifyViewRequest;
import com.fintech.pangu.api.bean.vo.FetchViewResult;
import com.fintech.pangu.upms.web.dto.SysViewDto;
import com.fintech.pangu.upms.web.entity.SysView;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Description:
 */
@Mapper(componentModel="spring")
public interface ViewMapper {

    SysViewDto createAndModify2Dto(CreateAndModifyViewRequest createAndModifyViewRequest);

    SysView dto2Po(SysViewDto sysViewDto);

    List<SysViewDto> po2DtoList(List<SysView> sysViews);

    List<FetchViewResult> dto2ResultList(List<SysViewDto> sysViewDtos);


}
