package com.fintech.pangu.upms.web.service;

import com.fintech.pangu.upms.web.dto.SysPermissionDto;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
public interface SysPermissionService{

    List<SysPermissionDto> permissionTreeData();

    List<SysPermissionDto> listForRolePermission(Long roleId);

    Long addPermission(SysPermissionDto sysPermissionDto);

    void updatePermission(SysPermissionDto sysPermissionDto);

    void deletePermission(Long id);

}
