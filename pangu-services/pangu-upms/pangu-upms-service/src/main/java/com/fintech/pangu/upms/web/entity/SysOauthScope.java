package com.fintech.pangu.upms.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fintech.pangu.commons.entity.PanguBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Admin
 * @since 2019-10-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_oauth_scope")
public class SysOauthScope extends PanguBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * SCOPE表达式，建议以 “系统:功能:操作” 格式定义，如商城的新增订单scope用 “shopping:order:add”，如表示商城订单scope用  “shopping:order”
     */
    @TableField("SCOPE")
    private String scope;

    /**
     * SCOPE名称
     */
    @TableField("SCOPE_NAME")
    private String scopeName;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    private String createdBy;

    /**
     * '更新人
     */
    @TableField("UPDATED_BY")
    private String updatedBy;


}
