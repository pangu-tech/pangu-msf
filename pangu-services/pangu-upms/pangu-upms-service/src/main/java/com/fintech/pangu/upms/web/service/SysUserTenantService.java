package com.fintech.pangu.upms.web.service;

import com.fintech.pangu.upms.web.dto.SysUserTenantDto;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Admin
 * @since 2019-10-30
 */
public interface SysUserTenantService{

    void addUserTenant(SysUserTenantDto sysUserTenantDto);

}
