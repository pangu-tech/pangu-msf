package com.fintech.pangu.upms.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fintech.pangu.upms.web.entity.SysDictDetail;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 数据字典详情表 Mapper 接口
 * </p>
 *
 * @author Admin
 * @since 2019-10-30
 */
@Mapper
public interface SysDictDetailDao extends BaseMapper<SysDictDetail> {

}
