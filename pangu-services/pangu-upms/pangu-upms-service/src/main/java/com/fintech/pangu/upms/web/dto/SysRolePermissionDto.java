package com.fintech.pangu.upms.web.dto;

import lombok.Data;

import java.util.List;

/**
 * @Description:
 */
@Data
public class SysRolePermissionDto {

    private Long roleId;

    private List<Long> permissionIds;
}
