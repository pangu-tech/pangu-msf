package com.fintech.pangu.upms.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dao.SysRoleDao;
import com.fintech.pangu.upms.web.dao.SysRolePermissionDao;
import com.fintech.pangu.upms.web.dao.SysUserRoleDao;
import com.fintech.pangu.upms.web.dto.SysRoleDto;
import com.fintech.pangu.upms.web.entity.SysRole;
import com.fintech.pangu.upms.web.entity.SysRolePermission;
import com.fintech.pangu.upms.web.entity.SysUserRole;
import com.fintech.pangu.upms.web.mapper.RoleMapper;
import com.fintech.pangu.upms.web.service.SysRoleService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private SysRoleDao sysRoleDao;

    @Autowired
    private SysUserRoleDao sysUserRoleDao;

    @Autowired
    private SysRolePermissionDao sysRolePermissionDao;

    @Override
    public SysRoleDto roleForId(Long id) {
        SysRole sysRole = sysRoleDao.selectById(id);
        // po --> dto
        SysRoleDto sysRoleDto = roleMapper.po2Dto(sysRole);
        return sysRoleDto;
    }

    @Override
    public IPage<SysRoleDto> listForPage(SysRoleDto sysRoleDto) {
        // 构建查询条件
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(StringUtils.isNotEmpty(sysRoleDto.getRoleCode()),SysRole::getRoleCode,sysRoleDto.getRoleCode())
                .eq(StringUtils.isNotEmpty(sysRoleDto.getRoleName()),SysRole::getRoleName,sysRoleDto.getRoleName())
                .eq(StringUtils.isNotEmpty(sysRoleDto.getRoleType()),SysRole::getRoleType,sysRoleDto.getRoleType());

        // 分页查询
        Page page = new Page(sysRoleDto.getCurrent(),sysRoleDto.getSize());
        IPage<SysRole> rolePage = sysRoleDao.selectPage(page,queryWrapper);

        // 结果转换 po --> dto
        List<SysRoleDto> roleList =  roleMapper.po2DtoList(rolePage.getRecords());

        // 构建返回
        IPage<SysRoleDto> roles = PageUtil.builder(roleList,rolePage);
        return roles;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void add(SysRoleDto sysRoleDto) {
        // dto --> po
        SysRole sysRole = roleMapper.dto2Po(sysRoleDto);
        // 新增
        sysRoleDao.insert(sysRole);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void update(SysRoleDto sysRoleDto) {
        // dto --> po
        SysRole sysRole = roleMapper.dto2Po(sysRoleDto);
        // 修改
        sysRoleDao.updateById(sysRole);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(Long id) {
        sysRoleDao.deleteById(id);

        sysUserRoleDao.delete(new QueryWrapper<SysUserRole>().eq("ROLE_ID",id));

        sysRolePermissionDao.delete(new QueryWrapper<SysRolePermission>().eq("ROLE_ID",id));
    }
}
