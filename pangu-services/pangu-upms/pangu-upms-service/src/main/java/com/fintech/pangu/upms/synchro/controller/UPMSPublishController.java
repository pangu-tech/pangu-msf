package com.fintech.pangu.upms.synchro.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class UPMSPublishController {

    @Autowired
    ApplicationEventPublisher applicationEventMulticaster;

    @RequestMapping(value = "/publish/{appName}")
    public Object publishConfig(@PathVariable("appName") String appName) {
        log.info("Publish app: " + appName);
        UPMSReleaseEvent event = new UPMSReleaseEvent(appName);

        applicationEventMulticaster.publishEvent(event);

        return "success";
    }

}
