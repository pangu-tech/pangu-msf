package com.fintech.pangu.upms.web.controller;

import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 */
@RestController
@RequestMapping("/user")
public class LoginController {


    @PostMapping("/login")
    public Map<String,Object> login(){
        Map<String,Object> params = new HashMap<>();
        params.put("token","admin-token");
        return params;
    }

    @PostMapping("/logout")
    public BaseResponse logout(){
        return BaseResponse.success();
    }

    @GetMapping("/info")
    public BaseResponse getInfo(){
        Map<String,Object> params = new HashMap<>();
        params.put("name","Super Admin");
        params.put("avatar","https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        params.put("introduction","I am a super administrator");
        String[] s= {"admin"};
        params.put("roles",s);
        return new BaseResponse(params);
    }
}
