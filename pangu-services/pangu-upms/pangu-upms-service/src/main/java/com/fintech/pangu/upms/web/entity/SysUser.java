package com.fintech.pangu.upms.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fintech.pangu.commons.entity.PanguBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.sql.Timestamp;

/**
 * <p>
 * 
 * </p>
 *
 * @author Admin
 * @since 2019-10-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_user")
public class SysUser extends PanguBaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("USER_NAME")
    private String userName;

    @TableField("USER_NO")
    private String userNo;

    @TableField("LOGIN_NAME")
    private String loginName;

    @TableField("PASSWORD")
    private String password;

    @TableField("SALT")
    private String salt;

    @TableField("MOBILE")
    private String mobile;

    @TableField("EMAIL")
    private String email;

    @TableField("USER_IMAGE")
    private String userImage;

    @TableField("SEX")
    private String sex;

    @TableField("BIRTHDAY")
    private String birthday;

    @TableField("NATIONALITY")
    private String nationality;

    @TableField("EDUCATION")
    private String education;

    @TableField("JOB")
    private String job;

    @TableField("HOME_ADDRESS")
    private String homeAddress;

    @TableField("HOME_ZIPCODE")
    private String homeZipcode;

    @TableField("HOME_TEL")
    private String homeTel;

    @TableField("OFFICE_TEL")
    private String officeTel;

    @TableField("OFFICE_ADDRESS")
    private String officeAddress;

    @TableField("ORDER_BY")
    private String orderBy;

    @TableField("IS_LOCKED")
    private String isLocked;

    @TableField("VERSION")
    private Long version;

    @TableField("PROBATION_PERIOD")
    private Integer probationPeriod;

    @TableField("ENTRY_DATE")
    private Timestamp entryDate;

    @TableField("QUIT_DATE")
    private Timestamp quitDate;

    @TableField("WORK_DATE")
    private Timestamp workDate;

    @TableField("POLITICAL_STATUS")
    private String politicalStatus;

    @TableField("USER_RELATION")
    private String userRelation;

    @TableField("CREATE_DATE")
    private Timestamp createDate;

    @TableField("CARD_NO")
    private String cardNo;

    @TableField("ANNUAL_LEAVE")
    private Integer annualLeave;

    @TableField("JXZJ")
    private String jxzj;

    @TableField("NJQSRQ")
    private Timestamp njqsrq;

    @TableField("PWD_FAILURES")
    private String pwdFailures;

    @TableField("LOGIN_TIME")
    private Timestamp loginTime;


}
