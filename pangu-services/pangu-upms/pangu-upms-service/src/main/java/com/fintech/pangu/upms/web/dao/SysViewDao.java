package com.fintech.pangu.upms.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fintech.pangu.upms.web.entity.SysView;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 视图表 Mapper 接口
 * </p>
 *
 * @author Admin
 * @since 2019-10-16
 */
@Mapper
public interface SysViewDao extends BaseMapper<SysView> {

}
