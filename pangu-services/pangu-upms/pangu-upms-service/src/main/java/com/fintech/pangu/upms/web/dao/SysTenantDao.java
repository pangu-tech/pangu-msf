package com.fintech.pangu.upms.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fintech.pangu.upms.web.entity.SysTenant;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 系统租户表 Mapper 接口
 * </p>
 *
 * @author Admin
 * @since 2019-10-17
 */
@Mapper
public interface SysTenantDao extends BaseMapper<SysTenant> {

    List<SysTenant> listForUserTenants(@Param(value = "userId") Long userId);
}
