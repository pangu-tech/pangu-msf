package com.fintech.pangu.upms.web.dto;

import lombok.Data;

import java.sql.Timestamp;

/**
 * @Description:
 */
@Data
public class SysRoleDto {

    private long size = 10;

    private long current = 1;

    private Long id;

    private String roleName;

    private String roleCode;

    private String roleType;

    private String tenantId;

    private String validateState;

    private Timestamp createdTime;

    private Timestamp updatedTime;
}
