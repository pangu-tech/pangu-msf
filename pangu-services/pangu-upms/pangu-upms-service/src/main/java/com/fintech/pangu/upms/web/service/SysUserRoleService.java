package com.fintech.pangu.upms.web.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.upms.web.dto.SysUserRoleDto;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
public interface SysUserRoleService{

    IPage<SysUserRoleDto> listForPage(SysUserRoleDto sysUserRoleDto);

    IPage<SysUserRoleDto> roleUserListForPage(SysUserRoleDto sysUserRoleDto);

    void addUserRoles(SysUserRoleDto sysUserRoleDto);

    void deleteUserRoles(SysUserRoleDto sysUserRoleDto);
}
