package com.fintech.pangu.upms.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fintech.pangu.commons.entity.PanguBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Admin
 * @since 2019-10-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_permission")
public class SysPermission extends PanguBaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 权限名称，非空
     */
    @TableField("PERMISSION_NAME")
    private String permissionName;

    /**
     * 权限类型：module-资源模块类型、url-权限URL类型
     */
    @TableField("PERMISSION_TYPE")
    private String permissionType;

    /**
     * 权限表达式，举例：order:*、order:view、order:add
     */
    @TableField("PERMISSION_EXPRESSION")
    private String permissionExpression;

    @TableField("HTTP_METHOD")
    private String httpMethod;

    @TableField("PERMISSION_URL")
    private String permissionUrl;

    /**
     * 父Module ID，非空
     */
    @TableField("PARENT_ID")
    private Long parentId;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    private String createdBy;

    /**
     * '更新人
     */
    @TableField("UPDATED_BY")
    private String updatedBy;


}
