package com.fintech.pangu.upms.web.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.upms.web.dto.SysTenantDto;

import java.util.List;

/**
 * <p>
 * 系统租户表 服务类
 * </p>
 *
 * @author Admin
 * @since 2019-10-17
 */
public interface SysTenantService{

    IPage<SysTenantDto> listForPage(SysTenantDto sysTenantDto);

    List<SysTenantDto> listForUserTenants(SysTenantDto sysTenantDto);

    void add(SysTenantDto sysTenantDto);

    void update(SysTenantDto sysTenantDto);

    void delete(Long id);
}
