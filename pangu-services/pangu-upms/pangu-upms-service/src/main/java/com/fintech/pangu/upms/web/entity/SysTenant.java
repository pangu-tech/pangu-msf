package com.fintech.pangu.upms.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fintech.pangu.commons.entity.PanguBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 系统租户表
 * </p>
 *
 * @author Admin
 * @since 2019-10-17
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_tenant")
public class SysTenant extends PanguBaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("TENANT_NAME")
    private String tenantName;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    private String createdBy;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    private String updatedBy;


}
