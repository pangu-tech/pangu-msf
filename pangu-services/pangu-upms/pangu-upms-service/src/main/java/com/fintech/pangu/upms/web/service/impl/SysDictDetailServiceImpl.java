package com.fintech.pangu.upms.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dao.SysDictDetailDao;
import com.fintech.pangu.upms.web.dto.SysDictDetailDto;
import com.fintech.pangu.upms.web.entity.SysDictDetail;
import com.fintech.pangu.upms.web.mapper.DictDetailMapper;
import com.fintech.pangu.upms.web.service.SysDictDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 数据字典详情表 服务实现类
 * </p>
 *
 * @author Admin
 * @since 2019-10-30
 */
@Service
public class SysDictDetailServiceImpl implements SysDictDetailService {

    @Autowired
    private DictDetailMapper dictDetailMapper;

    @Autowired
    private SysDictDetailDao sysDictDetailDao;

    @Override
    public IPage<SysDictDetailDto> listForPage(SysDictDetailDto sysDictDetailDto) {
        // 构建查询条件
        QueryWrapper<SysDictDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(SysDictDetail::getDictId,sysDictDetailDto.getDictId());

        // 分页查询
        Page page = new Page(sysDictDetailDto.getCurrent(),sysDictDetailDto.getSize());
        IPage<SysDictDetail> dictDetailPage = sysDictDetailDao.selectPage(page,queryWrapper);

        // 结果转换 po --> dto
        List<SysDictDetailDto> dictDetailList =  dictDetailMapper.po2DtoList(dictDetailPage.getRecords());

        // 构建返回
        IPage<SysDictDetailDto> dictDetails = PageUtil.builder(dictDetailList,dictDetailPage);
        return dictDetails;
    }

    @Override
    public void add(SysDictDetailDto sysDictDetailDto) {
        SysDictDetail sysDictDetail = dictDetailMapper.dto2Po(sysDictDetailDto);
        sysDictDetailDao.insert(sysDictDetail);
    }

    @Override
    public void update(SysDictDetailDto sysDictDetailDto) {
        SysDictDetail sysDictDetail = dictDetailMapper.dto2Po(sysDictDetailDto);
        sysDictDetailDao.updateById(sysDictDetail);
    }

    @Override
    public void delete(Long id) {
        sysDictDetailDao.deleteById(id);
    }

    @Override
    public void deleteBatch(SysDictDetailDto sysDictDetailDto) {
        sysDictDetailDao.deleteBatchIds(Arrays.asList(sysDictDetailDto.getDictDetailIds().split(",")));
    }
}
