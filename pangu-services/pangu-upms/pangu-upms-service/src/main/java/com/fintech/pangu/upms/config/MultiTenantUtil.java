package com.fintech.pangu.upms.config;

/**
 * @Description:
 */
public class MultiTenantUtil {

    private static final ThreadLocal<String> threadLocal = new ThreadLocal<String>();

    public static String get() {
        return  threadLocal.get();
    }

    public static void set(String tenantId) {
        threadLocal.set(tenantId);
    }

    public static void remove() {
        threadLocal.remove();
    }
}
