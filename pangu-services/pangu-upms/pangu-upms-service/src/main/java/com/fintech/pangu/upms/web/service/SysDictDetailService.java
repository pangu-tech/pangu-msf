package com.fintech.pangu.upms.web.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.upms.web.dto.SysDictDetailDto;

/**
 * <p>
 * 数据字典详情表 服务类
 * </p>
 *
 * @author Admin
 * @since 2019-10-30
 */
public interface SysDictDetailService{

    IPage<SysDictDetailDto> listForPage(SysDictDetailDto sysDictDetailDto);

    void add(SysDictDetailDto sysDictDetailDto);

    void update(SysDictDetailDto sysDictDetailDto);

    void delete(Long id);

    void deleteBatch(SysDictDetailDto sysDictDetailDto);
}
