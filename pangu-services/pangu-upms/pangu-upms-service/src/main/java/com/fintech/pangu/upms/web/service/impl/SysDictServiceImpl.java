package com.fintech.pangu.upms.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dao.SysDictDao;
import com.fintech.pangu.upms.web.dao.SysDictDetailDao;
import com.fintech.pangu.upms.web.dto.SysDictDto;
import com.fintech.pangu.upms.web.entity.SysDict;
import com.fintech.pangu.upms.web.entity.SysDictDetail;
import com.fintech.pangu.upms.web.mapper.DictMapper;
import com.fintech.pangu.upms.web.service.SysDictService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 数据字典表 服务实现类
 * </p>
 *
 * @author Admin
 * @since 2019-10-30
 */
@Service
public class SysDictServiceImpl implements SysDictService {

    @Autowired
    private DictMapper dictMapper;

    @Autowired
    private SysDictDao sysDictDao;

    @Autowired
    private SysDictDetailDao sysDictDetailDao;

    @Override
    public IPage<SysDictDto> listForPage(SysDictDto sysDictDto) {
        // 构建查询条件
        QueryWrapper<SysDict> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
                .eq(StringUtils.isNotEmpty(sysDictDto.getDictCode()),SysDict::getDictCode,sysDictDto.getDictCode())
                .eq(StringUtils.isNotEmpty(sysDictDto.getDictName()),SysDict::getDictName,sysDictDto.getDictName());

        // 分页查询
        Page page = new Page(sysDictDto.getCurrent(),sysDictDto.getSize());
        IPage<SysDict> dictPage = sysDictDao.selectPage(page,queryWrapper);

        // 结果转换 po --> dto
        List<SysDictDto> dictList =  dictMapper.po2DtoList(dictPage.getRecords());

        // 构建返回
        IPage<SysDictDto> dicts = PageUtil.builder(dictList,dictPage);
        return dicts;
    }

    @Override
    public void add(SysDictDto sysDictDto) {
        SysDict sysDict = dictMapper.dto2Po(sysDictDto);
        sysDictDao.insert(sysDict);
    }

    @Override
    public void update(SysDictDto sysDictDto) {
        SysDict sysDict = dictMapper.dto2Po(sysDictDto);
        sysDictDao.updateById(sysDict);
    }

    @Override
    public void delete(Long id) {
        sysDictDao.deleteById(id);
        sysDictDetailDao.delete(new QueryWrapper<SysDictDetail>().eq("DICT_ID",id));
    }
}
