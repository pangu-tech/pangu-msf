package com.fintech.pangu.upms.web.mapper;

import com.fintech.pangu.api.bean.qo.CreateAndModifyDictDetailRequest;
import com.fintech.pangu.api.bean.qo.DeleteDictDetailRequest;
import com.fintech.pangu.api.bean.qo.FetchDictDetailRequest;
import com.fintech.pangu.api.bean.vo.FetchDictDetailResult;
import com.fintech.pangu.upms.web.dto.SysDictDetailDto;
import com.fintech.pangu.upms.web.entity.SysDictDetail;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Description:
 */
@Mapper(componentModel="spring")
public interface DictDetailMapper {

    SysDictDetailDto fetchReq2Dto(FetchDictDetailRequest fetchDictDetailRequest);

    SysDictDetailDto createAndModify2Dto(CreateAndModifyDictDetailRequest createAndModifyDictDetailRequest);

    SysDictDetail dto2Po(SysDictDetailDto sysDictDetailDto);

    SysDictDetailDto po2Dto(SysDictDetail sysDictDetail);

    List<SysDictDetailDto> po2DtoList(List<SysDictDetail> sysDictDetailList);

    FetchDictDetailResult dto2Result(SysDictDetailDto sysDictDetailDto);

    List<FetchDictDetailResult> dto2ResultList(List<SysDictDetailDto> sysDictDetailDtoList);

    SysDictDetailDto deleteReq2Dto(DeleteDictDetailRequest deleteDictDetailRequest);

}
