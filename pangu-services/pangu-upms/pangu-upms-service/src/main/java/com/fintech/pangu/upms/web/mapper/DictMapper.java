package com.fintech.pangu.upms.web.mapper;

import com.fintech.pangu.api.bean.qo.CreateAndModifyDictRequest;
import com.fintech.pangu.api.bean.qo.FetchDictRequest;
import com.fintech.pangu.api.bean.vo.FetchDictResult;
import com.fintech.pangu.upms.web.dto.SysDictDto;
import com.fintech.pangu.upms.web.entity.SysDict;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Description:
 */
@Mapper(componentModel="spring")
public interface DictMapper {

    SysDictDto fetchReq2Dto(FetchDictRequest fetchDictRequest);

    SysDictDto createAndModify2Dto(CreateAndModifyDictRequest createAndModifyDictRequest);

    SysDict dto2Po(SysDictDto sysDictDto);

    SysDictDto po2Dto(SysDict sysDict);

    List<SysDictDto> po2DtoList(List<SysDict> sysDictList);

    FetchDictResult dto2Result(SysDictDto sysDictDto);

    List<FetchDictResult> dto2ResultList(List<SysDictDto> sysDictDtoList);
}
