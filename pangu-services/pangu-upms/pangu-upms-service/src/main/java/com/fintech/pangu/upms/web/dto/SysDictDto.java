package com.fintech.pangu.upms.web.dto;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class SysDictDto {

    private long size = 10;

    private long current = 1;

    private Long id;

    private String dictName;

    private String dictCode;
}
