package com.fintech.pangu.upms.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fintech.pangu.upms.web.entity.SysPermissionRelease;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 权限发布 Mapper 接口
 * </p>
 *
 * @author Admin
 * @since 2019-11-04
 */
@Mapper
public interface SysPermissionReleaseDao extends BaseMapper<SysPermissionRelease> {

}
