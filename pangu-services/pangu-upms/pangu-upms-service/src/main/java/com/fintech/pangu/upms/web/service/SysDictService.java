package com.fintech.pangu.upms.web.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.upms.web.dto.SysDictDto;

/**
 * <p>
 * 数据字典表 服务类
 * </p>
 *
 * @author Admin
 * @since 2019-10-30
 */
public interface SysDictService{

    IPage<SysDictDto> listForPage(SysDictDto sysDictDto);

    void add(SysDictDto sysDictDto);

    void update(SysDictDto sysDictDto);

    void delete(Long id);

}
