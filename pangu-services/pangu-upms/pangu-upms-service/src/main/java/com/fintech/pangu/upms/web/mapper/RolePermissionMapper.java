package com.fintech.pangu.upms.web.mapper;

import com.fintech.pangu.api.bean.qo.CreateAndModifyRolePermissionRequest;
import com.fintech.pangu.upms.web.dto.SysRolePermissionDto;
import org.mapstruct.Mapper;

/**
 * @Description:
 */
@Mapper(componentModel="spring")
public interface RolePermissionMapper {

    SysRolePermissionDto createAndModify2Dto(CreateAndModifyRolePermissionRequest createAndModifyRolePermissionRequest);

}
