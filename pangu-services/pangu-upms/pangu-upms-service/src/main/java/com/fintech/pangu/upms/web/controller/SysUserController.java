package com.fintech.pangu.upms.web.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.FetchUserRequest;
import com.fintech.pangu.api.bean.vo.FetchUserResult;
import com.fintech.pangu.api.interfaces.AuthSysUser;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dto.SysUserDto;
import com.fintech.pangu.upms.web.mapper.UserMapper;
import com.fintech.pangu.upms.web.service.SysUserService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制器
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
@RestController
@Api(tags = {"用户信息管理"})
public class SysUserController implements AuthSysUser {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private SysUserService sysUserService;

    @Override
    public BaseResponse<IPage<FetchUserResult>> listForPage(FetchUserRequest fetchUserRequest) {
        // req --> dto
        SysUserDto sysUserDto = userMapper.fetchReq2Dto(fetchUserRequest);

        IPage<SysUserDto> pages = sysUserService.listForPage(sysUserDto);

        // dto --> result
        List<FetchUserResult> list = userMapper.dto2Result(pages.getRecords());

        IPage<FetchUserResult> userPages = PageUtil.builder(list,pages);

        return new BaseResponse<>(userPages);
    }


    @Override
    public BaseResponse<Void> resetPassword(@PathVariable Long id) {
        return null;
    }
}
