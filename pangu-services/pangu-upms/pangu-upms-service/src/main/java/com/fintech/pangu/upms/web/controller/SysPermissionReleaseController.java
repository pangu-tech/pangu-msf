package com.fintech.pangu.upms.web.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateAndModifyPermissionReleaseRequest;
import com.fintech.pangu.api.bean.qo.FetchPermissionReleaseRequest;
import com.fintech.pangu.api.bean.vo.FetchPermissionReleaseResult;
import com.fintech.pangu.api.interfaces.AuthSysPermissionRelease;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.commons.api.PageUtil;
import com.fintech.pangu.upms.web.dto.SysPermissionReleaseDto;
import com.fintech.pangu.upms.web.mapper.PermissionReleaseMapper;
import com.fintech.pangu.upms.web.service.SysPermissionReleaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 权限发布 控制器
 * </p>
 *
 * @author Admin
 * @since 2019-11-04
 */
@RestController
public class SysPermissionReleaseController implements AuthSysPermissionRelease {

    @Autowired
    private PermissionReleaseMapper permissionReleaseMapper;

    @Autowired
    private SysPermissionReleaseService sysPermissionReleaseService;

    @Override
    public BaseResponse<IPage<FetchPermissionReleaseResult>> listForPage(FetchPermissionReleaseRequest fetchPermissionReleaseRequest) {
        // req --> dto
        SysPermissionReleaseDto sysPermissionReleaseDto = permissionReleaseMapper.fetchReq2Dto(fetchPermissionReleaseRequest);
        // 分页查询
        IPage<SysPermissionReleaseDto> permissionReleasePage = sysPermissionReleaseService.listForPage(sysPermissionReleaseDto);
        // dto --> result
        List<FetchPermissionReleaseResult> permissionReleaseList = permissionReleaseMapper.dto2ResultList(permissionReleasePage.getRecords());
        // 构建返回
        IPage<FetchPermissionReleaseResult> permissionReleases = PageUtil.builder(permissionReleaseList,permissionReleasePage);

        return new BaseResponse<>(permissionReleases);
    }

    @Override
    public BaseResponse<Void> add(@RequestBody CreateAndModifyPermissionReleaseRequest createAndModifyPermissionReleaseRequest) {
        SysPermissionReleaseDto sysPermissionReleaseDto = permissionReleaseMapper.createAndModify2Dto(createAndModifyPermissionReleaseRequest);
        sysPermissionReleaseDto.setReleaseTime(Timestamp.valueOf(LocalDateTime.now()));
        sysPermissionReleaseService.add(sysPermissionReleaseDto);
        return BaseResponse.success();
    }
}
