package com.fintech.pangu.upms.web.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.upms.web.dto.SysUserDto;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
public interface SysUserService{

    IPage<SysUserDto> listForPage(SysUserDto sysUserDto);

}
