package com.fintech.pangu.upms.web.mapper;

import com.fintech.pangu.api.bean.qo.CreateUserRoleRequest;
import com.fintech.pangu.api.bean.qo.DeleteUserRoleRequest;
import com.fintech.pangu.api.bean.qo.FetchUserRoleRequest;
import com.fintech.pangu.api.bean.vo.FetchUserRoleResult;
import com.fintech.pangu.upms.web.dto.SysUserRoleDto;
import com.fintech.pangu.upms.web.entity.SysUserRole;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Description:
 */
@Mapper(componentModel="spring")
public interface UserRoleMapper {

    SysUserRoleDto fetchReq2Dto(FetchUserRoleRequest fetchUserRoleRequest);

    SysUserRoleDto createReq2Dto(CreateUserRoleRequest createUserRoleRequest);

    SysUserRoleDto deleteReq2Dto(DeleteUserRoleRequest deleteUserRoleRequest);

    SysUserRole dto2Po(SysUserRoleDto sysUserRoleDto);

    List<FetchUserRoleResult> dto2ResultList(List<SysUserRoleDto> sysUserRoleDtoList);
}
