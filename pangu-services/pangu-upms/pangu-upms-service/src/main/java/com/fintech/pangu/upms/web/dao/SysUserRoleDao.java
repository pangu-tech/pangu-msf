package com.fintech.pangu.upms.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fintech.pangu.upms.web.dto.SysUserRoleDto;
import com.fintech.pangu.upms.web.entity.SysUserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
@Mapper
public interface SysUserRoleDao extends BaseMapper<SysUserRole> {

    IPage selectUserRoleByPage(Page page, @Param("sysUserRoleDto") SysUserRoleDto sysUserRoleDto);

    IPage roleUserListForPage(Page page, @Param("sysUserRoleDto") SysUserRoleDto sysUserRoleDto);

}
