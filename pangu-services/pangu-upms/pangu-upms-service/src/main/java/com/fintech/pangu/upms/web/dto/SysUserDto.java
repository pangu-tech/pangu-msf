package com.fintech.pangu.upms.web.dto;

import lombok.Data;

import java.sql.Timestamp;

/**
 * @Description:
 */
@Data
public class SysUserDto {

    private long size = 10;

    private long current = 1;

    private Long id;

    private String userName;

    private String userNo;

    private String loginName;

    private String mobile;

    private String email;

    private String sex;

    private String birthday;

    private String nationality;

    private String education;

    private String job;

    private String homeAddress;

    private String isLocked;

    private Timestamp loginTime;

    private String validateState;
}
