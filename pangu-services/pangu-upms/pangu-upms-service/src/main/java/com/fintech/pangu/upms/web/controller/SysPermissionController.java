package com.fintech.pangu.upms.web.controller;


import com.fintech.pangu.api.bean.qo.CreateAndModifyPermissionRequest;
import com.fintech.pangu.api.bean.vo.FetchPermissionResult;
import com.fintech.pangu.api.interfaces.AuthSysPermission;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.upms.web.dto.SysPermissionDto;
import com.fintech.pangu.upms.web.mapper.PermissionMapper;
import com.fintech.pangu.upms.web.service.SysPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 *  控制器
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
@RestController
public class SysPermissionController implements AuthSysPermission {

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private SysPermissionService sysPermissionService;

    @Override
    public BaseResponse<List<FetchPermissionResult>> permissionTreeData() {
        List<SysPermissionDto> permissionList = sysPermissionService.permissionTreeData();
        return new BaseResponse<>(permissionMapper.dto2ResultList(permissionList));
    }

    @Override
    public BaseResponse<List<FetchPermissionResult>> listForRolePermission(@PathVariable Long roleId) {
        List<SysPermissionDto> rolePermissionList = sysPermissionService.listForRolePermission(roleId);
        return new BaseResponse<>(permissionMapper.dto2ResultList(rolePermissionList));
    }

    @Override
    public BaseResponse<Long> addPermission(@RequestBody CreateAndModifyPermissionRequest createAndModifyPermissionRequest) {
        SysPermissionDto sysPermissionDto = permissionMapper.createAndModify2Dto(createAndModifyPermissionRequest);
        Long id = sysPermissionService.addPermission(sysPermissionDto);
        return new BaseResponse<>(id);
    }

    @Override
    public BaseResponse<Void> updatePermission(@PathVariable Long id, @RequestBody CreateAndModifyPermissionRequest createAndModifyPermissionRequest) {
        SysPermissionDto sysPermissionDto = permissionMapper.createAndModify2Dto(createAndModifyPermissionRequest);
        sysPermissionDto.setId(id);
        sysPermissionService.updatePermission(sysPermissionDto);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> deletePermission(@PathVariable Long id) {
        sysPermissionService.deletePermission(id);
        return BaseResponse.success();
    }
}
