package com.fintech.pangu.upms.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fintech.pangu.commons.entity.PanguBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 数据字典表
 * </p>
 *
 * @author Admin
 * @since 2019-10-30
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_dict")
public class SysDict extends PanguBaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("DICT_CODE")
    private String dictCode;

    @TableField("DICT_NAME")
    private String dictName;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    private String createdBy;

    /**
     * 更新人
     */
    @TableField("UPDATED_BY")
    private String updatedBy;


}
