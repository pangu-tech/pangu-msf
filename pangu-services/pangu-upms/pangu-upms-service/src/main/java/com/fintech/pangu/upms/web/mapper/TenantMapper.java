package com.fintech.pangu.upms.web.mapper;

import com.fintech.pangu.api.bean.qo.CreateAndModifyTenantRequest;
import com.fintech.pangu.api.bean.qo.FetchTenantRequest;
import com.fintech.pangu.api.bean.vo.FetchTenantResult;
import com.fintech.pangu.upms.web.dto.SysTenantDto;
import com.fintech.pangu.upms.web.entity.SysTenant;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Description:
 */
@Mapper(componentModel="spring")
public interface TenantMapper {

    SysTenantDto fetchReq2Dto(FetchTenantRequest fetchTenantRequest);

    SysTenantDto createAndModify2Dto(CreateAndModifyTenantRequest createAndModifyTenantRequest);

    SysTenant dto2Po(SysTenantDto sysTenantDto);

    SysTenantDto po2Dto(SysTenant sysTenant);

    List<SysTenantDto> po2DtoList(List<SysTenant> sysTenantList);

    FetchTenantResult dto2Result(SysTenantDto sysTenantDto);

    List<FetchTenantResult> dto2ResultList(List<SysTenantDto> sysTenantDtos);
}
