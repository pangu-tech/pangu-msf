package com.fintech.pangu.upms.web.service;

import com.fintech.pangu.upms.web.dto.SysViewDto;

import java.util.List;

/**
 * <p>
 * 视图表 服务类
 * </p>
 *
 * @author Admin
 * @since 2019-10-16
 */
public interface SysViewService{

    List<SysViewDto> viewTreeData();

    Long addView(SysViewDto sysViewDto);

    void updateView(SysViewDto sysViewDto);

    void deleteView(Long id);
}
