package com.fintech.pangu.upms.config;

import com.fintech.pangu.web.config.PanGuHandlerInterceptor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Description:
 */
@Component
public class MutiTenantInterceptor  extends PanGuHandlerInterceptor {

    private static final String TENANT_ID_HEADER = "sysCode";

    @Override
    public int interceptorOrder() {
        return 0;
    }

    @Override
    public String pathPatterns() {
        return "/**";
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String tenantId = request.getHeader(TENANT_ID_HEADER);
        if (StringUtils.isNotEmpty(tenantId)){
            MultiTenantUtil.set(tenantId);
        }
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        MultiTenantUtil.remove();
    }
}
