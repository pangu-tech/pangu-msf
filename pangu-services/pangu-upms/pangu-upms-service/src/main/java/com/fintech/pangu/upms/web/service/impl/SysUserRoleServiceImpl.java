package com.fintech.pangu.upms.web.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fintech.pangu.upms.web.dao.SysUserRoleDao;
import com.fintech.pangu.upms.web.dto.SysUserRoleDto;
import com.fintech.pangu.upms.web.entity.SysUserRole;
import com.fintech.pangu.upms.web.mapper.UserRoleMapper;
import com.fintech.pangu.upms.web.service.SysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
@Service
public class SysUserRoleServiceImpl implements SysUserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private SysUserRoleDao sysUserRoleDao;

    @Override
    public IPage<SysUserRoleDto> listForPage(SysUserRoleDto sysUserRoleDto) {
        Page page = new Page(sysUserRoleDto.getCurrent(),sysUserRoleDto.getSize());
        IPage<SysUserRoleDto> pages = sysUserRoleDao.selectUserRoleByPage(page,sysUserRoleDto);
        return pages;
    }

    @Override
    public IPage<SysUserRoleDto> roleUserListForPage(SysUserRoleDto sysUserRoleDto) {
        Page page = new Page(sysUserRoleDto.getCurrent(),sysUserRoleDto.getSize());
        IPage<SysUserRoleDto> pages = sysUserRoleDao.roleUserListForPage(page,sysUserRoleDto);
        return pages;
    }

    @Override
    public void addUserRoles(SysUserRoleDto sysUserRoleDto) {
        Optional.ofNullable(sysUserRoleDto.getUserIds()).ifPresent(list -> {
            list.stream().forEach(userId -> {
                SysUserRole sysUserRole = new SysUserRole();
                sysUserRole.setRoleId(sysUserRoleDto.getRoleId());
                sysUserRole.setUserId(userId);
                sysUserRoleDao.insert(sysUserRole);
            });
        });
    }

    @Override
    public void deleteUserRoles(SysUserRoleDto sysUserRoleDto) {
        sysUserRoleDao.deleteBatchIds(Arrays.asList(sysUserRoleDto.getUserRoleIds().split(",")));
    }
}
