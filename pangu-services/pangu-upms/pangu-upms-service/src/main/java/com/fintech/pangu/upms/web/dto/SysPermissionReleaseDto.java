package com.fintech.pangu.upms.web.dto;

import lombok.Data;

import java.sql.Timestamp;

/**
 * @Description:
 */
@Data
public class SysPermissionReleaseDto {
    private long size = 10;

    private long current = 1;

    private Long id;

    private String publisher;

    private String description;

    private Timestamp releaseTime;

    private String releaseStartTime;

    private String releaseEndTime;
}
