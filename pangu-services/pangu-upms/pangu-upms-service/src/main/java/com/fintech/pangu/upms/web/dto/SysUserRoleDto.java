package com.fintech.pangu.upms.web.dto;

import lombok.Data;

import java.util.List;

/**
 * @Description:
 */
@Data
public class SysUserRoleDto {

    private long size = 10;

    private long current = 1;

    private Long id;

    private Long userId;

    private String userName;

    private String userNo;

    private Long roleId;

    private String roleName;

    private String userRoleIds;

    private List<Long> userIds;
}
