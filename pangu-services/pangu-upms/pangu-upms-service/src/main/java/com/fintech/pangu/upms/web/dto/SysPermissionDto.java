package com.fintech.pangu.upms.web.dto;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class SysPermissionDto {

    private Long id;

    private Long parentId;

    private String permissionName;

    private String permissionType;

    private String permissionExpression;

    private String httpMethod;

    private String permissionUrl;
}
