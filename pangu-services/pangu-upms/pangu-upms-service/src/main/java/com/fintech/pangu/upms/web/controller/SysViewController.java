package com.fintech.pangu.upms.web.controller;


import com.fintech.pangu.api.bean.qo.CreateAndModifyViewRequest;
import com.fintech.pangu.api.bean.vo.FetchViewResult;
import com.fintech.pangu.api.interfaces.AuthSysView;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.upms.web.dto.SysViewDto;
import com.fintech.pangu.upms.web.mapper.ViewMapper;
import com.fintech.pangu.upms.web.service.SysViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 视图表 控制器
 * </p>
 *
 * @author Admin
 * @since 2019-10-16
 */
@RestController
public class SysViewController implements AuthSysView {

    @Autowired
    private ViewMapper viewMapper;

    @Autowired
    private SysViewService sysViewService;

    @Override
    public BaseResponse<List<FetchViewResult>> viewTreeData() {
        List<SysViewDto> sysViewDtos = sysViewService.viewTreeData();
        return new BaseResponse<>(viewMapper.dto2ResultList(sysViewDtos));
    }

    @Override
    public BaseResponse<Long> addView(@RequestBody CreateAndModifyViewRequest createAndModifyViewRequest) {
        SysViewDto sysViewDto = viewMapper.createAndModify2Dto(createAndModifyViewRequest);
        Long id = sysViewService.addView(sysViewDto);
        return new BaseResponse<>(id);
    }

    @Override
    public BaseResponse<Void> updateView(@PathVariable Long id, @RequestBody CreateAndModifyViewRequest createAndModifyViewRequest) {
        SysViewDto sysViewDto = viewMapper.createAndModify2Dto(createAndModifyViewRequest);
        sysViewDto.setId(id);
        sysViewService.updateView(sysViewDto);
        return BaseResponse.success();
    }

    @Override
    public BaseResponse<Void> deleteView(@PathVariable Long id) {
        sysViewService.deleteView(id);
        return BaseResponse.success();
    }
}
