package com.fintech.pangu.upms.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fintech.pangu.upms.web.entity.SysUserTenant;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Admin
 * @since 2019-10-30
 */
@Mapper
public interface SysUserTenantDao extends BaseMapper<SysUserTenant> {

}
