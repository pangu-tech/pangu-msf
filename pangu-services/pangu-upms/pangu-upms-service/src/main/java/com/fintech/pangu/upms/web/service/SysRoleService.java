package com.fintech.pangu.upms.web.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.upms.web.dto.SysRoleDto;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
public interface SysRoleService{

    SysRoleDto roleForId(Long id);

    IPage<SysRoleDto> listForPage(SysRoleDto sysRoleDto);

    void add(SysRoleDto sysRoleDto);

    void update(SysRoleDto sysRoleDto);

    void delete(Long id);
}
