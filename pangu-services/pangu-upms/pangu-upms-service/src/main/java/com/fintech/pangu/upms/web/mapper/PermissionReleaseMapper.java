package com.fintech.pangu.upms.web.mapper;

import com.fintech.pangu.api.bean.qo.CreateAndModifyPermissionReleaseRequest;
import com.fintech.pangu.api.bean.qo.FetchPermissionReleaseRequest;
import com.fintech.pangu.api.bean.vo.FetchPermissionReleaseResult;
import com.fintech.pangu.upms.web.dto.SysPermissionReleaseDto;
import com.fintech.pangu.upms.web.entity.SysPermissionRelease;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @Description:
 */
@Mapper(componentModel="spring")
public interface PermissionReleaseMapper {

    SysPermissionReleaseDto fetchReq2Dto(FetchPermissionReleaseRequest fetchPermissionReleaseRequest);

    SysPermissionReleaseDto createAndModify2Dto(CreateAndModifyPermissionReleaseRequest createAndModifyPermissionReleaseRequest);

    SysPermissionRelease dto2Po(SysPermissionReleaseDto sysPermissionReleaseDto);

    SysPermissionReleaseDto po2Dto(SysPermissionRelease sysPermissionRelease);

    List<SysPermissionReleaseDto> po2DtoList(List<SysPermissionRelease> sysPermissionReleaseList);

    FetchPermissionReleaseResult dto2Result(SysPermissionReleaseDto sysPermissionReleaseDto);

    List<FetchPermissionReleaseResult> dto2ResultList(List<SysPermissionReleaseDto> sysPermissionReleaseDtoList);
}
