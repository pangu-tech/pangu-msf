package com.fintech.pangu.upms.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fintech.pangu.commons.entity.PanguBaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Admin
 * @since 2019-10-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_role")
public class SysRole extends PanguBaseEntity {

    private static final long serialVersionUID = 1L;

    @TableField("ROLE_CODE")
    private String roleCode;

    @TableField("ROLE_NAME")
    private String roleName;

    @TableField("ROLE_TYPE")
    private String roleType;

    /**
     * 创建人
     */
    @TableField("CREATED_BY")
    private String createdBy;

    /**
     * '更新人
     */
    @TableField("UPDATED_BY")
    private String updatedBy;


}
