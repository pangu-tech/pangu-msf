package com.fintech.pangu.upms.web.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.upms.web.dto.SysPermissionReleaseDto;

/**
 * <p>
 * 权限发布 服务类
 * </p>
 *
 * @author Admin
 * @since 2019-11-04
 */
public interface SysPermissionReleaseService{

    IPage<SysPermissionReleaseDto> listForPage(SysPermissionReleaseDto sysPermissionReleaseDto);

    void add(SysPermissionReleaseDto sysPermissionReleaseDto);
}
