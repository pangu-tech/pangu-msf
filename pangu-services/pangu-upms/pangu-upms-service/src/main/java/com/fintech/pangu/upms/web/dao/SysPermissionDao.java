package com.fintech.pangu.upms.web.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fintech.pangu.upms.web.entity.SysPermission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
@Mapper
public interface SysPermissionDao extends BaseMapper<SysPermission> {

    List<SysPermission> listForRolePermission(@Param(value = "roleId") Long roleId);
}
