package com.fintech.pangu.upms.web.controller;


import com.fintech.pangu.api.bean.qo.CreateAndModifyRolePermissionRequest;
import com.fintech.pangu.api.interfaces.AuthSysRolePermission;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.upms.web.dto.SysRolePermissionDto;
import com.fintech.pangu.upms.web.mapper.RolePermissionMapper;
import com.fintech.pangu.upms.web.service.SysRolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  控制器
 * </p>
 *
 * @author Admin
 * @since 2019-10-08
 */
@RestController
public class SysRolePermissionController implements AuthSysRolePermission {

    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    @Autowired
    private SysRolePermissionService sysRolePermissionService;

    @Override
    public BaseResponse<Void> addRolePermission(@RequestBody CreateAndModifyRolePermissionRequest createAndModifyRolePermissionRequest) {
        SysRolePermissionDto sysRolePermissionDto = rolePermissionMapper.createAndModify2Dto(createAndModifyRolePermissionRequest);
        sysRolePermissionService.addRolePermission(sysRolePermissionDto);
        return BaseResponse.success();
    }
}
