package com.fintech.pangu.feign;

import com.fintech.pangu.api.interfaces.AuthSysDict;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Description:
 */
@FeignClient(value = "pangu-upms-service")
public interface IAuthSysDict extends AuthSysDict {

}
