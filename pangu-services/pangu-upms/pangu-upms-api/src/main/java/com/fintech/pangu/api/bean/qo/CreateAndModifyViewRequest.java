package com.fintech.pangu.api.bean.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateAndModifyViewRequest {

    private Long permissionId;

    private String viewName;

    private String viewType;

    private String viewIcon;

    private String viewPath;

    private String viewComponent;

    private Long parentId;

    private Integer viewOrder;
}
