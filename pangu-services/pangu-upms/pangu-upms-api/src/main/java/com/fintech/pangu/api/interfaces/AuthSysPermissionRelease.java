package com.fintech.pangu.api.interfaces;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateAndModifyPermissionReleaseRequest;
import com.fintech.pangu.api.bean.qo.FetchPermissionReleaseRequest;
import com.fintech.pangu.api.bean.vo.FetchPermissionReleaseResult;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Description:
 */
public interface AuthSysPermissionRelease {

    @GetMapping("/api/v1/permissionRelease")
    BaseResponse<IPage<FetchPermissionReleaseResult>> listForPage(FetchPermissionReleaseRequest fetchPermissionReleaseRequest);

    @PostMapping("/api/v1/permissionRelease")
    BaseResponse<Void> add(@RequestBody CreateAndModifyPermissionReleaseRequest createAndModifyPermissionReleaseRequest);
}
