package com.fintech.pangu.api.bean.vo;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class FetchTenantResult {

    private Long id;

    private String tenantName;

    private String tenantId;
}
