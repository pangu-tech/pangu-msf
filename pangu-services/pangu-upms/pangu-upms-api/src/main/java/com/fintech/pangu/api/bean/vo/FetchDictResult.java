package com.fintech.pangu.api.bean.vo;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class FetchDictResult {

    private Long id;

    private String dictName;

    private String dictCode;
}
