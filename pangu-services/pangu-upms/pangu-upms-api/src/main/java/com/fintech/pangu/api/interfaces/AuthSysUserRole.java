package com.fintech.pangu.api.interfaces;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateUserRoleRequest;
import com.fintech.pangu.api.bean.qo.DeleteUserRoleRequest;
import com.fintech.pangu.api.bean.qo.FetchUserRoleRequest;
import com.fintech.pangu.api.bean.vo.FetchUserRoleResult;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Description:
 */
public interface AuthSysUserRole {

    @GetMapping("/api/v1/userRoles")
    BaseResponse<IPage<FetchUserRoleResult>> listForPage(FetchUserRoleRequest fetchUserRoleRequest);

    @GetMapping("/api/v1/userRoles/getUsers")
    BaseResponse<IPage<FetchUserRoleResult>> roleUserListForPage(FetchUserRoleRequest fetchUserRoleRequest);

    @PostMapping("/api/v1/userRoles")
    BaseResponse<Void> addUserRoles(@RequestBody CreateUserRoleRequest createUserRoleRequest);

    @PostMapping("/api/v1/deleteUserRoles")
    BaseResponse<Void> deleteUserRoles(@RequestBody DeleteUserRoleRequest deleteUserRoleRequest);

}
