package com.fintech.pangu.api.bean.vo;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class FetchRoleResult {

    private Long id;

    private String roleName;

    private String roleCode;

    private String roleType;
}
