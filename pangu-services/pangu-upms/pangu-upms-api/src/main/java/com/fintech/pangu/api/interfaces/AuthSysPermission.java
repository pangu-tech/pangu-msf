package com.fintech.pangu.api.interfaces;

import com.fintech.pangu.api.bean.qo.CreateAndModifyPermissionRequest;
import com.fintech.pangu.api.bean.vo.FetchPermissionResult;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Description:
 */
public interface AuthSysPermission {

    @GetMapping("/api/v1/permissions/treeData")
    BaseResponse<List<FetchPermissionResult>> permissionTreeData();

    @GetMapping("/api/v1/permissions/{roleId}")
    BaseResponse<List<FetchPermissionResult>> listForRolePermission(@PathVariable(value = "roleId") Long roleId);

    @PostMapping("/api/v1/permissions")
    BaseResponse<Long> addPermission(@RequestBody CreateAndModifyPermissionRequest createAndModifyPermissionRequest);

    @PutMapping("/api/v1/permissions/{id}")
    BaseResponse<Void> updatePermission(@PathVariable("id") Long id, @RequestBody CreateAndModifyPermissionRequest createAndModifyPermissionRequest);

    @DeleteMapping("/api/v1/permissions/{id}")
    BaseResponse<Void> deletePermission(@PathVariable("id") Long id);

}
