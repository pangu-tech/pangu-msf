package com.fintech.pangu.api.bean.vo;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class FetchRolePermissionResult {

    private Long permissionId;
}
