package com.fintech.pangu.api.interfaces;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateAndModifyRoleRequest;
import com.fintech.pangu.api.bean.qo.FetchRoleRequest;
import com.fintech.pangu.api.bean.vo.FetchRoleResult;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Description:
 */
public interface AuthSysRole {

    @GetMapping("/api/v1/roles/{id}")
    BaseResponse<FetchRoleResult> roleForId(@PathVariable("id") Long id);

    @GetMapping("/api/v1/roles")
    BaseResponse<IPage<FetchRoleResult>> listForPage(FetchRoleRequest fetchRoleRequest);

    @PostMapping("/api/v1/roles")
    BaseResponse<Void> add(@RequestBody CreateAndModifyRoleRequest createAndModifyRoleRequest);

    @PutMapping("/api/v1/roles/{id}")
    BaseResponse<Void> update(@PathVariable("id") Long id, @RequestBody CreateAndModifyRoleRequest createAndModifyRoleRequest);

    @DeleteMapping("/api/v1/roles/{id}")
    BaseResponse<Void> delete(@PathVariable("id") Long id);
}
