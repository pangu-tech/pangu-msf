package com.fintech.pangu.api.interfaces;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.FetchUserRequest;
import com.fintech.pangu.api.bean.vo.FetchUserResult;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * @Description:
 */
public interface AuthSysUser {

    @GetMapping("/api/v1/users")
    BaseResponse<IPage<FetchUserResult>> listForPage(FetchUserRequest fetchUserRequest);

    @PutMapping("/api/v1/resetPassword/{id}")
    BaseResponse<Void> resetPassword(@PathVariable("id") Long id);

}
