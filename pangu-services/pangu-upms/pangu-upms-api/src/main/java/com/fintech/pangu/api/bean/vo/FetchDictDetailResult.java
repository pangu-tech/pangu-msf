package com.fintech.pangu.api.bean.vo;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class FetchDictDetailResult {
    private Long id;

    private Long dictId;

    private String dictDetailName;

    private String dictDetailValue;
}
