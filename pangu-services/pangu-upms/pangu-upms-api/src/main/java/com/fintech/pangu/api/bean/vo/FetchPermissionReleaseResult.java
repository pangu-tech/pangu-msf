package com.fintech.pangu.api.bean.vo;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class FetchPermissionReleaseResult {

    private Long id;

    private String publisher;

    private String description;

    private String releaseTime;
}
