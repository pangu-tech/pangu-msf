package com.fintech.pangu.api.interfaces;

import com.fintech.pangu.api.bean.qo.CreateAndModifyViewRequest;
import com.fintech.pangu.api.bean.vo.FetchViewResult;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Description:
 */
public interface AuthSysView {

    @GetMapping("/api/v1/views/treeData")
    BaseResponse<List<FetchViewResult>> viewTreeData();

    @PostMapping("/api/v1/views")
    BaseResponse<Long> addView(@RequestBody CreateAndModifyViewRequest createAndModifyViewRequest);

    @PutMapping("/api/v1/views/{id}")
    BaseResponse<Void> updateView(@PathVariable("id") Long id, @RequestBody CreateAndModifyViewRequest createAndModifyViewRequest);

    @DeleteMapping("/api/v1/views/{id}")
    BaseResponse<Void> deleteView(@PathVariable("id") Long id);

}
