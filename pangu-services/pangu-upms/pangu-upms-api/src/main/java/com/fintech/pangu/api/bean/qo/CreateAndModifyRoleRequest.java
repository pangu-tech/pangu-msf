package com.fintech.pangu.api.bean.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateAndModifyRoleRequest {

    @NotEmpty(message = "角色名称不允许为空")
    private String roleName;

    @NotEmpty(message = "角色编码不允许为空")
    private String roleCode;

    @NotEmpty(message = "角色类型不允许为空")
    private String roleType;
}
