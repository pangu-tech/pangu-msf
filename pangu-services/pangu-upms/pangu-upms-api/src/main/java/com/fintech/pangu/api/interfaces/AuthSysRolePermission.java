package com.fintech.pangu.api.interfaces;

import com.fintech.pangu.api.bean.qo.CreateAndModifyRolePermissionRequest;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Description:
 */
public interface AuthSysRolePermission {

    @PostMapping("/api/v1/rolePermissions")
    BaseResponse<Void> addRolePermission(@RequestBody CreateAndModifyRolePermissionRequest createAndModifyRolePermissionRequest);
}
