package com.fintech.pangu.api.bean.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateUserRoleRequest {

    private List<Long> userIds;

    private Long roleId;
}
