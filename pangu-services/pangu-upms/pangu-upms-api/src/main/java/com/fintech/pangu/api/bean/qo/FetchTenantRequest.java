package com.fintech.pangu.api.bean.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class FetchTenantRequest {

    private long size = 10;

    private long current = 1;

    private String tenantName;

    private String tenantId;

    private Long userId;
}
