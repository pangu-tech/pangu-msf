package com.fintech.pangu.api.interfaces;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateAndModifyTenantRequest;
import com.fintech.pangu.api.bean.qo.FetchTenantRequest;
import com.fintech.pangu.api.bean.vo.FetchTenantResult;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Description:
 */
public interface AuthSysTenant {

    @GetMapping("/api/v1/tenants")
    BaseResponse<IPage<FetchTenantResult>> listForPage(FetchTenantRequest fetchTenantRequest);

    @GetMapping("/api/v1/tenants/user")
    BaseResponse<List<FetchTenantResult>> listForUserTenant(FetchTenantRequest fetchTenantRequest);

    @PostMapping("/api/v1/tenants")
    BaseResponse<Void> add(@RequestBody CreateAndModifyTenantRequest createAndModifyTenantRequest);

    @PutMapping("/api/v1/tenants/{id}")
    BaseResponse<Void> update(@PathVariable("id") Long id, @RequestBody CreateAndModifyTenantRequest createAndModifyTenantRequest);

    @DeleteMapping("/api/v1/tenants/{id}")
    BaseResponse<Void> delete(@PathVariable("id") Long id);
}
