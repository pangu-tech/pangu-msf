package com.fintech.pangu.api.bean.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class FetchDictRequest {

    private long size = 10;

    private long current = 1;

    private String dictCode;

    private String dictName;
}
