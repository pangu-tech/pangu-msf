package com.fintech.pangu.api.bean.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateAndModifyDictDetailRequest {

    private Long dictId;

    private String dictDetailName;

    private String dictDetailValue;
}
