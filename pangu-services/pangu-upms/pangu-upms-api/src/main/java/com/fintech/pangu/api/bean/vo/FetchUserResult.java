package com.fintech.pangu.api.bean.vo;

import lombok.Data;

import java.sql.Timestamp;

/**
 * @Description:
 */
@Data
public class FetchUserResult {

    private Long id;

    private String userName;

    private String userNo;

    private String loginName;

    private String mobile;

    private String email;

    private String sex;

    private String birthday;

    private String nationality;

    private String education;

    private String job;

    private String homeAddress;

    private String isLocked;

    private Timestamp loginTime;

    private String validateState;

}
