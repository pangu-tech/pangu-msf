package com.fintech.pangu.api.bean.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateAndModifyPermissionRequest {

    private String permissionName;

    private String permissionType;

    private String permissionExpression;

    private String httpMethod;

    private String permissionUrl;

    private Long parentId;
}
