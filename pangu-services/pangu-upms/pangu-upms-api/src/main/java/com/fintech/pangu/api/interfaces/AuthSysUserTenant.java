package com.fintech.pangu.api.interfaces;

import com.fintech.pangu.api.bean.qo.CreateAndModifyUserTenantRequest;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Description:
 */
public interface AuthSysUserTenant {


    @PostMapping("/api/v1/userTenants")
    BaseResponse<Void> add(@RequestBody CreateAndModifyUserTenantRequest createAndModifyUserTenantRequest);

}
