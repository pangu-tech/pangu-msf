package com.fintech.pangu.api.interfaces;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateAndModifyDictRequest;
import com.fintech.pangu.api.bean.qo.FetchDictRequest;
import com.fintech.pangu.api.bean.vo.FetchDictResult;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Description:
 */
public interface AuthSysDict {

    @GetMapping("/api/v1/dicts")
    BaseResponse<IPage<FetchDictResult>> listForPage(FetchDictRequest fetchDictRequest);

    @PostMapping("/api/v1/dicts")
    BaseResponse<Void> add(@RequestBody CreateAndModifyDictRequest createAndModifyDictRequest);

    @PutMapping("/api/v1/dicts/{id}")
    BaseResponse<Void> update(@PathVariable(value = "id") Long id, @RequestBody CreateAndModifyDictRequest createAndModifyDictRequest);

    @DeleteMapping("/api/v1/dicts/{id}")
    BaseResponse<Void> delete(@PathVariable(value = "id") Long id);

}
