package com.fintech.pangu.api.bean.vo;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class FetchPermissionResult {

    private Long id;

    private Long parentId;

    private String permissionName;

    private String permissionType;

    private String permissionExpression;

    private String httpMethod;

    private String permissionUrl;
}
