package com.fintech.pangu.api.bean.vo;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class FetchUserRoleResult {

    private Long id;

    private Long userId;

    private String userName;

    private String userNo;

    private Long roleId;

    private String roleName;
}
