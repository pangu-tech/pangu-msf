package com.fintech.pangu.api.bean.qo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateAndModifyUserRequest {

    private Long id;

    private String userName;

    private String userNo;

    private String loginName;

    private String password;

    private String mobile;

    private String email;

    private String sex;

    private String birthday;

    private String nationality;

    private String education;

    private String job;

    private String homeAddress;

    private String validateState;

    private String isLocked;

}
