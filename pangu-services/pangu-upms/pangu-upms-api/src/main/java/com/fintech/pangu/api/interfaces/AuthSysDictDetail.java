package com.fintech.pangu.api.interfaces;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.api.bean.qo.CreateAndModifyDictDetailRequest;
import com.fintech.pangu.api.bean.qo.DeleteDictDetailRequest;
import com.fintech.pangu.api.bean.qo.FetchDictDetailRequest;
import com.fintech.pangu.api.bean.vo.FetchDictDetailResult;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Description:
 */
public interface AuthSysDictDetail {

    @GetMapping("/api/v1/dictDetails")
    BaseResponse<IPage<FetchDictDetailResult>> listForPage(FetchDictDetailRequest fetchDictDetailRequest);

    @PostMapping("/api/v1/dictDetails")
    BaseResponse<Void> add(@RequestBody CreateAndModifyDictDetailRequest createAndModifyDictDetailRequest);

    @PutMapping("/api/v1/dictDetails/{id}")
    BaseResponse<Void> update(@PathVariable(value = "id") Long id, @RequestBody CreateAndModifyDictDetailRequest createAndModifyDictDetailRequest);

    @DeleteMapping("/api/v1/dictDetails/{id}")
    BaseResponse<Void> delete(@PathVariable(value = "id") Long id);

    @PostMapping("/api/v1/dictDetails/deleteBatch")
    BaseResponse<Void> deleteBatch(@RequestBody DeleteDictDetailRequest deleteDictDetailRequest);
}
