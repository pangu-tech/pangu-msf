package com.fintech.pangu.gateway;

import com.fintech.pangu.apollo.annotation.EnablePanGuApolloConfig;
import com.fintech.pangu.autoconfigure.security.oauth2.client.EnablePanGuOAuth2SsoClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
@EnableDiscoveryClient
@EnablePanGuOAuth2SsoClient
@EnablePanGuApolloConfig
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

}
