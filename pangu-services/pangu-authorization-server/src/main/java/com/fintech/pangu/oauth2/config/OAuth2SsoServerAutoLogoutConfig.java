package com.fintech.pangu.oauth2.config;

import com.fintech.pangu.oauth2.logout.OAuth2SsoServerAutoLogoutHandler;
import com.fintech.pangu.oauth2.logout.OAuth2SsoServerAutoLogoutSuccessHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.web.authentication.logout.LogoutFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.ArrayList;
import java.util.List;

/**
 * SSO Server端自动级联登出配置
 */
@Configuration
public class OAuth2SsoServerAutoLogoutConfig {

    /**
     * SSO Server端自动级联登出处理器
     * @return
     */
    @Bean
    public LogoutHandler oauth2SsoServerAutoLogoutHandler(){
        return new OAuth2SsoServerAutoLogoutHandler();
    }

    /**
     * SSO Server端自动级联登出Filter
     * @return
     */
    @Bean
    public LogoutFilter oauth2SsoServerAutoLogoutFilter(){
        List<LogoutHandler> logoutHandlers = new ArrayList<>();
        logoutHandlers.add(oauth2SsoServerAutoLogoutHandler()); // 负责清理session，异步通知SSO Client
        LogoutHandler[] handlers = logoutHandlers.toArray(new LogoutHandler[logoutHandlers.size()]);

        LogoutFilter logoutFilter = new LogoutFilter(new OAuth2SsoServerAutoLogoutSuccessHandler(), handlers);
        logoutFilter.setLogoutRequestMatcher(new AntPathRequestMatcher("/ssoAutoLogout", "POST"));

        return logoutFilter;
    }

}
