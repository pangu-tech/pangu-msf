package com.fintech.pangu.oauth2.logout;

import com.fintech.pangu.oauth2.properties.OAuth2AuthorizationServerProperties;
import com.fintech.pangu.oauth2.properties.client.OAuth2ClientProperties;
import com.google.common.base.Splitter;
import lombok.Data;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.session.Session;
import org.springframework.util.MultiValueMap;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * SSO Server端的自动级联登出处理器，由SSO Client端触发调用
 * 1、通过JWT得到username，再找到对应的sessionId，清除session
 * 2、异步通知所有SSO Client登出
 */
@Data
public class OAuth2SsoServerAutoLogoutHandler implements LogoutHandler {
    private static Logger logger = LoggerFactory.getLogger(OAuth2SsoServerAutoLogoutHandler.class);

    //@Value("${pangu.oauth2.sso.jwt.secret}")
    //private String jwtSecret;


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private OAuth2AuthorizationServerProperties oAuth2AuthorizationServerProperties;

    @Autowired
    private FindByIndexNameSessionRepository<? extends Session> sessionRepository;

    private static ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(5);

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        String userInfo = extractHeaderJwtToken(request);

        if(StringUtils.isEmpty(userInfo)){
            logger.warn("SSO Server auto logout userInfo is null");
        }

       /* // 检查jwt
        JWTUtil.validateToken(jwt, jwtSecret);

        // 从jwt获取username
        String username = JWTUtil.getUserNameFromJWT(jwt, jwtSecret);*/

        String username = new String(Base64.getDecoder().decode(userInfo));

        // 根据username获取其session集合
        Map<String, ? extends Session> usersSessions =
                sessionRepository.findByIndexNameAndIndexValue(
                        FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME,
                        username);

        Set<String> keySet = usersSessions.keySet();
        if(keySet!=null && !keySet.isEmpty()){
            Iterator iterator = keySet.iterator();
            while(iterator.hasNext()){
                String sessionId = (String)iterator.next();
                sessionRepository.deleteById(sessionId);
            }
        }

        // TODO 异步通知其它客户端logout
        Set<String> logoutUris = null;
        if(ArrayUtils.isNotEmpty(oAuth2AuthorizationServerProperties.getClients())){
            logoutUris = new HashSet<>();
            for(OAuth2ClientProperties client : oAuth2AuthorizationServerProperties.getClients()){
                logoutUris.addAll(Splitter.on(",").omitEmptyStrings().trimResults()
                        .splitToList(client.getLogoutUris()).stream().distinct().collect(Collectors.toList()));
            }
        }

        if (logoutUris != null && logoutUris.size()>0){
            for (String uris : logoutUris) {
                EXECUTOR_SERVICE.submit(()->{
                    MultiValueMap<String, String> header = new HttpHeaders();
                    header.add("ssoAutoLogout", userInfo);
                    HttpEntity httpEntity = new HttpEntity("", header);
                    restTemplate.postForObject(uris,
                            httpEntity,String.class);
                });
            }
        }

        /*MultiValueMap<String, String> header = new HttpHeaders();
        header.add("ssoAutoLogout", userInfo);
        HttpEntity httpEntity = new HttpEntity("", header);*/


        /*try {
            String result = restTemplate.postForObject("http://ssoclient.pangu.com:10001/ssoAutoLogout",
                    httpEntity,String.class);
            logger.info("result==" + result);
        }
        catch(Exception e){
            logger.error("", e);
        }

        try {
            String result = restTemplate.postForObject("http://apigateway.pangu.com:9041/ssoAutoLogout",
                    httpEntity,String.class);
            logger.info("result==" + result);
        }
        catch(Exception e){
            logger.error("", e);
        }*/

    }


    private String extractHeaderJwtToken(HttpServletRequest request){
        Enumeration<String> headers = request.getHeaders("ssoAutoLogout");
        while (headers.hasMoreElements()) {
            String value = headers.nextElement();

            if(StringUtils.hasText(value)){
                return value;
            }
        }


        return null;
    }
}
