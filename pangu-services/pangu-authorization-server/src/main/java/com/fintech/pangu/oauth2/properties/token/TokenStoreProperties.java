package com.fintech.pangu.oauth2.properties.token;

import lombok.Data;

@Data
public class TokenStoreProperties {

    /**
     * 是否开启读写混合TokenStore，默认true
     */
    private boolean enableReadWriteCompositeTokenStore = true;

    /**
     * 负责读的TokenStore类型
     */
    private String[] readTokenStoreType;

    /**
     * 负责读写的TokenStore类型
     */
    private String[] writeTokenStoreType;

    /**
     * token存储前缀
     */
    private String prefix;


}
