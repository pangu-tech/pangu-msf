package com.fintech.pangu.oauth2.model.dto;

import lombok.Data;

import java.util.List;

/**
 * @Description:
 */
@Data
public class SystemInfoDto {

    private List<String> ssoClientUris;
}
