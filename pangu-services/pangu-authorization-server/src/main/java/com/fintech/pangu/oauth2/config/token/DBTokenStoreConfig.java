package com.fintech.pangu.oauth2.config.token;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;

/**
 * 数据库TokenStore配置
 */
@Configuration
@ConditionalOnClass(DataSource.class)
public class DBTokenStoreConfig {

    @Autowired
    private DataSource dataSource;

    /**
     * 数据库存储令牌
     * @return
     */
    @Bean
    public TokenStore jdbcTokenStore(){
        JdbcTokenStore jdbcTokenStore = new JdbcTokenStore(dataSource);
        return jdbcTokenStore;
    }

}
