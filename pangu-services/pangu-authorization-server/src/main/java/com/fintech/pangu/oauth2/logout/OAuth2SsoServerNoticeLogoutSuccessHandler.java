package com.fintech.pangu.oauth2.logout;

import com.alibaba.fastjson.JSON;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.oauth2.model.dto.SystemInfoDto;
import com.fintech.pangu.oauth2.model.vo.SystemInfoResponse;
import com.fintech.pangu.oauth2.properties.OAuth2AuthorizationServerProperties;
import com.fintech.pangu.oauth2.properties.client.OAuth2ClientProperties;
import com.google.common.base.Splitter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * SSO Server端基础登出/logout时使用，SSO Server登出后通知所有的客户端自动登出
 */
@Slf4j
public class OAuth2SsoServerNoticeLogoutSuccessHandler implements LogoutSuccessHandler {

    @Autowired
    private OAuth2AuthorizationServerProperties oAuth2AuthorizationServerProperties;

    public OAuth2SsoServerNoticeLogoutSuccessHandler(){}

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        boolean isNoticeOtherClient = StringUtils.isNotEmpty(request.getParameter("isNoticeOtherClient")) ? Boolean.valueOf(request.getParameter("isNoticeOtherClient")) : true;
        String clientId = request.getParameter("clientId");

        Set<String> logoutUris = null;
        if(ArrayUtils.isNotEmpty(oAuth2AuthorizationServerProperties.getClients())){
            logoutUris = new HashSet<>();
            for(OAuth2ClientProperties client : oAuth2AuthorizationServerProperties.getClients()){
                if(Objects.equals(clientId,client.getClientId())){
                    continue;
                }
                if(StringUtils.isNotEmpty(client.getLogoutUris())){
                    logoutUris.addAll(Splitter.on(",").omitEmptyStrings().trimResults()
                            .splitToList(client.getLogoutUris()).stream().distinct().collect(Collectors.toList()));
                }
            }
        }
        List<String> ssoClientUris = null;
        String origin = request.getHeader("origin");
        if (logoutUris != null
                && logoutUris.size()>0
                && StringUtils.isNotEmpty(origin)){
            ssoClientUris = logoutUris.stream().filter(s->!s.startsWith(origin)).collect(Collectors.toList());
        }
        SystemInfoDto systemInfoDto = new SystemInfoDto();
        systemInfoDto.setSsoClientUris(ssoClientUris);

        response.setStatus(HttpServletResponse.SC_OK);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.write(JSON.toJSONString(isNoticeOtherClient ? SystemInfoResponse.builder().systemInfo(systemInfoDto).build(): BaseResponse.success()));
        out.flush();
        out.close();
    }
}
