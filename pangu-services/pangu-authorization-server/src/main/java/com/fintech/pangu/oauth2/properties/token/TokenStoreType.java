package com.fintech.pangu.oauth2.properties.token;

public class TokenStoreType {

    public static final String db = "db";

    public static final String redis = "redis";

}
