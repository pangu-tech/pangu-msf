package com.fintech.pangu.oauth2.logout;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * SSO Server端的自动级联登出成功处理器
 * 由SSO Client端触发调用，AutoLogoutSuccessHandler告知Client自动登出结果
 */
public class OAuth2SsoServerAutoLogoutSuccessHandler implements LogoutSuccessHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setStatus(HttpStatus.OK.value());
        response.getWriter().write("SSO Server AutoLogout Success");
        response.getWriter().flush();
    }

}
