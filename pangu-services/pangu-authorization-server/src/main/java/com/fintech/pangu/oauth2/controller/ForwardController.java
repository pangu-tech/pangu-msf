package com.fintech.pangu.oauth2.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URLDecoder;

/**
 * 页面跳转Controller
 */
@Controller
public class ForwardController {

    @RequestMapping("/toLogin")
    public String toLogin(@RequestAttribute(required = false) String msg,
                          @RequestAttribute(required = false) String userName,
                          Model model){
        if (StringUtils.isNotEmpty(msg)){
            try {
                msg = URLDecoder.decode(msg,"utf-8");
                model.addAttribute("msg",msg);
                model.addAttribute("userName",userName);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return "login";
    }

}
