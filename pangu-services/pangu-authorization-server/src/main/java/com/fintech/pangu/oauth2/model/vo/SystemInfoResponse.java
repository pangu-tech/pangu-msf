package com.fintech.pangu.oauth2.model.vo;

import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.oauth2.model.dto.SystemInfoDto;
import lombok.Builder;
import lombok.Data;

/**
 * @Description:
 */
@Data
@Builder
public class SystemInfoResponse extends BaseResponse {

    private SystemInfoDto systemInfo;
}
