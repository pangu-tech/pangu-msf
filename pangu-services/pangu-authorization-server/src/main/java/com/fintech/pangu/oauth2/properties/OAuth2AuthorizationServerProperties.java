package com.fintech.pangu.oauth2.properties;

import com.fintech.pangu.oauth2.properties.client.OAuth2ClientProperties;
import com.fintech.pangu.oauth2.properties.token.TokenStoreProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@ConfigurationProperties( prefix = "pangu.security.oauth2.server" )
@Data
public class OAuth2AuthorizationServerProperties {

    /**
     * Token存储
     */
    @NestedConfigurationProperty
    private TokenStoreProperties tokenStore = new TokenStoreProperties();

    /**
     * OAuth2客户端配置
     */
    @NestedConfigurationProperty
    private OAuth2ClientProperties[] clients;

}
