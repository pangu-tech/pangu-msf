package com.fintech.pangu.oauth2.properties.client;

import lombok.Data;

@Data
public class OAuth2ClientProperties {

    /**
     * OAuth2 clientId
     */
    private String clientId;

    /**
     * OAuth2 client secret
     */
    private String clientSecret;

    /**
     * OAuth2 client 重定向地址
     */
    private String redirectUris;

    /**
     * OAuth2 client 登出地址
     */
    private String logoutUris;

    /**
     * access token有效时间，默认12小时
     */
    private int accessTokenValidateSeconds = 60 * 60 * 12;

    /**
     * refresh token有效时间，默认30天
     */
    private int refreshTokenValiditySeconds = 60 * 60 * 24 * 30;

}
