// js
var username = document.getElementById('normal_login_username');
var password = document.getElementById('normal_login_password');
var userNameTip = document.getElementById('userNameTip');
var passwordTip = document.getElementById('passwordTip');

username.blur = function () {
  if(username.value !== ''){
    username.style.borderColor = '#d9d9d9';
    userNameTip.innerHTML = '';
  }
};

password.blur = function () {
  if(password.value !== ''){
    password.style.borderColor = '#d9d9d9';
    passwordTip.innerHTML = '';
  }
};

document.onkeydown = function (event) {
  var e = event || window.event;
  if (e && e.keyCode == 13) { //回车键的键值为13
    login();
  }
};

// 提交验证
function login(){
  // e.preventDefault();
  var isSucc = formCheck();
  if (!isSucc){
    return false;
  }
  // 发送ajax请求验证是否已经登录
  var xhr = createXMLHTTPRequest();
  if(xhr){
    xhr.withCredentials = true;
    xhr.open("GET", "oauth/resource/userInfo", true);
    xhr.setRequestHeader("X-Requested-With","XMLHttpRequest");
    xhr.onreadystatechange = function(){
      if(xhr.readyState == 4){
        var msg = {};
        if(xhr.status == 200){
            var data = JSON.parse(xhr.responseText);
            if (data.retCode === '0000'){
              msg.retCode = '0000';
              msg.reload = true;
              window.parent.postMessage(JSON.stringify(msg),'*');
            } else {
              document.getElementById("_loginForm").submit();
            }
        }else{
          msg.retCode = '0001';
          msg.retDesc = '系统异常，请联系管理员.';
          window.parent.postMessage(JSON.stringify(msg),'*');
        }
      }
    }
    xhr.send(null);
  }
}

function formCheck() {
  var username = document.getElementById('normal_login_username');
  var password = document.getElementById('normal_login_password');
  var userNameTip = document.getElementById('userNameTip');
  var passwordTip = document.getElementById('passwordTip');

  if(username.value === ''){
    username.style.borderColor = '#f5222d';
    userNameTip.innerHTML = '请输入用户名！';
    return false;
  }else{
    username.style.borderColor = '#d9d9d9';
    userNameTip.innerHTML = '';
  }

  if(password.value === ''){
    password.style.borderColor = '#f5222d';
    passwordTip.innerHTML = '请输入密码！';
    return false;
  }else{
    password.style.borderColor = '#d9d9d9';
    passwordTip.innerHTML = ''
  }
  // 加密密码
  password.value = encryptPasswordByAES(password.value);
  return true;
}

/*
* key与iv需与主数据保持一致，请勿随意修改
* */
function encryptPasswordByAES(password) {
  var key = CryptoJS.enc.Utf8.parse("abcdnnnnnn123456");
  var iv = CryptoJS.enc.Utf8.parse("2015030120123456");
  var encrypted = CryptoJS.AES.encrypt(password, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7,
  });
  return encrypted.toString();
}

function createXMLHTTPRequest() {
  //1.创建XMLHttpRequest对象
  //这是XMLHttpReuquest对象无部使用中最复杂的一步
  //需要针对IE和其他类型的浏览器建立这个对象的不同方式写不同的代码
  var xmlHttpRequest;
  if (window.XMLHttpRequest) {
    //针对FireFox，Mozillar，Opera，Safari，IE7，IE8
    xmlHttpRequest = new XMLHttpRequest();
    //针对某些特定版本的mozillar浏览器的BUG进行修正
    if (xmlHttpRequest.overrideMimeType) {
      xmlHttpRequest.overrideMimeType("text/xml");
    }
  } else if (window.ActiveXObject) {
    //针对IE6，IE5.5，IE5
    //两个可以用于创建XMLHTTPRequest对象的控件名称，保存在一个js的数组中
    //排在前面的版本较新
    var activexName = [ "MSXML2.XMLHTTP", "Microsoft.XMLHTTP" ];
    for ( var i = 0; i < activexName.length; i++) {
      try {
        //取出一个控件名进行创建，如果创建成功就终止循环
        //如果创建失败，回抛出异常，然后可以继续循环，继续尝试创建
        xmlHttpRequest = new ActiveXObject(activexName[i]);
        if(xmlHttpRequest){
          break;
        }
      } catch (e) {
      }
    }
  }
  return xmlHttpRequest;
}