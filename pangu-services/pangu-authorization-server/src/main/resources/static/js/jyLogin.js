/* eslint-disable */
function createUrl(conf) {
  var urlArr = [];
  for (var key in conf) {
    var val = conf[key];
    if(key !== 'src') {
      urlArr.push([key,val].join('='))
    }
  }
  return conf['src'] + '?' + urlArr.join('&')
}


!function(win, doc) {
  function JyLogin(conf) {
    var iframe = doc.createElement('iframe');
    // var src = conf.src;
    // var src = conf.src + '?appid=' + conf.appid + '&scope=' + conf.scope + '&redirect_uri=' + conf.redirect_uri + '&state=' + conf.state + '&login_type=jssdk&self_redirect=' + c + '&styletype=' + (conf.styletype || '') + '&sizetype=' + (conf.sizetype || '') + '&bgcolor=' + (conf.bgcolor || '') + '&rst=' + (conf.rst || '');
    // src += conf.style ? '&style=' + conf.style : '';
    // src += conf.href ? '&href=' + conf.href : '';
    iframe.src = createUrl(conf);
    iframe.frameBorder = '0';
    iframe.allowTransparency = 'true';
    iframe.scrolling = 'no';
    iframe.width = '360px';
    iframe.height = '375.5px';
    var container = doc.getElementById(conf.id);
    iframe.onload=function () {
      var msg = {};
      msg.iframeOnLoad=true;
      window.parent.postMessage(JSON.stringify(msg),'*');
    }
    container.innerHTML = '';
    container.appendChild(iframe);
  }
  win.JyLogin = JyLogin;
}(window, document);
