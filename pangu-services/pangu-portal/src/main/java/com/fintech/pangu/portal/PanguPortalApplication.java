package com.fintech.pangu.portal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PanguPortalApplication {

    public static void main(String[] args) {
        SpringApplication.run(PanguPortalApplication.class, args);
    }

}
