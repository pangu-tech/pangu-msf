package com.fintech.pangu.portal.conf;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@ConfigurationProperties(prefix = "pangu.portal")
public class PortalConfig {

    List<SystemVO> system = new ArrayList<>();

   public static class SystemVO{
       private String imgUrl;
       private String httpUrl;
       private String name;

       public String getName() {
           return name;
       }

       public void setName(String name) {
           this.name = name;
       }

       public String getImgUrl() {
           return imgUrl;
       }

       public void setImgUrl(String imgUrl) {
           this.imgUrl = imgUrl;
       }

       public String getHttpUrl() {
           return httpUrl;
       }

       public void setHttpUrl(String httpUrl) {
           this.httpUrl = httpUrl;
       }
   }

    public List<SystemVO> getSystem() {
        return system;
    }

    public void setSystem(List<SystemVO> system) {
        this.system = system;
    }
}
