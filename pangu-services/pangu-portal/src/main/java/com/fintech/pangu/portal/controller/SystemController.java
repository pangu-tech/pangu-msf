package com.fintech.pangu.portal.controller;

import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.portal.conf.PortalConfig;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class SystemController {

    @Resource
    private PortalConfig portalConfig;

    @GetMapping("/api/systemInfo")
    public BaseResponse getSystemList(){
        List<PortalConfig.SystemVO> systemInfo = portalConfig.getSystem();
        return new BaseResponse(systemInfo);
    }
}
