package com.fintech.pangu.client;

import com.ctrip.framework.apollo.openapi.client.constant.ApolloOpenApiConstants;
import com.ctrip.framework.apollo.openapi.dto.NamespaceReleaseDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenGrayReleaseRuleDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenItemDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenNamespaceDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenReleaseDTO;
import com.fintech.pangu.GrayReleaseSeverApplication;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.model.vo.GrayReleaseVO;
import com.fintech.pangu.service.GrayReleaseService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

/**
 * @Description:
 */
public class GrayReleaseOpenApiClient {

    private final String portalUrl;

    private final String token;

    private final GrayReleaseService grayReleaseService;

    public GrayReleaseOpenApiClient(String portalUrl,String token){
        this.portalUrl = portalUrl;
        this.token = token;

        Gson gson = new GsonBuilder().setDateFormat(ApolloOpenApiConstants.JSON_DATE_FORMAT).create();
        String baseUrl = this.portalUrl + ApolloOpenApiConstants.OPEN_API_V1_PREFIX;

        RestTemplate restTemplate =(RestTemplate) GrayReleaseSeverApplication.applicationContext.getBean("restTemplate");
        restTemplate.setInterceptors(Collections.singletonList((httpRequest, bytes, clientHttpRequestExecution) -> {
            HttpHeaders headers = httpRequest.getHeaders();
            headers.add(HttpHeaders.AUTHORIZATION,token);
            return clientHttpRequestExecution.execute(httpRequest, bytes);
        }));

        grayReleaseService = new GrayReleaseService(baseUrl,gson,restTemplate);
    }

    public static GrayReleaseOpenApiClientBuilder builder(){
        return new GrayReleaseOpenApiClientBuilder();
    }

    public static class GrayReleaseOpenApiClientBuilder{
        private String portalUrl;
        private String token;

        public GrayReleaseOpenApiClientBuilder withPortalUrl(String portalUrl) {
            this.portalUrl = portalUrl;
            return this;
        }

        public GrayReleaseOpenApiClientBuilder withToken(String token) {
            this.token = token;
            return this;
        }

        public GrayReleaseOpenApiClient build(){
            return new GrayReleaseOpenApiClient(portalUrl, token);
        }
    }

    public OpenNamespaceDTO findBranch(String appId,String env,String clusterName,String namespaceName){
        return grayReleaseService.findBranch(appId, env, clusterName, namespaceName);
    }

    public OpenNamespaceDTO createBranch(String appId,String env,String clusterName,String namespaceName,String operator){
        return grayReleaseService.createBranch(appId, env, clusterName, namespaceName,operator);
    }

    public OpenItemDTO createItem(String appId,String env,String clusterName,String namespaceName,OpenItemDTO itemDTO){
        return grayReleaseService.createItem(appId,env,clusterName,namespaceName,itemDTO);
    }

    public void updateBranchRules(String appId, String env, String clusterName, String namespaceName, String branchName, OpenGrayReleaseRuleDTO rules, String operator){
        grayReleaseService.updateBranchRules(appId,env,clusterName,namespaceName,branchName,rules,operator);
    }

    public OpenReleaseDTO publishGrayRelease(String appId, String env, String clusterName, String namespaceName, String branchName, NamespaceReleaseDTO releaseDTO){
        return grayReleaseService.publishGrayRelease(appId,env,clusterName,namespaceName,branchName,releaseDTO);
    }


    public BaseResponse grayRelease(GrayReleaseVO grayReleaseVO){
        return grayReleaseService.createAndGrayRelease(grayReleaseVO);
    }

}
