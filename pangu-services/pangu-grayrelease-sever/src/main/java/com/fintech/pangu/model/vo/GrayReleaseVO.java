package com.fintech.pangu.model.vo;

import com.ctrip.framework.apollo.openapi.dto.OpenGrayReleaseRuleItemDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenItemDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Set;

/**
 * @Description:
 */
@Data
public class GrayReleaseVO {

    @ApiModelProperty(value = "基于第三方应用创建的token", name = "token",required = true)
    private String token;

    @ApiModelProperty(value = "应用的appid", name = "appId",required = true)
    private String appId;

    @ApiModelProperty(value = "环境信息", name = "env",required = true)
    private String env;

    @ApiModelProperty(value = "集群信息", name = "clusterName", example = "default", required = false)
    private String clusterName;

    @ApiModelProperty(value = "命名空间", name = "namespaceName", example = "apilication", required = false)
    private String namespaceName;

    @ApiModelProperty(value = "操作者", name = "operator", required = true)
    private String operator;

    @ApiModelProperty(value = "配置信息", name = "item",  required = true)
    private OpenItemDTO item;

    @ApiModelProperty(value = "灰度规则信息", name = "ruleItems", required = true)
    private Set<OpenGrayReleaseRuleItemDTO> ruleItems;
}
