package com.fintech.pangu.controller;

import com.ctrip.framework.apollo.openapi.client.ApolloOpenApiClient;
import com.ctrip.framework.apollo.openapi.dto.NamespaceReleaseDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenGrayReleaseRuleDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenItemDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenNamespaceDTO;
import com.ctrip.framework.apollo.openapi.dto.OpenReleaseDTO;
import com.fintech.pangu.client.GrayReleaseOpenApiClient;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.model.vo.GrayReleaseVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @Description:
 */
@Api(tags = "配置中心apollo灰度发布服务")
@Slf4j
@RestController
@RequestMapping("/apollo/openapi/grayscale")
public class GrayReleaseController {

    @Value("${apollo.portalUrl}")
    private String portalUrl;

    @ApiIgnore
    @PostMapping("/findBranch")
    public OpenNamespaceDTO findBranch(@RequestParam String token,
                                       @RequestParam String appId,
                                       @RequestParam String env,
                                       @RequestParam(required = false) String clusterName,
                                       @RequestParam(required = false) String namespaceName){
        GrayReleaseOpenApiClient grayReleaseOpenApiClient = GrayReleaseOpenApiClient.builder().withPortalUrl(portalUrl).withToken(token).build();
        return grayReleaseOpenApiClient.findBranch(appId,env,clusterName,namespaceName);
    }

    @ApiIgnore
    @PostMapping("/createBranch")
    public OpenNamespaceDTO createBranch(@RequestParam String token,
                                       @RequestParam String appId,
                                       @RequestParam String env,
                                       @RequestParam String operator,
                                       @RequestParam(required = false) String clusterName,
                                       @RequestParam(required = false) String namespaceName){
        GrayReleaseOpenApiClient grayReleaseOpenApiClient = GrayReleaseOpenApiClient.builder()
                .withPortalUrl(portalUrl)
                .withToken(token)
                .build();
        return grayReleaseOpenApiClient.createBranch(appId,env,clusterName,namespaceName,operator);
    }

    @ApiIgnore
    @PostMapping("/createItem")
    public OpenItemDTO createItem(@RequestParam String token,
                                  @RequestParam String appId,
                                  @RequestParam String env,
                                  @RequestParam(required = false) String clusterName,
                                  @RequestParam(required = false) String namespaceName,
                                  @RequestBody OpenItemDTO itemDTO){
        ApolloOpenApiClient client = ApolloOpenApiClient.newBuilder()
                .withPortalUrl(portalUrl)
                .withToken(token)
                .build();
        return client.createItem(appId,env,clusterName,namespaceName,itemDTO);
    }

    @ApiIgnore
    @PostMapping("/updateBranchRules")
    public BaseResponse updateBranchRules(@RequestParam String token,
                                          @RequestParam String appId,
                                          @RequestParam String env,
                                          @RequestParam(required = false) String clusterName,
                                          @RequestParam(required = false) String namespaceName,
                                          @RequestParam String branchName,
                                          @RequestBody OpenGrayReleaseRuleDTO rules,
                                          @RequestParam String operator){
        GrayReleaseOpenApiClient grayReleaseOpenApiClient = GrayReleaseOpenApiClient.builder()
                .withPortalUrl(portalUrl)
                .withToken(token)
                .build();
        grayReleaseOpenApiClient.updateBranchRules(appId,env,clusterName,namespaceName,branchName,rules,operator);
        return BaseResponse.success();
    }

    @ApiIgnore
    @PostMapping("/publishGrayRelease")
    public OpenReleaseDTO publishGrayRelease(@RequestParam String token,
                                             @RequestParam String appId,
                                             @RequestParam String env,
                                             @RequestParam(required = false) String clusterName,
                                             @RequestParam(required = false) String namespaceName,
                                             @RequestParam String branchName,
                                             @RequestBody NamespaceReleaseDTO releaseDTO){
        GrayReleaseOpenApiClient grayReleaseOpenApiClient = GrayReleaseOpenApiClient.builder()
                .withPortalUrl(portalUrl)
                .withToken(token)
                .build();
        return grayReleaseOpenApiClient.publishGrayRelease(appId,env,clusterName,namespaceName,branchName,releaseDTO);
    }

    @ApiOperation(value = "灰度发布", notes = "创建并发布灰度配置", response = BaseResponse.class)
    @PostMapping("/createAndGrayRelease")
    public BaseResponse createAndGrayRelease(@RequestBody GrayReleaseVO grayReleaseVO){
        GrayReleaseOpenApiClient grayReleaseOpenApiClient = GrayReleaseOpenApiClient.builder()
                .withPortalUrl(portalUrl)
                .withToken(grayReleaseVO.getToken())
                .build();
        return grayReleaseOpenApiClient.grayRelease(grayReleaseVO);

    }

}
