package com.fintech.pangu;

import com.fintech.pangu.apollo.annotation.EnablePanGuApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnablePanGuApolloConfig
public class GrayReleaseSeverApplication {

    public static ConfigurableApplicationContext applicationContext;

    public static void main(String[] args) {
        GrayReleaseSeverApplication.applicationContext = SpringApplication.run(GrayReleaseSeverApplication.class, args);
    }

}
