## 目的
> 该项目实现微服务体系下基于jekins、配置中心apollo灰度发布服务的功能。

## 接口定义
### 1. 创建并发布配置接口 

+ 请求url:
    
    > http://ip:port/apollo/openapi/grayscale/createAndGrayRelease
    
+ 请求类型:
    
    > POST
    
+ 请求的ContentType
    
    > application/json
    
+ 请求参数: 
  
    ```json
  {
  	"token":"191a2c4861e7f5c5a63d8e1e538936376f48c725",
  	"appId":"apollo-test",
  	"env":"dev",
  	"operator":"apollo",
  	"item":{
  		"key":"timeout",
  		"value":"3000",
  		"comment":"超时时间"
  	},
  	"ruleItems":[{
  		"clientIpList":["192.168.69.66","192.168.69.67"]
  	}]
  }
    ```

> **说明：1.该项目需依赖配置中心apollo的portal、configservice、adminservice服务的1.4.0+版本.<br>
          2.在配置中心管理端，给第三方应用赋权时，赋权类型应选择App**
            
