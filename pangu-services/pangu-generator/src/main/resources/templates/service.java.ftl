package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};
import ${cfg.customConfig.basePackage}.web.dto.${entity}Dto;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName}{

    ${entity}Dto get${entity}ById(Long id);

    IPage<${entity}Dto> listForPage(${entity}Dto ${entity?uncap_first}Dto);

    int add(${entity}Dto ${entity?uncap_first}Dto);

    int update(${entity}Dto ${entity?uncap_first}Dto);

    int delete(Long id);
}
</#if>
