# 基础配置
server.port = 30000
spring.application.name = ${cfg.customConfig.artifactId}
server.servlet.context-path = /${cfg.customConfig.artifactId}

# 数据源配置
spring.datasource.driver-class-name = com.mysql.jdbc.Driver
spring.datasource.url = ${cfg.dataSource.url}
spring.datasource.username = ${cfg.dataSource.userName}
spring.datasource.password = ${cfg.dataSource.password}
spring.datasource.type = com.alibaba.druid.pool.DruidDataSource

#swagger配置
pangu.swagger2.enabled = true
pangu.swagger2.title = ${cfg.customConfig.artifactId}
pangu.swagger2.description = ${cfg.customConfig.descriptionUnicode}
pangu.swagger2.select-base-package = ${cfg.customConfig.basePackage}
pangu.swagger2.terms-of-service-url=

mybatis-plus.mapper-locations=classpath*:/mappers/**/*.xml
mybatis-plus.type-aliases-package=${package.Entity}
#mybatis-plus.type-aliases-super-type=com.baomidou.mybatisplus.extension.activerecord.Model

logging.config=classpath:logback-spring.xml



pangu.http.converter.fast-json.enabled=true

# 是否开启检查JWT Token，并从中恢复认证态
pangu.security.authentication.token.jwt.check-enable = true
# jwt签名秘钥
pangu.security.authentication.token.jwt.secret = my-secret
# 认证令牌返回类型，默认是cookie
pangu.security.authentication.token.response.return-type = cookie

pangu.security.http.csrf.enable = false

########## log ##########
logging.level.root = info
logging.level.org.springframework.jdbc.core.JdbcTemplate = debug
logging.level.org.springframework.security = debug
logging.level.org.springframework.web = debug
# request debug
logging.level.org.apache.coyote.http11 = debug
# response debug
logging.level.org.springframework.web.servlet.mvc = debug