package ${cfg.customConfig.basePackage}.api.bean.qo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
* @Description:
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("${table.comment!}分页请求Model类")
public class Fetch${entity}Request {

    @ApiModelProperty(name = "size",value = "当前页展示条数")
    private long size = 10;
    @ApiModelProperty(name = "current",value = "当前页数")
    private long current = 1;

<#list table.fields as field>
    <#if field.fetchGenerate?? && field.fetchGenerate == 1>
    @ApiModelProperty(name = "${field.propertyName}",value = "${field.comment}")
    private ${field.propertyType} ${field.propertyName};
    <#elseif field.fetchGenerate?? && field.fetchGenerate == 2>
    @ApiModelProperty(name = "${field.propertyName}",value = "${field.comment}")
    private ${field.propertyType} ${field.propertyName};
    @ApiModelProperty(name = "${field.propertyName}Start",value = "${field.comment}起始值")
    private ${field.propertyType} ${field.propertyName}Start;
    @ApiModelProperty(name = "${field.propertyName}End",value = "${field.comment}截止值")
    private ${field.propertyType} ${field.propertyName}End;
    <#elseif  field.fetchGenerate?? && field.fetchGenerate == 3>
    @ApiModelProperty(name = "${field.propertyName}",value = "${field.comment}")
    private ${field.propertyType} ${field.propertyName};
    </#if>
</#list>

}
