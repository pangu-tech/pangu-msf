package ${cfg.customConfig.basePackage}.web.mapper;

import org.mapstruct.Mapper;
import ${cfg.customConfig.basePackage}.web.dto.${entity}Dto;
import ${cfg.customConfig.basePackage}.api.bean.qo.CreateAndModify${entity}Request;
import ${cfg.customConfig.basePackage}.api.bean.qo.Fetch${entity}Request;
import ${cfg.customConfig.basePackage}.api.bean.vo.${entity}Result;
import ${package.Entity}.${entity};
import java.util.List;

@Mapper(componentModel="spring")
public interface ${entity}Mapper {

    ${entity}Dto fetchReq2Dto(Fetch${entity}Request fetch${entity}Request);

    ${entity}Dto createAndModify2Dto(CreateAndModify${entity}Request createAndModify${entity}Request);

    ${entity} dto2Po(${entity}Dto ${entity}Dto);

    ${entity}Dto po2Dto(${entity} ${entity?uncap_first});

    List<${entity}Dto> po2DtoList(List<${entity}> ${entity?uncap_first}List);

    ${entity}Result dto2Result(${entity}Dto ${entity?uncap_first}Dto);

    List<${entity}Result> dto2ResultList(List<${entity}Dto> ${entity?uncap_first}Dtos);

}
