<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    
    <parent>
        <groupId>com.fintech.platform</groupId>
        <artifactId>pangu-services</artifactId>
        <version>${cfg.customConfig.parentVersion}</version>
    </parent>

    <artifactId>${cfg.customConfig.artifactId}</artifactId>
    <packaging>pom</packaging>

    
    <modules>
		<module>${cfg.customConfig.artifactId}-api</module>
    	<module>${cfg.customConfig.artifactId}-feign</module>
        <module>${cfg.customConfig.artifactId}-service</module>
    </modules>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>