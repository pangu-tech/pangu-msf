package com.fintech.config;

import com.fintech.pangu.web.config.PanGuResourceHandler;
import org.springframework.stereotype.Component;

/**
 * @Description:
 */
@Component
public class SwaggerResourceHandler extends PanGuResourceHandler {

    @Override
    public String[] pathPatterns() {
        return new String[]{"/doc.html","/webjars/**"};
    }

    @Override
    public String[] resourceLocations() {
        return new String[]{"classpath:/META-INF/resources/","classpath:/META-INF/resources/webjars/"};
    }
}
