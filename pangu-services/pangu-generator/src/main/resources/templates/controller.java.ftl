package ${package.Controller};

import ${cfg.customConfig.basePackage}.api.interfaces.I${entity}Api;
import ${cfg.customConfig.basePackage}.web.service.${entity}Service;
import ${cfg.customConfig.basePackage}.web.mapper.${entity}Mapper;
import ${cfg.customConfig.basePackage}.web.dto.${entity}Dto;
import ${cfg.customConfig.basePackage}.api.bean.vo.${entity}Result;
import ${cfg.customConfig.basePackage}.api.bean.qo.CreateAndModify${entity}Request;
import ${cfg.customConfig.basePackage}.api.bean.qo.Fetch${entity}Request;
import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.commons.api.PageUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * <p>
 * ${table.comment!} 控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */

@RestController
@Api(tags = {"${table.comment!}接口"})
public class ${table.controllerName} implements I${entity}Api {

    @Autowired
    private ${entity}Service ${entity?uncap_first}Service;
    @Autowired
    private ${entity}Mapper ${entity?uncap_first}Mapper;

    @Override
    @ApiOperation(value = "分页查找数据", notes = "分页获取数据；current从1开始，默认为1；size默认：10")
    public BaseResponse<IPage<${entity}Result>> listForPage(Fetch${entity}Request fetch${entity}Request){
        // req --> dto
        ${entity}Dto ${entity?uncap_first}Dto = ${entity?uncap_first}Mapper.fetchReq2Dto(fetch${entity}Request);
        // 分页查询
        IPage<${entity}Dto> ${entity?uncap_first}IPage = ${entity?uncap_first}Service.listForPage(${entity?uncap_first}Dto);
        // dto --> result
        List<${entity}Dto> records = ${entity?uncap_first}IPage.getRecords();
        List<${entity}Result> listResult = ${entity?uncap_first}Mapper.dto2ResultList(records);
        // 构建返回
        IPage<${entity}Result> result = PageUtil.builder(listResult, ${entity?uncap_first}IPage);
        return new BaseResponse(result);
    }

    @Override
    @ApiOperation(value = "通过id查找", notes = "ID必传")
    public BaseResponse<${entity}Result> get${entity}ById(@PathVariable(value = "id") Long id){
        ${entity}Dto getByIdDto = ${entity?uncap_first}Service.get${entity}ById(id);
        ${entity}Result ${entity?uncap_first}Result = ${entity?uncap_first}Mapper.dto2Result(getByIdDto);
        return new BaseResponse(${entity?uncap_first}Result);
    }

    @Override
    @ApiOperation(value = "添加数据", notes = "添加${table.comment!}信息")
    public BaseResponse<Integer> add(@RequestBody CreateAndModify${entity}Request createAndModify${entity}Request){
        ${entity}Dto ${entity?uncap_first}Dto = ${entity?uncap_first}Mapper.createAndModify2Dto(createAndModify${entity}Request);
        int add = ${entity?uncap_first}Service.add(${entity?uncap_first}Dto);
        return new BaseResponse(add);
    }

    @Override
    @ApiOperation(value = "修改数据", notes = "修改${table.comment!}信息，ID必传")
    public BaseResponse<Integer> update(@PathVariable(value = "id") Long id, @RequestBody CreateAndModify${entity}Request createAndModify${entity}Request){
        ${entity}Dto ${entity?uncap_first}Dto = ${entity?uncap_first}Mapper.createAndModify2Dto(createAndModify${entity}Request);
        ${entity?uncap_first}Dto.setId(id);
        int update = ${entity?uncap_first}Service.update(${entity?uncap_first}Dto);
        return new BaseResponse(update);
    }

    @Override
    @ApiOperation(value = "删除数据", notes = "删除${table.comment!}信息，ID必传")
    public BaseResponse<Integer> delete(@PathVariable(value = "id") Long id){
        int delete = ${entity?uncap_first}Service.delete(id);
        return new BaseResponse(delete);
    }
}