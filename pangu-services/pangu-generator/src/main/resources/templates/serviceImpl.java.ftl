package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${superServiceImplClassPackage};
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import ${cfg.customConfig.basePackage}.web.dto.${entity}Dto;
import ${cfg.customConfig.basePackage}.web.mapper.${entity}Mapper;
import org.springframework.transaction.annotation.Transactional;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fintech.pangu.commons.api.PageUtil;

import java.util.List;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} implements ${table.serviceName} {

    @Autowired
    private ${table.mapperName} ${table.mapperName?uncap_first};
    @Autowired
    private ${entity}Mapper ${entity?uncap_first}Mapper;

    @Override
    public ${entity}Dto get${entity}ById(Long id){
        ${entity} ${entity?uncap_first} = ${table.mapperName?uncap_first}.selectById(id);
        ${entity}Dto ${entity?uncap_first}Dto = ${entity?uncap_first}Mapper.po2Dto(${entity?uncap_first});
        return ${entity?uncap_first}Dto;
    }

    @Override
    public IPage<${entity}Dto> listForPage(${entity}Dto ${entity?uncap_first}Dto){
        //构建查询条件
        QueryWrapper<${entity}> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda()
        <#list table.fields as field>
        <#if field.fetchGenerate == 1>
            <#if field.propertyType == 'String'>
			.eq(StringUtils.isNotEmpty(${entity?uncap_first}Dto.get${field.propertyName?cap_first}()), ${entity}::get${field.propertyName?cap_first}, ${entity?uncap_first}Dto.get${field.propertyName?cap_first}())
            <#else>
			.eq(${entity?uncap_first}Dto.get${field.propertyName?cap_first}() != null, ${entity}::get${field.propertyName?cap_first}, ${entity?uncap_first}Dto.get${field.propertyName?cap_first}())
            </#if>
        <#elseif field.fetchGenerate == 2>
            <#if field.propertyType == 'String'>
			.ge(StringUtils.isNotEmpty(${entity?uncap_first}Dto.get${field.propertyName?cap_first}Start()), ${entity}::get${field.propertyName?cap_first}, ${entity?uncap_first}Dto.get${field.propertyName?cap_first}Start())
			.lt(StringUtils.isNotEmpty(${entity?uncap_first}Dto.get${field.propertyName?cap_first}End()), ${entity}::get${field.propertyName?cap_first}, ${entity?uncap_first}Dto.get${field.propertyName?cap_first}End())
            <#else>
			.ge(${entity?uncap_first}Dto.get${field.propertyName?cap_first}Start() != null, ${entity}::get${field.propertyName?cap_first}, ${entity?uncap_first}Dto.get${field.propertyName?cap_first}Start())
			.lt(${entity?uncap_first}Dto.get${field.propertyName?cap_first}End() != null, ${entity}::get${field.propertyName?cap_first}, ${entity?uncap_first}Dto.get${field.propertyName?cap_first}End())
            </#if>
        <#elseif field.fetchGenerate == 3>
            <#if field.propertyType == 'String'>
			.like(StringUtils.isNotEmpty(${entity?uncap_first}Dto.get${field.propertyName?cap_first}()), ${entity}::get${field.propertyName?cap_first}, ${entity?uncap_first}Dto.get${field.propertyName?cap_first}())
            <#else>
			.like(${entity?uncap_first}Dto.get${field.propertyName?cap_first}() != null, ${entity}::get${field.propertyName?cap_first}, ${entity?uncap_first}Dto.get${field.propertyName?cap_first}())
            </#if>
        </#if>
        </#list>;
        //分页查询
        Page page = new Page(${entity?uncap_first}Dto.getCurrent(), ${entity?uncap_first}Dto.getSize());
        IPage<${entity}> ${entity?uncap_first}Page = ${table.mapperName?uncap_first}.selectPage(page, queryWrapper);
        //构建返回
        List<${entity}> records = ${entity?uncap_first}Page.getRecords();
        List<${entity}Dto> ${entity?uncap_first}List = ${entity?uncap_first}Mapper.po2DtoList(records);
        IPage<${entity}Dto> result = PageUtil.builder(${entity?uncap_first}List, ${entity?uncap_first}Page);
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int add(${entity}Dto ${entity?uncap_first}Dto){
        ${entity} ${entity?uncap_first} = ${entity?uncap_first}Mapper.dto2Po(${entity?uncap_first}Dto);
        int insert = ${table.mapperName?uncap_first}.insert(${entity?uncap_first});
        return insert;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int update(${entity}Dto ${entity?uncap_first}Dto){
        ${entity} ${entity?uncap_first} = ${entity?uncap_first}Mapper.dto2Po(${entity?uncap_first}Dto);
        int update = ${table.mapperName?uncap_first}.updateById(${entity?uncap_first});
        return update;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int delete(Long id){
        int delete = ${table.mapperName?uncap_first}.deleteById(id);
        return delete;
    }
}
</#if>
