<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>com.fintech.platform</groupId>
        <artifactId>${cfg.customConfig.artifactId}</artifactId>
        <version>${cfg.customConfig.parentVersion}</version>
    </parent>

    <artifactId>${cfg.customConfig.artifactId}-service</artifactId>
    <version>${cfg.customConfig.version}</version>

    <description>
        ${cfg.customConfig.description}服务service接口
    </description>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>1.8</java.version>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.fintech.platform</groupId>
            <artifactId>${cfg.customConfig.artifactId}-api</artifactId>
            <version>${cfg.customConfig.version}</version>
        </dependency>
        <dependency>
            <groupId>com.fintech.platform</groupId>
            <artifactId>pangu-web-starter</artifactId>
        </dependency>

        <dependency>
            <groupId>com.fintech.platform</groupId>
            <artifactId>pangu-actuator-starter</artifactId>
        </dependency>

        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.2.0</version>
        </dependency>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>druid</artifactId>
        </dependency>
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
        </dependency>
        <dependency>
            <groupId>org.mapstruct</groupId>
            <artifactId>mapstruct-jdk8</artifactId>
            <version>1.3.0.Final</version>
        </dependency>
        <dependency>
            <groupId>org.mapstruct</groupId>
            <artifactId>mapstruct-processor</artifactId>
            <version>1.3.0.Final</version>
        </dependency>
    </dependencies>

    <build>
        <finalName>${r'${project.artifactId}'}</finalName>
        <plugins>
            <!-- springboot可执行jar -->
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <!-- 跳过deploy -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <configuration>
                    <skip>true</skip>
                </configuration>
            </plugin>
        </plugins>

        <resources>
            <resource>
                <directory>${r'${basedir}'}/src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>static/**</include>
                    <include>templates/**</include>
                    <include>logback*.xml</include>
                </includes>
            </resource>
        </resources>
    </build>
</project>
