package ${cfg.customConfig.basePackage}.api.bean.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
<#if entityLombokModel>
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.sql.Timestamp;

</#if>

/**
* <p>
    * ${table.comment!}
    * </p>
*
* @author ${author}
* @since ${date}
*/
@Data
@ApiModel(value = "${table.comment!}添加和修改Model类",description = "${table.comment!}添加和修改Model类")
public class ${entity}Result {

<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>
    <#if field.voGenerate?? && field.voGenerate == 1>
    @ApiModelProperty(name = "${field.propertyName}",value = "${field.comment}")
    private ${field.propertyType} ${field.propertyName};
    <#elseif field.voGenerate?? && field.voGenerate == 2>
    @ApiModelProperty(name = "${field.propertyName}",value = "${field.comment}")
    private ${field.propertyType} ${field.propertyName};
    @ApiModelProperty(name = "${field.propertyName}Start",value = "${field.comment}起始值")
    private ${field.propertyType} ${field.propertyName}Start;
    @ApiModelProperty(name = "${field.propertyName}End",value = "${field.comment}截止值")
    private ${field.propertyType} ${field.propertyName}End;
    </#if>
</#list>
<#------------  END 字段循环遍历  ---------->

}