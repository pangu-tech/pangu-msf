package ${cfg.customConfig.basePackage}.web.dto;

<#if entityLombokModel>
import java.sql.Timestamp;
import lombok.Data;
</#if>

/**
 * <p>
 * ${table.comment!}
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Data
public class ${entity}Dto {

    private long size = 10;

    private long current = 1;
<#-- ----------  BEGIN 字段循环遍历  ---------->
<#list table.fields as field>
    <#if field.fetchGenerate?? && field.fetchGenerate == 2>
    private ${field.propertyType} ${field.propertyName};
    private ${field.propertyType} ${field.propertyName}Start;
    private ${field.propertyType} ${field.propertyName}End;
    <#else>
    private ${field.propertyType} ${field.propertyName};
    </#if>
</#list>
<#------------  END 字段循环遍历  ---------->

}
