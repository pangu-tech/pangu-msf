package ${cfg.customConfig.basePackage}.api.interfaces;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ${cfg.customConfig.basePackage}.api.bean.qo.CreateAndModify${entity}Request;
import ${cfg.customConfig.basePackage}.api.bean.qo.Fetch${entity}Request;
import ${cfg.customConfig.basePackage}.api.bean.vo.${entity}Result;
import com.fintech.pangu.commons.api.BaseResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
@RequestMapping("/api/v1")
public interface I${entity}Api{

    @GetMapping("/${entity}")
    BaseResponse<IPage<${entity}Result>> listForPage(Fetch${entity}Request fetch${entity}Request);

    @GetMapping("/${entity}/{id}")
    BaseResponse<${entity}Result> get${entity}ById(@PathVariable(value = "id") Long id);

    @PostMapping("/${entity}")
    BaseResponse<Integer> add(@RequestBody CreateAndModify${entity}Request createAndModify${entity}Request);

    @PutMapping("/${entity}/{id}")
    BaseResponse<Integer> update(@PathVariable(value = "id") Long id, @RequestBody CreateAndModify${entity}Request createAndModify${entity}Request);

    @DeleteMapping("/${entity}/{id}")
    BaseResponse<Integer> delete(@PathVariable(value = "id") Long id);
}
</#if>
