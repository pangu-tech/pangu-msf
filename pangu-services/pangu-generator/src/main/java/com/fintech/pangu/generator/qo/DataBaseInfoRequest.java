package com.fintech.pangu.generator.qo;

import lombok.Data;

/**
 * @Description: 数据库请求信息
 * @Author xiexiaole
 * @Date 2019/11/18 10:05
 */
@Data
public class DataBaseInfoRequest {

    private int current = 1;
    private int size = 10;

    private String url;
    private String userName;
    private String password;

}
