package com.fintech.pangu.generator.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.generator.entity.TableInfomation;
import com.fintech.pangu.generator.qo.DataBaseInfoRequest;

import java.util.List;

public interface GeneratorDao {

    List<TableInfomation> getTableList(DataBaseInfoRequest dataBaseInfoRequest);

    IPage<List<TableInfomation>> getTablePage(DataBaseInfoRequest dataBaseInfoRequest);
}
