package com.fintech.pangu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Description: 
 * @Author xiexiaole
 * @Date 2019/11/7 15:44
 */
@SpringBootApplication
public class PanguGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(PanguGeneratorApplication.class, args);
    }

}
