package com.fintech.pangu.generator.entity;

import lombok.Data;

import java.util.List;

/**
 * @Description: 获取表信息
 * @Author xiexiaole
 * @Date 2019/11/23 14:46
 */
@Data
public class TableInfomation {
    private String tableName;
    private String tableType;
    private String engine;
    private String tableCollation;
    private String tableComment;
    private List<FieldInfomation> filedList;
}
