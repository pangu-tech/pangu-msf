package com.fintech.pangu.generator.entity;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 自定义生成配置
 * @Author xiexiaole
 * @Date 2019/11/15 18:08
 */
@Data
public class TableGeneratorConfig {

    /** 表名 */
    private String tableName;
    /** 分页查询的字段名称/值 */
    private Map<String,Integer> fetchGeneratorColumn;
    /** createUpdate的字段名称/值 */
    private Map<String,Integer> cuGeneratorColumn;
    /** 生成vo的字段名称/值 */
    private Map<String,Integer> voGeneratorColumn;
    /** 生成dao的字段名称/值 */
    private Map<String,Integer> dtoGeneratorColumn;

    public void addFetchGeneratorColumn(String key,Integer value){
        if(fetchGeneratorColumn == null){
            fetchGeneratorColumn = new HashMap<>();
        }
        fetchGeneratorColumn.put(key, value);
    }

    public void addCuGeneratorColumn(String key,Integer value){
        if(cuGeneratorColumn == null){
            cuGeneratorColumn = new HashMap<>();
        }
        cuGeneratorColumn.put(key, value);
    }

    public void addVoGeneratorColumn(String key,Integer value){
        if(voGeneratorColumn == null){
            voGeneratorColumn = new HashMap<>();
        }
        voGeneratorColumn.put(key, value);
    }
}
