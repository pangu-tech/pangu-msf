package com.fintech.pangu.generator.entity;

import lombok.Data;

/**
 * @Description: 表字段信息
 * @Author xiexiaole
 * @Date 2019/11/23 15:27
 */
@Data
public class FieldInfomation {
    private String columnName;
    private String columnDefault;
    private String isNullable;
    private String dataType;
    private String columnType;
    private String columnKey;
    private String columnComment;
    private String columnQuery;
    private String columnTableShow = "1";
    private String columnCreateAndUpdate = "1";
}
