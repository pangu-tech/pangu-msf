package com.fintech.pangu.generator.entity;

import com.baomidou.mybatisplus.generator.config.po.TableField;
import lombok.Data;

@Data
public class TableFieldExtend extends TableField {

    /** 0-dto不显示，1-dto显示，2-dto范围查询 */
    private Integer dtoGenerate;

    /** 0-vo不显示，1-vo显示，2-vo范围查询 */
    private Integer voGenerate;

    /** 添加、修改Request */
    private Integer cuGenerate;

    /** 分页查询字段 */
    private Integer fetchGenerate;
    
    public TableFieldExtend() {
    }
    public TableFieldExtend(TableField target) {
        super.setConvert(target.isConvert());
        super.setKeyFlag(target.isKeyFlag());
        super.setKeyFlag(target.isKeyFlag());
        super.setKeyIdentityFlag(target.isKeyIdentityFlag());
        super.setName(target.getName());
        super.setType(target.getType());
        super.setPropertyName(target.getPropertyName());
        super.setColumnType(target.getColumnType());
        super.setComment(target.getComment());
        super.setFill(target.getFill());
    }

}
