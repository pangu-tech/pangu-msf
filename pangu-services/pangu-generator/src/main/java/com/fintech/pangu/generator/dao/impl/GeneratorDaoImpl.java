package com.fintech.pangu.generator.dao.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fintech.pangu.commons.exception.BaseException;
import com.fintech.pangu.exception.GeneratorException;
import com.fintech.pangu.generator.constant.DBConstant;
import com.fintech.pangu.generator.dao.GeneratorDao;
import com.fintech.pangu.generator.entity.TableInfomation;
import com.fintech.pangu.generator.qo.DataBaseInfoRequest;
import com.fintech.pangu.generator.util.GeneratorUtil;
import com.fintech.pangu.generator.util.JDBCUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Component
public class GeneratorDaoImpl implements GeneratorDao {

    private static Logger logger = LoggerFactory.getLogger(GeneratorDaoImpl.class);

    @Override
    public List<TableInfomation> getTableList(DataBaseInfoRequest dataBaseInfoRequest) {
        JDBCUtil instance = null;
        try {
            instance = JDBCUtil.instance(dataBaseInfoRequest.getUrl(), dataBaseInfoRequest.getUserName(), dataBaseInfoRequest.getPassword());
            String dataBase = GeneratorUtil.getDataBase(dataBaseInfoRequest.getUrl());
            ResultSet resultSet = instance.executeSQL(DBConstant.SELECT_DB_TABLE, dataBase);
            List<TableInfomation> result = GeneratorUtil.getResult(instance, resultSet, dataBase);
            return result;
        }catch (Exception e){
            logger.error("代码生成获取列表异常：{}",e);
            GeneratorException.GENERATOR_PARAM_EXCEPTION.throwException();
        }finally {
            if(instance != null){
                instance.closeAll();
            }
        }
        return null;
    }

    @Override
    public IPage<List<TableInfomation>> getTablePage(DataBaseInfoRequest dataBaseInfoRequest) {
        JDBCUtil instance = null;
        try {
            instance = JDBCUtil.instance(dataBaseInfoRequest.getUrl(), dataBaseInfoRequest.getUserName(), dataBaseInfoRequest.getPassword());
            String dataBase = GeneratorUtil.getDataBase(dataBaseInfoRequest.getUrl());
            ResultSet resultSet = instance.executeSQL(DBConstant.SELECT_DB_TABLE, dataBaseInfoRequest.getCurrent(), dataBaseInfoRequest.getSize(), dataBase);
            List<TableInfomation> result = GeneratorUtil.getResult(instance, resultSet, dataBase);
            Page pageInfo = new Page();
            pageInfo.setRecords(result);
            pageInfo.setTotal(instance.getTotal());
            pageInfo.setCurrent(instance.getCurrentPage());
            pageInfo.setSize(instance.getPageSize());
            return pageInfo;
        }catch (BaseException e){
            logger.error("获取代码生成列表异常：{}",e);
            throw e;
        } catch (SQLException e) {
            logger.error("代码生成获取列表异常：{}",e);
            GeneratorException.GENERATOR_SYSTEM_EXCEPTION.throwException();
        } finally {
            if(instance != null){
                instance.closeAll();
            }
        }
        return null;
    }
}
