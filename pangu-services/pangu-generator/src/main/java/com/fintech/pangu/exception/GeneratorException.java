package com.fintech.pangu.exception;

import com.fintech.pangu.commons.exception.BusinessExceptionValidator;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description: 代码生成异常类
 * @Author xiexiaole
 * @Date 2019/11/18 11:11
 */
@Getter
@AllArgsConstructor
public enum GeneratorException implements BusinessExceptionValidator {

    GENERATOR_SYSTEM_EXCEPTION("0200","系统异常"),
    GENERATOR_PARAM_EXCEPTION("0201","参数配置异常"),
    GENERATOR_DATASOURCE_EXCEPTION("0202","连接池初始化失败!"),
    GENERATOR_SQL_EXCEPTION("0202","连接池初始化失败!"),
    ;

    /**
     * 返回码
     */
    private String code;
    /**
     * 返回消息
     */
    private String message;

}
