package com.fintech.pangu.generator.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.fintech.pangu.config.MyProps;
import com.fintech.pangu.config.ParamConfig;
import com.fintech.pangu.generator.dao.GeneratorDao;
import com.fintech.pangu.generator.entity.TableInfomation;
import com.fintech.pangu.generator.qo.DataBaseInfoRequest;
import com.fintech.pangu.generator.service.GeneratorService;
import com.fintech.pangu.generator.util.GeneratorUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @Description: 代码生成实现类
 * @Author xiexiaole
 * @Date 2019/11/7 15:49
 */
@Service
public class GeneratorServiceImpl implements GeneratorService {

    private static Logger logger = LoggerFactory.getLogger(GeneratorServiceImpl.class);


    @Autowired
    private GeneratorDao generatorDao;
    @Autowired
    private MyProps props;

    @Override
    public List<TableInfomation> getTableList(DataBaseInfoRequest dataBaseInfoRequest){
        List<TableInfomation> tableList = generatorDao.getTableList(dataBaseInfoRequest);
        return tableList;
    }

    @Override
    public IPage<List<TableInfomation>> getTablePage(DataBaseInfoRequest dataBaseInfoRequest) {
        IPage<List<TableInfomation>> page = generatorDao.getTablePage(dataBaseInfoRequest);
        return page;
    }

    @Override
    public String generatorCode(JSONObject object, String basePath) {
        logger.info("-------------------开始生成代码-------------------");
        AutoGenerator gen = new AutoGenerator();
        String version = props.getPathConfigMap().get("config_current-version");
        ParamConfig config = new ParamConfig(object, basePath, version);
        gen.setCfg(config);

        /** 数据库配置 */
        DataSourceConfig dataSourceConfig = config.getDataSourceConfig();
        gen.setDataSource(dataSourceConfig);

        /** 全局配置 */
        GlobalConfig globalConfig = config.getGlobalConfig();
        gen.setGlobalConfig(globalConfig);

        /** 策略配置 */
        StrategyConfig strategyConfig = config.getStrategyConfig();
        gen.setStrategy(strategyConfig);

        /** 基础包配置 */
        PackageConfig packageInfo = config.getPackageInfo();
        gen.setPackageInfo(packageInfo);

        /** 指定模板引擎 默认是VelocityTemplateEngine ，需要引入相关引擎依赖 */
        gen.setTemplateEngine(new FreemarkerTemplateEngine());

        /** 模板配置 */
        gen.setTemplate(config.getTemplateConfig());

        /** 执行生成 */
        gen.execute();

        /** README拷贝 */
        GeneratorUtil.copyFile("templates/README.md",config.getOutPath() + "/README.md");

        logger.info("-------------------结束生成代码-------------------");
        return config.getOutPath();
    }

    @Override
    public List<String> getParentVersion() {
        String parentVersionStr = props.getPathConfigMap().get("config_parent-version");
        List<String> list = new ArrayList<>();
        if(parentVersionStr != null){
            String[] split = parentVersionStr.split(",");
            list.addAll(Arrays.asList(split));
        }
        return list;
    }
}
