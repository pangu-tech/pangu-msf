package com.fintech.pangu.generator.constant;

public interface FieldConst {

    String DATA_SOURCE = "dataSource";
    String DATA_SOURCE_URL = "url";
    String DATA_SOURCE_USER_NAME = "userName";
    String DATA_SOURCE_PASSWORD = "password";

    String BASE_CONFIG = "baseConfig";
    String AUTHOR = "author";
    String BASE_PACKAGE = "basePackage";
    String ARTIFACT = "artifact";
    String DESCRIPTION = "description";
    String VERSION = "version";
    String PARENTVERSION = "parentVersion";

    String TABLE_LIST = "tableList";
    String TABLE_NAME = "tableName";
    String FIELDS = "fields";
    String FIELD_NAME = "fieldName";
    String FETCH = "fetch";
    String CREATE_UPDATE = "createUpdate";
    String VO = "vo";

}
