package com.fintech.pangu.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.TreeMap;

@Configuration
@ConfigurationProperties(prefix = "pangu-props")
@Component
public class MyProps {
    private Map<String, String> pathConfigMap = new TreeMap<String, String>();

    public Map<String, String> getPathConfigMap() {
        return pathConfigMap;
    }

    public void setPathConfigMap(Map<String, String> pathConfigMap) {
        this.pathConfigMap = pathConfigMap;
    }
}
