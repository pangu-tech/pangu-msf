package com.fintech.pangu.generator.util;

/**
 * @Description: 字体工具类
 * @Author xiexiaole
 * @Date 2019/11/22 11:53
 */
public class FontUtil {

    public static void main(String[] args) {
        String chinies = chinese2Unicode("汉语在乎体智能abc");
        System.out.println(chinies);
        String s = unicode2Chinese(chinies);
        System.out.println(s);
    }

    /**
     * @Description: 中文转Unicode
     * @Author xiexiaole
     * @Date 2019/11/22 11:54
     * @param
     * @return
     */
    public static String chinese2Unicode(String str) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            int chr1 = str.charAt(i);
            // 汉字范围 \u4e00-\u9fa5 (中文)
            if (chr1 >= 19968 && chr1 <= 171941) {
                builder.append("\\u" + Integer.toHexString(chr1));
            } else {
                builder.append(str.charAt(i));
            }
        }
        return builder.toString();
    }

    /**
     * @Description: Unicode转中文
     * @Author xiexiaole
     * @Date 2019/11/22 11:54
     * @param
     * @return
     */
    public static String unicode2Chinese(final String unicode) {
        StringBuilder builder = new StringBuilder();
        String[] hex = unicode.split("\\\\u");
        for (int i = 0; i < hex.length; i++) {
            try {
                // 汉字范围 \u4e00-\u9fa5 (中文)
                //取前四个，判断是否是汉字
                if(hex[i].length()>=4){
                    String chinese = hex[i].substring(0, 4);
                    try {
                        int chr = Integer.parseInt(chinese, 16);
                        boolean isChinese = isChinese((char) chr);
                        //转化成功，判断是否在  汉字范围内
                        if (isChinese){
                            // 追加成string
                            builder.append((char) chr);
                            //并且追加  后面的字符
                            String behindString = hex[i].substring(4);
                            builder.append(behindString);
                        }else {
                            builder.append(hex[i]);
                        }
                    } catch (NumberFormatException e1) {
                        builder.append(hex[i]);
                    }

                }else{
                    builder.append(hex[i]);
                }
            } catch (NumberFormatException e) {
                builder.append(hex[i]);
            }
        }

        return builder.toString();
    }

    private static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }
}
