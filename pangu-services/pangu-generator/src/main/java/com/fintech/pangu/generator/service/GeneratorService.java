package com.fintech.pangu.generator.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fintech.pangu.generator.entity.TableInfomation;
import com.fintech.pangu.generator.qo.DataBaseInfoRequest;

import java.util.List;

/**
 * @Description: 代码生成接口
 * @Author xiexiaole
 * @Date 2019/11/7 15:48
 */
public interface GeneratorService {

    List<TableInfomation> getTableList(DataBaseInfoRequest dataBaseInfoRequest);

    IPage<List<TableInfomation>> getTablePage(DataBaseInfoRequest dataBaseInfoRequest);

    String generatorCode(JSONObject object,String outPath);

    List<String> getParentVersion();

}
