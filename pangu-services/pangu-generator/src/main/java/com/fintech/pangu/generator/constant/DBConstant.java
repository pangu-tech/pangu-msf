package com.fintech.pangu.generator.constant;

public interface DBConstant {

    String DB_URL_SET = "useUnicode=true&useSSL=false&characterEncoding=utf8";
    String DB_DRIVER_NAME = "com.mysql.jdbc.Driver";

    String SELECT_DB_TABLE = "select TABLE_NAME,TABLE_TYPE,ENGINE,TABLE_COLLATION,TABLE_COMMENT from information_schema.tables where table_schema = ? order by TABLE_NAME ";
    String SELECT_DB_TABLE_FIELD = " select COLUMN_NAME,COLUMN_DEFAULT,IS_NULLABLE,DATA_TYPE,COLUMN_TYPE,COLUMN_KEY,COLUMN_COMMENT from information_schema.columns where table_schema = ? and table_name = ? ";

}
