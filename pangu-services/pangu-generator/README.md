## 项目说明
- pangu-generator是一个基于springboot、mybatisPlus开发的前后端分离代码生成工具，包括entity、xml、server、controller，包括各层级的实体转换及Controller请求参数的制定。代码生成器中集成了swagger，生成之后可以访问IP:PORT/doc.html，详情请查看生成项目中的README.md
## 接口规范
本代码生成工具所生成的代码遵循RestFul接口规范：

分页获取数据：
```
GET /api/v1/${entity}
```

通过ID获取一条数据：
```
GET /api/v1/${entity}/{id}
```
添加一条数据：
```
POST /api/v1/${entity}
```
修改一条数据：
```
PUT /api/v1/${entity}/{id}
```
删除一条数据：
```
DELETE /api/v1/${entity}/{id}
```
注：
```
${entity}：数据库表名称切换为去“_”且首字母大写。比如sys_user表entity命名为SysUser
{id}：被操作的数据id
```
## 本地部署
- 通过git下载代码
- 运行com.fintech.pangu包下的PanguGeneratorApplication主类
- 下载运行前端代码：npm run dev
- 访问 http://localhost:8888/gencode

## 操作步骤
- 填写指定信息连接数据库
- 点击对应表的字段配置，配置所需信息参数点击确定
- 配置完成之后点击代码生成

## 演示效果
#### 基本参数配置界面
![initParam](../../doc/assets/20191204095201.png)
#### 代码生成界面
![generator](../../doc/assets/20191204124557.png)