package com.fintech.pangu.security.oauth2.sso.client.logout;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 自动登出的LogoutSuccessHandler
 * 由于是后端发起调用的，返回200状态
 */
public class OAuth2SsoClientAutoLogoutSuccessHandler implements LogoutSuccessHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        response.setStatus(HttpStatus.OK.value());
        response.getWriter().write("SSO Client AutoLogout Success");
        response.getWriter().flush();
    }

}
