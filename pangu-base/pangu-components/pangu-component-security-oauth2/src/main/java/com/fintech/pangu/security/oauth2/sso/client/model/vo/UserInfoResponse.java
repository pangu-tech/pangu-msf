package com.fintech.pangu.security.oauth2.sso.client.model.vo;


import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.security.oauth2.sso.client.model.dto.UserInfoDto;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

/**
 * @Description:
 */
@Data
@Builder
public class UserInfoResponse extends BaseResponse {

    @ApiModelProperty(value = "用户详情", name = "userInfo")
    private UserInfoDto userInfo;
}
