package com.fintech.pangu.security.oauth2.sso.client.logout;

import com.alibaba.fastjson.JSON;
import com.fintech.pangu.security.oauth2.sso.client.model.dto.SystemInfoDto;
import com.fintech.pangu.security.oauth2.sso.client.model.vo.SystemInfoResponse;
import lombok.Setter;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * SSO Client使用，用于通知授权服务器退出的Handler
 */
@Setter
public class OAuth2SsoClientNoticeLogoutSuccessHandler implements LogoutSuccessHandler {

    /**
     * 授权服务器URL
     */
    private String authServerLogoutUri;

    private OAuth2ProtectedResourceDetails oauth2ProtectedResourceDetails;


    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        // 返回授权服务器退出端点地址，前端触发调用获取其余客户端地址；实现所有客户端退出
        String clientId = oauth2ProtectedResourceDetails!=null ? oauth2ProtectedResourceDetails.getClientId() : "";

        SystemInfoDto systemInfoDto = new SystemInfoDto();
        systemInfoDto.setAuthServerLogoutUri(authServerLogoutUri + "/logout?clientId=" + clientId);

        response.setStatus(HttpServletResponse.SC_OK);
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.write(JSON.toJSONString(SystemInfoResponse.builder().systemInfo(systemInfoDto).build()));
        out.flush();
        out.close();
    }
}
