package com.fintech.pangu.security.oauth2.sso.client.model.vo;


import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.security.oauth2.sso.client.model.dto.SystemInfoDto;
import lombok.Builder;
import lombok.Data;

/**
 * @Description:
 */
@Data
@Builder
public class SystemInfoResponse extends BaseResponse
{

    private SystemInfoDto systemInfo;
}
