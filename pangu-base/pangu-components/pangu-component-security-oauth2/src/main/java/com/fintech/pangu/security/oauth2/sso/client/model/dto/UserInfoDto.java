package com.fintech.pangu.security.oauth2.sso.client.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Description:
 */
@Data
public class UserInfoDto {

    @ApiModelProperty(value = "用户code", name = "userCode")
    private String userCode;

    @ApiModelProperty(value = "用户名", name = "userName")
    private String userName;
}
