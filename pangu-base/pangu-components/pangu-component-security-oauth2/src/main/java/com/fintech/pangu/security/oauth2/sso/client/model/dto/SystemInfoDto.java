package com.fintech.pangu.security.oauth2.sso.client.model.dto;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class SystemInfoDto {

    private String authServerLogoutUri;

}
