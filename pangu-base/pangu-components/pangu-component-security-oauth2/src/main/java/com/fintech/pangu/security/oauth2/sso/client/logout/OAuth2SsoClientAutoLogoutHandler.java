package com.fintech.pangu.security.oauth2.sso.client.logout;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.IterableMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.MapUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.session.FindByIndexNameSessionRepository;
import org.springframework.session.Session;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Base64;
import java.util.Enumeration;
import java.util.Map;

/**
 * 实现由SSO Server端触发的SSO Client自动退出，只清理Client自己的session
 * 通过base64解析后得到username，再找到对应session并清理
 */
@Data
@Slf4j
public class OAuth2SsoClientAutoLogoutHandler implements LogoutHandler {

    private FindByIndexNameSessionRepository<? extends Session> sessionRepository;

    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        // 提取用户信息
        String userInfo = extractUserInfoFromHeader(request);

        if (StringUtils.isEmpty(userInfo)) {
            log.warn("SSO Client auto logout userInfo is null");
            //TODO 抛异常
        }

        // 提取用户名
        String userName = new String(Base64.getDecoder().decode(userInfo));

        // 通过userName找到对应的session
        Map<String, ? extends Session> usersSessions =
                sessionRepository.findByIndexNameAndIndexValue(
                        FindByIndexNameSessionRepository.PRINCIPAL_NAME_INDEX_NAME,
                        userName);

        // 清除session
        IterableMap<String, ? extends Session> iterableMap = MapUtils.iterableMap(usersSessions);
        MapIterator<String, ? extends Session> it = iterableMap.mapIterator();
        while (it.hasNext()) {
            String sessionId = (String) it.next();
            sessionRepository.deleteById(sessionId); // 删除session
        }
    }

    /**
     * 从请求头中提取用户信息
     *
     * @param request
     * @return
     */
    private String extractUserInfoFromHeader(HttpServletRequest request) {
        Enumeration<String> headers = request.getHeaders("ssoAutoLogout");

        while (headers.hasMoreElements()) {
            String value = headers.nextElement();

            if (StringUtils.hasText(value)) {
                return value;
            }
        }

        return null;
    }
}
