package com.fintech.pangu.rocketmq.metrics;

/**
 * 收集RocketMQ客户端执行数据的收集器接口
 * 接口方法与事件对应
 */
public interface RocketMQMetricsCollector {

    //============= 发送事件 =============//
    /**
     * 客户端发送成功
     */
    void sendSuccess(RocketMQSendMessageContext context);

    /**
     * 客户端发送失败
     */
    void sendFailure(RocketMQSendMessageContext context);

    /**
     * 内部发送成功
     */
    void sendKernelSuccess(RocketMQSendMessageContext context);

    /**
     * 内部发送失败
     */
    void sendKernelFailure(RocketMQSendMessageContext context);



    //============= 消费事件 =============//
    /**
     * 客户端消费成功
     */
    void consumeSuccess(RocketMQConsumeMessageContext context);

    /**
     * 客户端消费失败
     */
    void consumeFailure(RocketMQConsumeMessageContext context);

    /**
     * 内部消费成功
     */
    void consumeKernelSuccess(RocketMQConsumeMessageContext context);

    /**
     * 内部消费失败
     */
    void consumeKernelFailure(RocketMQConsumeMessageContext context);


}
