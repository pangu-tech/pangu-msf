package com.fintech.pangu.rocketmq.metrics;

/**
 * RocketMQMetricsCollector接口的空实现
 */
public class NoOpRocketMQMetricsCollector implements RocketMQMetricsCollector {

    @Override
    public void sendSuccess(RocketMQSendMessageContext context) {

    }

    @Override
    public void sendFailure(RocketMQSendMessageContext context) {

    }

    @Override
    public void sendKernelSuccess(RocketMQSendMessageContext context) {

    }

    @Override
    public void sendKernelFailure(RocketMQSendMessageContext context) {

    }

    @Override
    public void consumeSuccess(RocketMQConsumeMessageContext context) {

    }

    @Override
    public void consumeFailure(RocketMQConsumeMessageContext context) {

    }

    @Override
    public void consumeKernelSuccess(RocketMQConsumeMessageContext context) {

    }

    @Override
    public void consumeKernelFailure(RocketMQConsumeMessageContext context) {

    }
}
