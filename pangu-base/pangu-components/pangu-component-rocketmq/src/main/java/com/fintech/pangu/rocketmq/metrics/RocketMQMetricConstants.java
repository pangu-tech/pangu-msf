package com.fintech.pangu.rocketmq.metrics;

/**
 * RocketMq Metrics常量类
 */
public class RocketMQMetricConstants {

    public static class SendStatus {
        // 自定义失败状态
        public static final String SEND_FAILED = "SEND_FAILED";

    }

    public static class ConsumeStatus {
        // 自定义失败状态
        public static final String CONSUME_FAILED = "CONSUME_FAILED";

    }


    /**
     * Metrics Tags
     */
    public static class Tags {
        //============ 公共
        // 主题
        public static final String TOPIC = "topic";

        // 是否成功
        public static final String SUCCESS = "success";

        // 异常
        public static final String EXCEPTION = "exception";


        //============ 发送
        // 生产组
        public static final String PRODUCER_GROUP = "producer.group";

        // 交互类型
        public static final String COMMUNICATION_MODE = "communication.mode";

        // 发送状态
        public static final String SEND_STATUS = "send.status";

        // 异常，response code
        public static final String SEND_RESPONSE_CODE = "response.code";


        //============ 消费
        // 消费组
        public static final String CONSUMER_GROUP = "consumer.group";

        // 消费组
        public static final String MESSAGE_MODEL = "message.model";

        // 消费组
        public static final String CONSUME_MODE = "consume.mode";

        // 消费状态
        public static final String CONSUME_STATUS = "consume.status";

        // 是否重试
        public static final String RETRY = "retry";
    }


}
