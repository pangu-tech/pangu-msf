package com.fintech.pangu.rocketmq.metrics;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * RocketMQMetricsCollector收集指标数据时的入参
 * 封装记录指标数据时的必要数据，生成Tags
 */
@Data
@Builder
@AllArgsConstructor
public class RocketMQConsumeMessageContext {

    /** 是否消费成功 */
    private boolean success;

    /** 是否重复消费消费 */
    private boolean retry;

    /** 主题 */
    private String topic;

    /** 消费组 */
    private String consumerGroup;

    /** 消息模式，默认集群模式，还有BROADCASTING广播模式 */
    private String messageModel;

    /** 消费模式，默认是并发消费CONCURRENTLY */
    private String consumeMode;

    /** 消费状态 */
    private String consumeStatus;

    /** 消费数量 */
    private int consumeCount;

    /** 耗时 (ms) */
    private long duration;

}
