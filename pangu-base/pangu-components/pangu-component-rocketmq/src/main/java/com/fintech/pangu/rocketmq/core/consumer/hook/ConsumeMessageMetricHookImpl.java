package com.fintech.pangu.rocketmq.core.consumer.hook;

import com.alibaba.rocketmq.client.hook.ConsumeMessageContext;
import com.alibaba.rocketmq.client.hook.ConsumeMessageHook;
import com.fintech.pangu.rocketmq.metrics.NoOpRocketMQMetricsCollector;
import com.fintech.pangu.rocketmq.metrics.RocketMQMetricsCollector;
import lombok.Setter;

/**
 * （暂不使用）用于收集Metrics指标的ConsumeMessageHook
 */
@Setter
public class ConsumeMessageMetricHookImpl implements ConsumeMessageHook {

    /**
     * Metric指标收集器
     */
    private RocketMQMetricsCollector rocketMQMetricsCollector;

    /**
     * 获取RocketMQMetricsCollector收集器，默认创建NoOpRocketMQMetricsCollector
     * @return
     */
    private RocketMQMetricsCollector getRocketMQMetricsCollector() {
        if(rocketMQMetricsCollector == null){
            synchronized (this){
                if(rocketMQMetricsCollector == null){
                    rocketMQMetricsCollector = new NoOpRocketMQMetricsCollector();
                }
            }
        }

        return rocketMQMetricsCollector;
    }

    @Override
    public String hookName() {
        return "ConsumeMessageMetricHook";
    }


    @Override
    public void consumeMessageBefore(ConsumeMessageContext context) {
        // nothing to do
    }

    @Override
    public void consumeMessageAfter(ConsumeMessageContext context) {

    }

}
