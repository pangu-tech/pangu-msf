package com.fintech.pangu.security.jwt.client.authentication.config;


import com.fintech.pangu.security.config.PanGuHttpSecurityConfigurer;
import com.fintech.pangu.security.jwt.client.authentication.filter.JwtAuthenticationFilter;
import com.fintech.pangu.security.jwt.client.authentication.filter.JwtBlackListFilter;
import lombok.Setter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * 用于注入JwtBlackListFilter的子配置
 */
@Setter
public class JwtBlackListPanGuHttpSecurityConfig extends PanGuHttpSecurityConfigurer {

    private JwtBlackListFilter jwtBlackListFilter;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        // 添加JWT黑名单filter，在JwtAuthenticationFilter之前
        http.addFilterAfter(jwtBlackListFilter, JwtAuthenticationFilter.class);
    }

}
