package com.fintech.pangu.security.jwt.client.authentication.filter;

import com.fintech.pangu.commons.jwt.JWTUtil;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 解析JWT，还原Authentication的Filter
 */
@Slf4j
@Setter
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    //@Autowired
    //private UserDetailsService userDetailsService;

    private String secret;

    private String tokenReturnType;


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            Assert.hasText(secret, "jwt secret can not be empty!");
            Assert.hasText(tokenReturnType, "properties[.token.response.return-type] can not be empty!");

            String jwt = getJwtFromRequest(request);

            // 如果jwt不为空 且 校验通过
            if (StringUtils.hasText(jwt) && JWTUtil.validateToken(jwt, secret)) {

                String userName = JWTUtil.getUserNameFromJWT(jwt, secret);

                // 获取用户信息
                // 暂时废弃，check jwt的客户端的所有用户信息均从jwt中获取
                //UserDetails userDetails = userDetailsService.loadUserByUsername(userName);

                // 创建Authentication
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userName, null, null);
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));

                // SecurityContext中存入Authentication
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        catch (Exception ex) {
            logger.error("Could not set user authentication in security context", ex);
        }

        filterChain.doFilter(request, response);
    }

    /**
     * 从reuqest中获取JWT
     * @param request
     * @return
     */
    private String getJwtFromRequest(HttpServletRequest request) {
        if(StringUtils.hasText(tokenReturnType)){
            // cookie
            if("cookie".equals(tokenReturnType)){
                Cookie[] cookies = request.getCookies();

                if(cookies!=null && cookies.length>0){
                    for(Cookie cookie : cookies){
                        if("pangu_auth_token".equals(cookie.getName())){
                            return cookie.getValue();
                        }
                    }
                }
            }
            // response
            else if("response".equals(tokenReturnType)){
                // TODO
            }
        }

        return null;
    }
}
