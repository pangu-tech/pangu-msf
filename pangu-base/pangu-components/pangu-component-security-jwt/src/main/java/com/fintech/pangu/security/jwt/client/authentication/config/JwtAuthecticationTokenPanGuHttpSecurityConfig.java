package com.fintech.pangu.security.jwt.client.authentication.config;


import com.fintech.pangu.security.config.PanGuHttpSecurityConfigurer;
import com.fintech.pangu.security.jwt.client.authentication.filter.JwtAuthenticationFilter;
import lombok.Setter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

/**
 * 用于注入JwtAuthenticationFilter的子配置
 */
@Setter
public class JwtAuthecticationTokenPanGuHttpSecurityConfig extends PanGuHttpSecurityConfigurer {

    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        //jwtAuthenticationFilter.setUserDetailsService(http.getSharedObject(UserDetailsService.class)); // 使用依赖注入会产生循环依赖
                                                                                                       // 暂时废弃，check jwt的客户端的所有用户信息均从jwt中获取

        // 添加使用JWT恢复认证状态filter
        http.addFilterAfter(jwtAuthenticationFilter, AbstractPreAuthenticatedProcessingFilter.class);
    }

}
