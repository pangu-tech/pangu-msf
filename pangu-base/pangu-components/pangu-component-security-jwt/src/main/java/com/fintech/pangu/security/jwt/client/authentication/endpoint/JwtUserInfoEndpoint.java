package com.fintech.pangu.security.jwt.client.authentication.endpoint;

import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.framework.endpoint.PanGuFrameworkEndpoint;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

/**
 * JWT获取用户信息端点
 *
 * @author Trust_FreeDom
 */
@PanGuFrameworkEndpoint
@ResponseBody
public class JwtUserInfoEndpoint {

    @RequestMapping(value = "/api/userInfo", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseResponse userDetails(Principal principal){
        Map<String, String> userInfo = new HashMap();
        userInfo.put("username", principal.getName());
        return new BaseResponse(userInfo);
    }

}
