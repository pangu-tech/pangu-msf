package com.fintech.pangu.security.jwt.provider.authentication.token;

import com.fintech.pangu.security.authentication.token.AuthenticationToken;
import com.fintech.pangu.security.authentication.token.AuthenticationTokenStore;
import org.springframework.security.core.Authentication;

public class JwtAuthenticationTokenStore implements AuthenticationTokenStore {

    @Override
    public AuthenticationToken getAuthenticationToken(Authentication authentication) {
        // JWT自包含，服务端不保存，也无法获取
        return null;
    }

    @Override
    public void storeAuthenticationToken(AuthenticationToken token, Authentication authentication) {
        // JWT自包含，服务端不保存
    }

    @Override
    public void removeAuthenticationToken(AuthenticationToken token) {
        // JWT自包含，服务端不保存，更不用删除
    }

}
