package com.fintech.pangu.elasticjob.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * elastic job 任务注解类
 *
 * @author xujunqi
 * @since 1.0.0
 */
@Target({ElementType.ANNOTATION_TYPE, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface ElasticScheduledJob {
    /**
     * 定时任务表达式
     */
    String cron();

    /**
     * 分片数量
     */
    String shardingTotalCount() default "1";

    /**
     * 分片表达式
     */
    String shardingItemParameters() default "";

    String jobParameter() default "";

    boolean failover() default true;

    boolean misfire() default true;

    String description() default "";

    boolean overwrite() default true;

    /**
     * 作业是否启用，默认启动
     */
    boolean disabled() default false;
}
