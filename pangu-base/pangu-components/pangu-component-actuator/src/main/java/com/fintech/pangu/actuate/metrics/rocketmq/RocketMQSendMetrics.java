package com.fintech.pangu.actuate.metrics.rocketmq;

import com.fintech.pangu.actuate.metrics.util.MetricsUtil;
import com.fintech.pangu.rocketmq.metrics.RocketMQMetricConstants;
import com.fintech.pangu.rocketmq.metrics.RocketMQSendMessageContext;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.DistributionSummary;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import org.springframework.util.StringUtils;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Description: RocketMQ发送端信息采集
 * @Author xiexiaole
 * @Date 2019/12/17 13:33
 */
public class RocketMQSendMetrics  {



    private static AtomicInteger sendDelayCount = new AtomicInteger();



    public void getSendCountCounter(RocketMQSendMessageContext context){
        //总数
        Counter counter = MetricsUtil.recordCounter("rocketmq.client.send.total",
                "rocketmq client send success, total count",
                convertToTags(context));
        counter.increment();
    }

    public AtomicInteger getSendDelayGauge(RocketMQSendMessageContext context){
        AtomicInteger sendDelayCount = MetricsUtil.recordGauge("rocketmq.client.send.delay",
                "rocketmq client not send count", convertToTags(context));
        return sendDelayCount;
    }

    public void getSendCostTime(RocketMQSendMessageContext context){
        DistributionSummary summary = MetricsUtil.recordDistributionSummary("rocketmq.client.send.timer",
                "rocketmq client send success, timer",
                    convertToTags(context),
                0.5, 0.9, 0.95, 0.99);
        summary.record(context.getDuration());
    }

    public void getSendKernelCounter(RocketMQSendMessageContext context){
        Counter counter = MetricsUtil.recordCounter("rocketmq.client.kernel.send.total",
                "rocketmq client kernel send fail, total count",
                convertToTags(context)
                );
        counter.increment();
    }

    /**
     * RocketMQSendMessageContext转换为 Micrometer Tags
     */
    private Tags convertToTags(RocketMQSendMessageContext context) {
        Tags tags = Tags.empty();

        if(context.isSuccess()){
            tags = tags.and(Tag.of(RocketMQMetricConstants.Tags.SUCCESS, "Y"));
        }
        else {
            tags = tags.and(Tag.of(RocketMQMetricConstants.Tags.SUCCESS, "N"));
        }

        if(StringUtils.hasText(context.getTopic())){
            tags = tags.and(Tag.of(RocketMQMetricConstants.Tags.TOPIC, context.getTopic()));
        }

        if(StringUtils.hasText(context.getProducerGroup())){
            tags = tags.and(Tag.of(RocketMQMetricConstants.Tags.PRODUCER_GROUP, context.getProducerGroup()));
        }

//        if(StringUtils.hasText(context.getCommunicationMode())){
//            tags = tags.and(Tag.of(RocketMQMetricConstants.Tags.COMMUNICATION_MODE, context.getCommunicationMode()));
//        }

        if(StringUtils.hasText(context.getSendStatus())){
            tags = tags.and(Tag.of(RocketMQMetricConstants.Tags.SEND_STATUS, context.getSendStatus()));
        }

//        if(context.getException()!=null && StringUtils.hasText(context.getException().getClass().getSimpleName())){
//            tags = tags.and(Tag.of(RocketMQMetricConstants.Tags.EXCEPTION, context.getException().getClass().getSimpleName()));
//        }

//        if(context.getResponseCode()!=null){
//            tags = tags.and(Tag.of(RocketMQMetricConstants.Tags.SEND_RESPONSE_CODE, context.getResponseCode().toString()));
//        }

        return tags;
    }
}
