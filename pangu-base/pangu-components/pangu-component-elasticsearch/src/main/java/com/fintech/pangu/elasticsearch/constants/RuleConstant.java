package com.fintech.pangu.elasticsearch.constants;

/**
 * 规则常量类
 *
 * @author chendongdong
 * @since 1.0.0
 **/
public class RuleConstant {

    /**
     * 索引结构大类(object-对象;nested-嵌套结构(数组))
     */
    public static final String OBJECT = "object";
    public static final String NESTED = "nested";
    /**
     * 字段类型
     * text-字符串<支持分词>
     */
    public static final String TEXT = "text";

    /**
     * 运算符
     */
    public final static String EQUAL = "=";

    public final static String NOT_EQUAL = "<>";

    public final static String GREATER_THAN = ">";

    public final static String GREATER_THAN_OR_EQUAL = ">=";

    public final static String LESS_THAN = "<";

    public final static String LESS_THAN_OR_EQUAL = "<=";

    public final static String IN = "IN";

    public final static String NOT_IN = "NOT IN";

    public final static String IS_NOT_NULL = "IS NOT NULL";


    /**
     * like模糊查询
     */
    public final static String LIKE = "LIKE";

    /**
     * partial部分匹配
     */
    public final static String PARTIAL = "PARTIAL";

    /**
     * fuzzy模糊检索
     * 模糊查询查找在模糊度中指定的最大编辑距离内的所有可能的匹配项，然后检查术语字典，以找出在索引中实际存在待检索的关键词。
     */
    public final static String FUZZY_MATCH = "FUZZY MATCH";

    public final static String NOT_FUZZY_MATCH = "NOT FUZZY MATCH";


    /**
     * 逻辑运算符
     */
    public final static String AND = "AND";

    public final static String OR = "OR";

    /**
     * 左括号
     */
    public final static String LEFT_BRACKET = "(";

    /**
     * 右括号
     */
    public final static String RIGHT_BRACKET = ")";

    /**
     * 左中括号
     */
    public final static String LEFT_SQUARE_BRACKET = "[";

    /**
     * 右中括号
     */
    public final static String RIGHT_SQUARE_BRACKET = "]";

    /**
     * 是否标识
     */
    public final static String YES = "1";
    public final static String NO = "0";

    /**
     * 建议索引分词使用ik_max_word,
     * 搜索分词器根据需要选择可以使用ik_smart
     */
    public final static String IK_MAX_WORD_ANALYZER = "ik_max_word";
    public final static String IK_SMART_ANALYZER = "ik_smart";


    /**
     * 问号
     */
    public final static String QUESTION_MARK = "?";

    /**
     * 星号
     */
    public final static String STAR_MARK = "*";

    /**
     * 英文句号
     */
    public final static String ENGLISH_FULL_STOP = ".";

    /**
     * 井号键
     */
    public final static String POUND_MARK = "#";

    /**
     * 分隔符逗号
     */
    public final static String COMMA = ",";

    /**
     * 百分号
     */
    public final static String PERCENT_MARK = "%";

    /**
     * 连字符
     */
    public final static String HYPHENS_MARK = "-";

    /**
     * 下划线
     */
    public final static String UNDER_LINE = "_";

    /**
     * 字段统计别名后缀
     */
    public static final String FIELD_COUNT_ALIAS_SUFFIX = "cnt";

    /**
     * 元数据模式
     */
    public static final String SOURCE_MODE = "1";

    /**
     * script脚本模式
     */
    public static final String SCRIPT_MODE = "2";
}
