package com.fintech.pangu.elasticsearch.service;

import java.util.List;

/**
 * 分析查询服务
 *
 * @author xujunqi
 * @author chendongdong
 * @since 1.0.0
 */
public interface AnalyzeService {
    /**
     * 分词查询服务
     *
     * @param text     分词文本
     * @param analyzer 分词器名称
     * @return
     **/
    List<String> queryAnalyzer(String text, String analyzer);
}
