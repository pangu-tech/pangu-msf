package com.fintech.pangu.elasticsearch.service;

import com.fintech.pangu.elasticsearch.vo.AggregationSearchReqVO;
import org.elasticsearch.action.search.SearchResponse;

/**
 * 常用聚合查询 接口
 *
 * @author chendongdong
 * @since 1.0.0
 **/
public interface AggregationService {

    /**
     * 获取单个字段聚合统计
     *
     * @param aggregationSearchReqVO 聚合查询请求VO类对象
     * @return
     **/
    SearchResponse getFieldGroupBy(AggregationSearchReqVO aggregationSearchReqVO);

    /**
     * 获取单个字段聚合统计, 并支持having查询
     *
     * @param aggregationSearchReqVO 聚合查询请求VO类对象
     * @return
     **/
    SearchResponse getFieldGroupByHaving(AggregationSearchReqVO aggregationSearchReqVO);


    /**
     * 获取单个字段聚合统计, 并支持having查询, 以及filter查询筛选
     *
     * @param aggregationSearchReqVO 聚合查询请求VO类对象
     * @return
     **/
    SearchResponse getFieldGroupByHavingWithFilter(AggregationSearchReqVO aggregationSearchReqVO);


    /**
     * 获取嵌套聚合单字段统计
     *
     * @param aggregationSearchReqVO 聚合查询请求VO类对象
     * @return
     **/
    SearchResponse getNestedFieldGroupBy(AggregationSearchReqVO aggregationSearchReqVO);

    /**
     * 获取嵌套聚合单字段统计, 并支持having查询
     *
     * @param aggregationSearchReqVO 聚合查询请求VO类对象
     * @return
     **/
    SearchResponse getNestedFieldGroupByHaving(AggregationSearchReqVO aggregationSearchReqVO);

    /**
     * 获取嵌套聚合单字段统计, 并支持having查询, 以及filter查询筛选
     *
     * @param aggregationSearchReqVO 聚合查询请求VO类对象
     * @return
     **/
    SearchResponse getNestedFieldGroupByHavingWithFilter(AggregationSearchReqVO aggregationSearchReqVO);


    /**
     * 获取多个字段聚合统计
     *
     * @param index       索引
     * @param firstField  第一个字段(类型编码.字段编码)
     * @param otherFields 其它字段(类型编码.字段编码)
     * @return
     **/
    SearchResponse getMultiFieldGroupBy(String index, String firstField, String... otherFields);
}
