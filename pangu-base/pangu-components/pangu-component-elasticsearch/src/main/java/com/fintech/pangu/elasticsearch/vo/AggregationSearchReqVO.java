package com.fintech.pangu.elasticsearch.vo;

import com.fintech.pangu.elasticsearch.validation.NestedQueryGroup;
import com.fintech.pangu.elasticsearch.validation.ObjectQueryGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.elasticsearch.index.query.QueryBuilder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 聚合查询请求VO类
 *
 * @author chendongdong
 * @since 1.0.0
 **/
@ApiModel(value = "聚合查询请求VO类")
public class AggregationSearchReqVO implements Serializable {


    private static final long serialVersionUID = -5215044357098701285L;

    /**
     * 索引
     */
    @NotBlank(message = "索引不能为空", groups = {ObjectQueryGroup.class, NestedQueryGroup.class})
    @ApiModelProperty(value = "索引")
    private String index;

    /**
     * 字段(类型编码.字段编码)
     */
    @NotBlank(message = "字段不能为空", groups = {ObjectQueryGroup.class, NestedQueryGroup.class})
    @ApiModelProperty(value = "字段(类型编码.字段编码)")
    private String field;

    /**
     * 最小出现频率
     */
    @NotNull(message = "最小出现频率不能为空", groups = {ObjectQueryGroup.class, NestedQueryGroup.class})
    @ApiModelProperty(value = "最小出现频率")
    private Integer minValueOccurrences;

    /**
     * 路径(类型编码)
     */
    @NotBlank(message = "路径不能为空", groups = NestedQueryGroup.class)
    @ApiModelProperty(value = "路径(类型编码)")
    private String path;

    /**
     * 预定义查询构造器
     */
    @ApiModelProperty(value = "预定义查询构造器")
    private QueryBuilder queryBuilder;


    public String getIndex() {
        return index;
    }

    public AggregationSearchReqVO setIndex(String index) {
        this.index = index;
        return this;
    }

    public String getField() {
        return field;
    }

    public AggregationSearchReqVO setField(String field) {
        this.field = field;
        return this;
    }

    public Integer getMinValueOccurrences() {
        return minValueOccurrences;
    }

    public AggregationSearchReqVO setMinValueOccurrences(Integer minValueOccurrences) {
        this.minValueOccurrences = minValueOccurrences;
        return this;
    }

    public String getPath() {
        return path;
    }

    public AggregationSearchReqVO setPath(String path) {
        this.path = path;
        return this;
    }

    public QueryBuilder getQueryBuilder() {
        return queryBuilder;
    }

    public AggregationSearchReqVO setQueryBuilder(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
        return this;
    }

    @Override
    public String toString() {
        return "AggregationSearchReqVO{" +
                "index='" + index + '\'' +
                ", field='" + field + '\'' +
                ", minValueOccurrences=" + minValueOccurrences +
                ", path='" + path + '\'' +
                ", queryBuilder=" + queryBuilder +
                '}';
    }

    public AggregationSearchReqVO() {
    }
}
