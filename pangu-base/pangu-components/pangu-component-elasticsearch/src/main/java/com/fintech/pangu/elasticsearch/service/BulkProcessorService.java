package com.fintech.pangu.elasticsearch.service;

import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;

import java.util.List;

/**
 * ElasticSearch批量处理服务接口
 *
 * @author chendongdong
 * @since 1.0.0
 **/
public interface BulkProcessorService {

    /**
     * 批量新增操作
     *
     * @param insertRequestList 批量新增请求列表
     **/
    void bulkInsertProcessor(List<IndexRequest> insertRequestList);

    /**
     * 批量更新操作
     *
     * @param updateRequestList 批量更新请求列表
     **/
    void bulkUpdateProcessor(List<UpdateRequest> updateRequestList);


}
