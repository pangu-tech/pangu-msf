package com.fintech.pangu.elasticsearch.service;

import com.fintech.pangu.elasticsearch.vo.SingleDocumentHandleVO;

import java.util.List;

/**
 * ElasticSearch文档操作服务接口
 *
 * @author xujunqi
 * @author chendongdong
 * @since 1.0.0
 */
public interface DocumentService {

    /**
     * 查询单个文档对象
     *
     * @param index 索引名称
     * @param type  文档类型
     * @param id    文档id
     * @return 字符串格式文档
     * @throws Exception
     */
    String getSingleDocumentAsString(String index, String type, String id) throws Exception;

    /**
     * 查询多个文档对象
     *
     * @param index 索引名称
     * @param type  文档类型
     * @param ids
     * @return
     */
    List<String> multiGetDocuments(String index, String type, String... ids);

    /**
     * 按照id删除单个文档
     *
     * @param index 索引名称
     * @param type  文档类型
     * @param id    文档id
     * @return
     * @throws Exception
     */
    Boolean deleteSingleDocument(String index, String type, String id) throws Exception;

    /**
     * 单文档操作初始校验
     *
     * @param index 索引名称
     * @param type  文档类型
     * @param id    文档id
     * @throws Exception
     */
    void singleDocumentParamInitCheck(String index, String type, String id) throws Exception;

    /**
     * 新增单个文档
     *
     * @param index      索引名称
     * @param jsonSource 文档JSON格式字符串
     * @return
     * @throws Exception
     */
    String insertSingleDocument(String index, String jsonSource) throws Exception;

    /**
     * 新增单个文档
     *
     * @param index      索引名称
     * @param type       文档类型
     * @param jsonSource 文档JSON格式字符串
     * @return
     * @throws Exception
     */
    String insertSingleDocument(String index, String type, String jsonSource) throws Exception;

    /**
     * 新增单个文档
     *
     * @param index      索引名称
     * @param type       文档类型
     * @param id         文档id
     * @param jsonSource 文档JSON格式字符串
     * @return
     * @throws Exception
     */
    String insertSingleDocument(String index, String type, String id, String jsonSource) throws Exception;

    /**
     * 修改单个文档
     *
     * @param singleDocumentHandleVO 单个文档操作请求VO类对象
     * @return
     * @throws Exception
     */
    String updateSingleDocument(SingleDocumentHandleVO singleDocumentHandleVO) throws Exception;
}
