package com.fintech.pangu.elasticsearch.vo;

import com.fintech.pangu.elasticsearch.validation.Insert;
import com.fintech.pangu.elasticsearch.validation.Update;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.elasticsearch.common.xcontent.XContentType;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * 单个文档操作请求VO类
 *
 * @author chendongdong
 * @since 1.0.0
 **/
@ApiModel(value = "单个文档操作请求VO类")
public class SingleDocumentHandleVO implements Serializable {

    private static final long serialVersionUID = 3948751041354041997L;

    /**
     * 索引名称
     */
    @NotBlank(message = "索引名称不能为空", groups = {Insert.class, Update.class})
    @ApiModelProperty(value = "索引名称")
    private String index;

    /**
     * 类型名称
     */
    @NotBlank(message = "类型名称不能为空", groups = {Insert.class, Update.class})
    @ApiModelProperty(value = "类型名称")
    private String type;


    /**
     * 文档ID
     */
    @NotBlank(message = "文档ID不能为空", groups = {Update.class})
    @ApiModelProperty(value = "文档ID名称")
    private String id;

    /**
     * 操作模式(1-元数据模式;2-script脚本模式)
     */
    @NotBlank(message = "操作模式为空", groups = {Update.class})
    @ApiModelProperty(value = "操作模式")
    private String operateMode;


    /**
     * 元数据
     */
    @ApiModelProperty(value = "元数据")
    private String source;

    /**
     * script脚本
     */
    @ApiModelProperty(value = "script脚本")
    private String script;

    /**
     * 内容类型
     */
    @ApiModelProperty(value = "内容类型")
    private XContentType xcontentType;

    public String getIndex() {
        return index;
    }

    public SingleDocumentHandleVO setIndex(String index) {
        this.index = index;
        return this;
    }

    public String getType() {
        return type;
    }

    public SingleDocumentHandleVO setType(String type) {
        this.type = type;
        return this;
    }

    public String getId() {
        return id;
    }

    public SingleDocumentHandleVO setId(String id) {
        this.id = id;
        return this;
    }

    public String getOperateMode() {
        return operateMode;
    }

    public SingleDocumentHandleVO setOperateMode(String operateMode) {
        this.operateMode = operateMode;
        return this;
    }

    public String getSource() {
        return source;
    }

    public SingleDocumentHandleVO setSource(String source) {
        this.source = source;
        return this;
    }

    public String getScript() {
        return script;
    }

    public SingleDocumentHandleVO setScript(String script) {
        this.script = script;
        return this;
    }

    public XContentType getXcontentType() {
        return xcontentType;
    }

    public SingleDocumentHandleVO setXcontentType(XContentType xcontentType) {
        this.xcontentType = xcontentType;
        return this;
    }

    @Override
    public String toString() {
        return "SingleDocumentHandleVO{" +
                "index='" + index + '\'' +
                ", type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", operateMode='" + operateMode + '\'' +
                ", source='" + source + '\'' +
                ", script='" + script + '\'' +
                ", xcontentType=" + xcontentType +
                '}';
    }
}
