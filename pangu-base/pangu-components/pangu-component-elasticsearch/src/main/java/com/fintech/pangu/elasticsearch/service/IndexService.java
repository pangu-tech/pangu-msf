package com.fintech.pangu.elasticsearch.service;

/**
 * ElasticSearch索引操作服务接口
 *
 * @author xujunqi
 * @since 1.0.0
 */
public interface IndexService {

    /**
     * 创建索引
     *
     * @param index 索引名称
     * @return Boolean 是否创建成功
     * @throws Exception
     */
    Boolean createIndex(String index) throws Exception;

    /**
     * 创建索引, 默认索引名称与文档类型名称相同
     *
     * @param index       索引名称
     * @param jsonMapping mapping映射定义信息(Json格式)
     * @return
     * @throws Exception
     */
    Boolean createIndex(String index, String jsonMapping) throws Exception;

    /**
     * 创建索引
     *
     * @param index       索引名称
     * @param type        文档类型名称
     * @param jsonMapping mapping映射定义信息(Json格式)
     * @return Boolean 是否创建成功
     * @throws Exception
     */
    Boolean createIndex(String index, String type, String jsonMapping) throws Exception;

    /**
     * 删除指定的索引
     *
     * @param index 索引名称
     * @return
     * @throws Exception
     */
    Boolean deleteIndex(String index) throws Exception;

    /**
     * 更改已经索引的mapping映射
     *
     * @param index       索引名称
     * @param type        文档类型
     * @param jsonMapping mapping映射定义信息(Json格式)
     * @return
     * @throws Exception
     */
    Boolean putMapping(String index, String type, String jsonMapping) throws Exception;

    /**
     * 判断索引是否已经存在
     *
     * @param index 索引名称
     * @return
     */
    Boolean indexExists(String index);
}
