package com.fintech.pangu.framework.endpoint;

import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * 针对@PanGuFrameworkEndpoint端点的HandlerMapping
 * @author Trust_FreeDom
 */
public class PanGuFrameworkEndpointHandlerMapping extends RequestMappingHandlerMapping {

    @Override
    protected boolean isHandler(Class<?> beanType) {
        return AnnotationUtils.findAnnotation(beanType, PanGuFrameworkEndpoint.class) != null;
    }

}
