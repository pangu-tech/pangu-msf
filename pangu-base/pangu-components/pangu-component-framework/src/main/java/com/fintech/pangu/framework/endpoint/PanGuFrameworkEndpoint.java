package com.fintech.pangu.framework.endpoint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 盘古微服务框架内使用的暴露Endpoint端点注解
 * @author Trust_FreeDom
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface PanGuFrameworkEndpoint {

}
