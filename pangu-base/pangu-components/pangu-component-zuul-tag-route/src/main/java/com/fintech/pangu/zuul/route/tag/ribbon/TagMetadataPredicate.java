package com.fintech.pangu.zuul.route.tag.ribbon;


import com.fintech.pangu.zuul.route.tag.filter.GrayRequestContext;
import com.netflix.loadbalancer.AbstractServerPredicate;
import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.PredicateKey;
import com.netflix.niws.loadbalancer.DiscoveryEnabledServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * 基于Tag元数据的谓词
 */
@Slf4j
public class TagMetadataPredicate extends AbstractServerPredicate {

    public TagMetadataPredicate(IRule rule) {
        super(rule);
    }

    /**
     * 获取请求头的Tag
     * @return
     */
    private String getTag(){
        // 从GrayRequestContext中获取tag
        String tag = GrayRequestContext.currentRequestContext().getDataValue("tag");
        if(StringUtils.hasText(tag)){
            return tag.trim();
        }

        return null;
    }


    @Override
    public boolean apply(PredicateKey input) {
        // 请求中的tag
        String tag = getTag();
        log.info("current request tag： {}", tag);

        // server元数据
        Map<String, String> metadata = ((DiscoveryEnabledServer) input.getServer()).getInstanceInfo().getMetadata();
        // tag元数据
        String metaTag = metadata.get("tag");
        if(StringUtils.hasText(metaTag)){
            metaTag = metaTag.trim();
        }
        log.info("current instance metadata tag： {}", metaTag);

        // 逻辑：
        // 如果请求中有tag，只能选择server中有相同tag元数据的
        if(StringUtils.hasText(tag)){
            if(tag.equals(metaTag)){
                return true;
            }
        }
        // 如果请求中没有tag，只能选择server中没有tag元数据的
        else{
            if(StringUtils.isEmpty(metaTag)){
                return true;
            }
        }

        return false;
    }
}
