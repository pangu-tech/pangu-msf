package com.fintech.pangu.zuul.route.tag.ribbon;

import com.netflix.loadbalancer.AbstractServerPredicate;
import com.netflix.loadbalancer.CompositePredicate;
import com.netflix.loadbalancer.ZoneAvoidanceRule;
import lombok.extern.slf4j.Slf4j;

/**
 * 继承ZoneAvoidanceRule，只是添加一个基于Tag的谓词TagMetadataPredicate
 */
@Slf4j
public class TagMetadataRule extends ZoneAvoidanceRule {

    protected CompositePredicate tagCompositePredicate;

    public TagMetadataRule(){
        // 初始化ZoneAvoidanceRule
        super();

        // 初始化，添加TagMetadataPredicate到CompositePredicate
        init();
    }

    protected void init() {
        TagMetadataPredicate grayPredicate = new TagMetadataPredicate(this);
        tagCompositePredicate = CompositePredicate.withPredicates(super.getPredicate(),
                grayPredicate).build();
    }

    /**
     * 重写ZoneAvoidanceRule的getPredicate()，返回添加了tag谓词策略的tagCompositePredicate
     * @return
     */
    @Override
    public AbstractServerPredicate getPredicate() {
        return tagCompositePredicate;
    }

}
