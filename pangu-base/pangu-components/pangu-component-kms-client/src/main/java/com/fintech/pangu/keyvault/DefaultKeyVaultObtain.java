package com.fintech.pangu.keyvault;


/**
 * @Description:
 */
public class DefaultKeyVaultObtain implements KeyVaultObtain {

    //@Autowired
    private KeyVaultProperties keyVaultProperties;

    public DefaultKeyVaultObtain(KeyVaultProperties keyVaultProperties) {
        this.keyVaultProperties = keyVaultProperties;
    }

    @Override
    public String getKey() {
        if(keyVaultProperties != null){
            return keyVaultProperties.getDefaultKey();
        }

        return null;
    }

}
