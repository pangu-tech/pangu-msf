package com.fintech.pangu.keyvault;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class KeyVaultProperties {

    private String defaultKey;

    private String serverUri;

}
