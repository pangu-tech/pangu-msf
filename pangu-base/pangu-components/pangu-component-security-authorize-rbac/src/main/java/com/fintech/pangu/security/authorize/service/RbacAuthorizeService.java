package com.fintech.pangu.security.authorize.service;

import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;

/**
 * RBAC权限校验服务接口
 */
public interface RbacAuthorizeService {

    /**
     * 通过request请求数据、认证用户数据、配置数据校验用户的对当前请求是否有权限
     * @param request
     * @param authentication
     * @return  有权限:true
     */
    boolean hasPermission(HttpServletRequest request, Authentication authentication);

}
