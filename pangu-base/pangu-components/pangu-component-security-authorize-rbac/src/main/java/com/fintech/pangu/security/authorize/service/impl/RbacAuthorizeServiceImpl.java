package com.fintech.pangu.security.authorize.service.impl;

import com.fintech.pangu.security.authorize.service.RbacAuthorizeService;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * RBAC权限校验服务接口
 */
public class RbacAuthorizeServiceImpl implements RbacAuthorizeService {


    /**
     * 通过request请求数据、认证用户数据、配置数据校验用户的对当前请求是否有权限
     * @param request
     * @param authentication
     * @return  有权限:true
     */
    @Override
    public boolean hasPermission(HttpServletRequest request, Authentication authentication) {



        return false;
    }


    /**
     * 根据用户名找到用户拥有权限的UrlPatterns
     * @param userName
     * @return
     */
    private List<String> findAccessUrlPatternsByUsername(String userName){
        List<String> accessUrlPatterns = new ArrayList<>();



        return accessUrlPatterns;
    }

}
