package com.fintech.pangu.security.authorize;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/**
 * Rbac权限配置提供者
 */
public class RbacAuthorizeConfigProvider implements AuthorizeConfigProvider {

    // 服务名，默认值为 rbacAuthorizeService
    //private String rbacAuthorizeServiceName = "RbacAuthorizeService";

    /**
     * @param rbacAuthorizeServiceName  实现rbac授权服务的服务类在Spring容器中注册的BeanName
     */
    //public RbacAuthorizeConfigProvider(String rbacAuthorizeServiceName) {
    //    if(StringUtils.hasText(rbacAuthorizeServiceName)){
    //        this.rbacAuthorizeServiceName = rbacAuthorizeServiceName;
    //    }
    //}

    /**
     *
     * @param config
     * @return 返回的boolean表示配置中是否有针对anyRequest的配置
     */
    @Override
    public boolean config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
        config
            .anyRequest().access("@rbacAuthorizeService.hasPermission(request, authentication)");

        return true; // 包含anyRequest的配置
    }


}
