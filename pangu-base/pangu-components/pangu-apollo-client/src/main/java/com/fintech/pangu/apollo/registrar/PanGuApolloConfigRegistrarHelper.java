package com.fintech.pangu.apollo.registrar;

import com.ctrip.framework.apollo.spring.spi.DefaultApolloConfigRegistrarHelper;
import com.fintech.pangu.apollo.conditional.PanGuApolloUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.core.type.AnnotationMetadata;

/**
 * @Description:
 */
@Slf4j
public class PanGuApolloConfigRegistrarHelper extends DefaultApolloConfigRegistrarHelper {

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
        if (!PanGuApolloUtil.isLoadApolloConfig()){
            log.warn("not found apollo config,load by configLocation");
            return;
        }
        super.registerBeanDefinitions(importingClassMetadata, registry);
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
