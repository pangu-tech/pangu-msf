package com.fintech.pangu.apollo.autorefresh;

import org.springframework.context.ApplicationEvent;

import java.util.Set;

/**
 * @Description:
 */
@SuppressWarnings("serial")
public class AutoRefreshChangeEvent extends ApplicationEvent {

    private Set<String> beanNames;

    public AutoRefreshChangeEvent(Set<String> beanNames) {
        super(beanNames);
        this.beanNames = beanNames;
    }

    public Set<String> getBeanNames() {
        return beanNames;
    }
}
