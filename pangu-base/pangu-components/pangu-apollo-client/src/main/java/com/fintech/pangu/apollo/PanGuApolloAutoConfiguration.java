package com.fintech.pangu.apollo;

import com.fintech.pangu.apollo.annotation.EnablePanGuApolloConfig;
import com.fintech.pangu.apollo.autorefresh.AutoRefreshChangeRebinder;
import com.fintech.pangu.apollo.autorefresh.AutoRefreshConfig;
import com.fintech.pangu.apollo.conditional.LoadApolloConfigConditional;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.ConfigurationPropertiesBindingPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

/**
 * @Description:
 */
@Configuration
@Conditional({LoadApolloConfigConditional.class})
@ConditionalOnClass(EnablePanGuApolloConfig.class)
public class PanGuApolloAutoConfiguration {

    private ConfigurationPropertiesBindingPostProcessor binder;

    public PanGuApolloAutoConfiguration(ConfigurationPropertiesBindingPostProcessor binder){
        this.binder = binder;
    }

    @Bean
    public AutoRefreshConfig autoRefreshConfig(){
        return new AutoRefreshConfig();
    }

    @Bean
    public AutoRefreshChangeRebinder autoRefreshChangeRebinder(){
        return new AutoRefreshChangeRebinder(binder);
    }
}
