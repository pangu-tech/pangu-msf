package com.fintech.pangu.apollo.annotation;

import com.ctrip.framework.apollo.core.ConfigConsts;
import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Description:
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@EnableApolloConfig
public @interface EnablePanGuApolloConfig {

    /**
     * Apollo namespaces to inject configuration into Spring Property Sources.
     */
    @AliasFor(annotation = EnableApolloConfig.class)
    String[] value() default {ConfigConsts.NAMESPACE_APPLICATION};

    /**
     * The order of the apollo config, default is {@link Ordered#LOWEST_PRECEDENCE}, which is Integer.MAX_VALUE.
     * If there are properties with the same name in different apollo configs, the apollo config with smaller order wins.
     * @return
     */
    @AliasFor(annotation = EnableApolloConfig.class)
    int order() default Ordered.LOWEST_PRECEDENCE;
}
