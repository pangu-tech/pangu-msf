package com.fintech.pangu.apollo.conditional;

import com.ctrip.framework.foundation.Foundation;
import org.springframework.util.StringUtils;

/**
 * @Description:
 */
public class PanGuApolloUtil {

    public static boolean isLoadApolloConfig(){
        String appId = Foundation.app().getAppId();
        if (StringUtils.isEmpty(appId)){
            return false;
        }
        return true;
    }
}
