package com.fintech.pangu.security.authentication.token;

import java.util.Date;

/**
 * 即将到期（可过期）的认证令牌
 *
 * @author Trust_FreeDom
 */
public interface ExpiringAuthenticationToken extends AuthenticationToken {

    /**
     * 获取过期时间
     * @return
     */
    Date getExpiration();

    /**
     * 设置过期时间
     * @param expiration
     */
    void setExpiration(Date expiration);

    /**
     * 是否已过期
     * @return
     */
    boolean isExpired();

}
