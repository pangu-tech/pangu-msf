package com.fintech.pangu.security.authentication.token;

import java.util.Map;

/**
 * 认证令牌
 *
 * @author Trust_FreeDom
 */
public interface AuthenticationToken {

    /**
     * 获取token值
     * @return
     */
    String getValue();

    /**
     * 设置token值
     * @param value
     */
    void setValue(String value);

    /**
     * 获取附加信息
     * @return
     */
    Map<String, Object> getAdditionalInformation();

    /**
     * 设置附加信息
     * @param additionalInformation
     */
    void setAdditionalInformation(Map<String, Object> additionalInformation);

}
