package com.fintech.pangu.security.authentication.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * API登出处理器
 *
 * @author Trust_FreeDom
 */
@Slf4j
public class ApiLogoutHandler implements LogoutHandler {


    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
        try {
            // logout登记等操作
            // TODO

        }
        catch (Exception e){
            log.error("api logout handler error: ", e);
        }
    }

}
