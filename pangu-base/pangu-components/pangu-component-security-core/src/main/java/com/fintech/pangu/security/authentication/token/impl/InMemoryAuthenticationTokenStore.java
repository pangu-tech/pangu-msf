package com.fintech.pangu.security.authentication.token.impl;

import com.fintech.pangu.security.authentication.token.AuthenticationToken;
import com.fintech.pangu.security.authentication.token.AuthenticationTokenStore;
import org.springframework.security.core.Authentication;

/**
 * 基于内存的token存储
 */
public class InMemoryAuthenticationTokenStore implements AuthenticationTokenStore {

    @Override
    public AuthenticationToken getAuthenticationToken(Authentication authentication) {
        return null;
    }

    @Override
    public void storeAuthenticationToken(AuthenticationToken token, Authentication authentication) {

    }

    @Override
    public void removeAuthenticationToken(AuthenticationToken token) {

    }

}
