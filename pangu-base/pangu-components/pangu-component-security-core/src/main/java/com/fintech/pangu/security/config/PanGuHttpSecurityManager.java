package com.fintech.pangu.security.config;

import lombok.Setter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

import java.util.List;

/**
 * PanGuHttpSecurityConfigurer配置管理器
 */
@Setter
public class PanGuHttpSecurityManager {

    private List<PanGuHttpSecurityConfigurer> panGuHttpSecurityConfigurers;

    public PanGuHttpSecurityManager() {
    }


    /**
     * 遍历并应用所有PanGuHttpSecurityConfigurer
     * @param http
     * @throws Exception
     */
    public void config(HttpSecurity http) throws Exception {
        if(panGuHttpSecurityConfigurers!=null && panGuHttpSecurityConfigurers.size()>0){
            for(PanGuHttpSecurityConfigurer panGuHttpSecurityConfigurer : panGuHttpSecurityConfigurers){
                //panGuHttpSecurityConfigurer.configure(http); // 这样写子配置中使用http.getSharedObject(AuthenticationManager.class)返回null
                                                               // 因为直接给子配置传入http，那么子配置仅仅是方法调用，而不是一个真正的子配置
                                                               // 其会在WebSecurityConfigurer的init()环节发生调用，此时是初始化环节
                                                               // 而http.setSharedObject(AuthenticationManager.class)是在beforeConfigure()环节创建并设置的
                                                               // 所以应使用http.apply()将其设置为一个真正的子配置
                http.apply(panGuHttpSecurityConfigurer);
            }
        }
    }

}
