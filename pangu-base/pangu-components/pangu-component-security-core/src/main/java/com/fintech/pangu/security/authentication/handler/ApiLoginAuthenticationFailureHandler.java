package com.fintech.pangu.security.authentication.handler;

import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.commons.enums.BusinessEnum;
import com.fintech.pangu.commons.util.HttpUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * API Login认证失败处理器
 */
@Slf4j
public class ApiLoginAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
        HttpUtil.responseForJson(response, BaseResponse.failure(BusinessEnum.USERNAME_PASSWORD_ERROR));
    }

}
