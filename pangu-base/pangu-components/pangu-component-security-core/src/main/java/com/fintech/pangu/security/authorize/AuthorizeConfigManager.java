package com.fintech.pangu.security.authorize;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/**
 * 权限配置信息管理器
 */
public interface AuthorizeConfigManager {

    /**
     * 权限配置信息管理器内部会找到Spring容器内所有AuthorizeConfigProvider
     * 并逐个遍历添加对应的权限配置
     * @param config
     */
    void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config);

}
