package com.fintech.pangu.security.config;

import lombok.Setter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

import java.util.List;

/**
 * PanGuAuthenticationManagerBuilderConfigurer配置管理器
 *
 * @author Trust_FreeDom
 */
@Setter
public class PanGuAuthenticationManagerBuilderManager {

    private List<PanGuAuthenticationManagerBuilderConfigurer> panGuAuthenticationManagerBuilderConfigurers;

    public PanGuAuthenticationManagerBuilderManager() {
    }

    /**
     * 遍历并应用所有PanGuAuthenticationManagerBuilderConfigurer
     * @param auth
     * @throws Exception
     */
    public void config(AuthenticationManagerBuilder auth) throws Exception {
        if(panGuAuthenticationManagerBuilderConfigurers!=null && panGuAuthenticationManagerBuilderConfigurers.size()>0){
            for(PanGuAuthenticationManagerBuilderConfigurer panGuAuthenticationManagerBuilderConfigurer : panGuAuthenticationManagerBuilderConfigurers){
                auth.apply(panGuAuthenticationManagerBuilderConfigurer);
            }
        }
    }

}
