package com.fintech.pangu.security.authorize;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;

/**
 * 权限配置提供器
 * 各个模块和业务系统可以通过实现此接口向系统添加授权配置
 */
public interface AuthorizeConfigProvider {

    /**
     * 在整个权限配置中，应该有且仅有一个针对anyRequest的配置，否则后面加载到的会覆盖前面的
     * 如果所有的实现都没有针对anyRequest的配置，系统会自动增加一个anyRequest().authenticated()的配置
     * 如果有多个针对anyRequest的配置，则会抛出异常
     * @param config
     * @return 返回的boolean表示配置中是否有针对anyRequest的配置
     */
    boolean config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config);

}
