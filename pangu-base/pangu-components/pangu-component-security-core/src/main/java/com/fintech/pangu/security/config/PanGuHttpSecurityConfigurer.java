package com.fintech.pangu.security.config;

import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;

/**
 * PanGuHttpSecurityConfigurer用于为盘古微服务框架配置HttpSecurity提供入口
 * 便于将一段子配置注入安全框架
 * 注意：不可在configure(HttpSecurity http)方法中再添加其它安全子配置，如 http.formLogin()
 */
public class PanGuHttpSecurityConfigurer extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

}
