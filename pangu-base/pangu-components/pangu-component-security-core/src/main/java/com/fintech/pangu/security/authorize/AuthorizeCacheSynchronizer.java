package com.fintech.pangu.security.authorize;

/**
 * 权限缓存同步器
 */
public interface AuthorizeCacheSynchronizer {


    /**
     * 启动同步器
     * @param initialDelayMs  启动延时(ms)
     * @param intervalMs      运行间隔(ms)
     */
    void start(int initialDelayMs, int intervalMs);

    /**
     * 停止同步器
     */
    void stop();

    /**
     * 全量同步
     */
    void sync();


    /**
     * 手工全量同步
     */
    void onDemandSync();

}
