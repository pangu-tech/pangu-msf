package com.fintech.pangu.security.authentication.token;

import org.springframework.security.core.Authentication;

/**
 * 复合的认证令牌存储
 * 普通认证令牌存储 + 刷新认证令牌存储
 *
 * @author Trust_FreeDom
 */
public interface CompositeAuthenticationTokenStore extends AuthenticationTokenStore {

    /**
     * 按照刷新令牌value读取令牌
     * @param tokenValue
     * @return
     */
    AuthenticationRefreshToken readAuthenticationRefreshToken(String tokenValue);


    /**
     * 持久化刷新令牌
     * @param refreshToken
     * @param authentication
     */
    void storeAuthenticationRefreshToken(AuthenticationRefreshToken refreshToken, Authentication authentication);


    /**
     * 删除刷新令牌
     * @param refreshToken
     */
    void removeAuthenticationRefreshToken(AuthenticationRefreshToken refreshToken);

}
