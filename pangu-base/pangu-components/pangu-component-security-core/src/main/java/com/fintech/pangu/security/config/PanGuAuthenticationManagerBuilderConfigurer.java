package com.fintech.pangu.security.config;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

/**
 * PanGuAuthenticationManagerBuilderConfigurer用于为盘古微服务框架配置AuthenticationManagerBuilder提供入口
 * 所有PanGuAuthenticationManagerBuilderConfigurer统一由PanGuAuthenticationManagerBuilderManager管理
 *
 * @author Trust_FreeDom
 */
public class PanGuAuthenticationManagerBuilderConfigurer extends SecurityConfigurerAdapter<AuthenticationManager, AuthenticationManagerBuilder> {


}
