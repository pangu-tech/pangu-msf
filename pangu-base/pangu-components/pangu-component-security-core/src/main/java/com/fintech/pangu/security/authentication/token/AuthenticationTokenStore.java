package com.fintech.pangu.security.authentication.token;

import org.springframework.security.core.Authentication;

/**
 * 认证令牌存储接口
 */
public interface AuthenticationTokenStore {

    /**
     * 按照Authentication查找认证令牌
     * @param authentication
     * @return
     */
    AuthenticationToken getAuthenticationToken(Authentication authentication);


    /**
     * 持久化认证令牌
     * @param token
     * @param authentication
     */
    void storeAuthenticationToken(AuthenticationToken token, Authentication authentication);


    /**
     * 删除认证令牌
     * @param token
     */
    void removeAuthenticationToken(AuthenticationToken token);

}
