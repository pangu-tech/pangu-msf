package com.fintech.pangu.security.authentication.token;

/**
 * 刷新认证令牌（用于刷新认证令牌的令牌）
 * 暂时只是一个标识接口，其实质就是一个可过期的认证令牌
 *
 * @author Trust_FreeDom
 */
public interface AuthenticationRefreshToken extends ExpiringAuthenticationToken {

}
