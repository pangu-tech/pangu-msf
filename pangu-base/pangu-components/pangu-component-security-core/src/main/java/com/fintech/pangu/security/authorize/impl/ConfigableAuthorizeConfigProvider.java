package com.fintech.pangu.security.authorize.impl;

import com.fintech.pangu.security.authorize.AuthorizeConfigProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * 可配置的权限配置提供者
 */
public abstract class ConfigableAuthorizeConfigProvider implements AuthorizeConfigProvider {

    /**
     * 子类实现，提供通过配置获得的permitAllUrls
     * @return
     */
    protected abstract List<String> permitAllUrls();

    /**
     * 子类实现，默认的permitAllUrls
     * @return
     */
    protected abstract List<String> defaultPermitAllUrls();


    @Override
    public boolean config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
        // 1、默认的permitAll配置
        List<String> defaultPermitAllUrls = defaultPermitAllUrls();
        if(defaultPermitAllUrls!=null && defaultPermitAllUrls.size()>0){
            for(String defaultPermitAllUrl : defaultPermitAllUrls){
                if(StringUtils.hasText(defaultPermitAllUrl)){
                    config.antMatchers(defaultPermitAllUrl).permitAll();
                }
            }
        }

        // 2、permitAll配置
        List<String> permitAllUrls = permitAllUrls();
        if(permitAllUrls!=null && permitAllUrls.size()>0){
            for(String permitAllUrl : permitAllUrls){
                if(StringUtils.hasText(permitAllUrl)){
                    config.antMatchers(permitAllUrl).permitAll();
                }
            }
        }

        return false;
    }

}
