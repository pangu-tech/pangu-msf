package com.fintech.pangu.security.authentication.handler;

import com.fintech.pangu.security.authentication.token.AuthenticationToken;
import com.fintech.pangu.security.authentication.token.AuthenticationTokenResponder;
import com.fintech.pangu.security.authentication.token.AuthenticationTokenServices;
import lombok.Setter;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * API Login认证成功处理器
 */
@Setter
public class ApiLoginAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    /**
     * token服务
     * 用于生成、获取令牌
     */
    //@Autowired
    private AuthenticationTokenServices authenticationTokenServices;

    /**
     * 处理如何响应token
     */
    //@Autowired
    private AuthenticationTokenResponder authenticationTokenResponder;


    /**
     * 生成认证令牌token并以某种形式返回给调用方
     * 1、tokenGenerator（tokenServices、tokenStore）
     * 2、tokenResponder
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        // 1、生成令牌
        AuthenticationToken token = authenticationTokenServices.createAuthenticationToken(authentication);

        // 2、处理认证Token响应
        authenticationTokenResponder.handlingResponse(token, response);
    }

}
