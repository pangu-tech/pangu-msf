package com.fintech.pangu.security.authentication.token;

import org.springframework.security.core.Authentication;

/**
 * 复合的认证令牌服务接口
 *
 * @author Trust_FreeDom
 */
public interface CompositeAuthenticationTokenServices extends AuthenticationTokenServices {

    /**
     * 创建刷新认证令牌
     * @param authentication
     * @return
     */
    AuthenticationRefreshToken createAuthenticationRefreshToken(Authentication authentication);


    /**
     * 刷新认证令牌
     */
    AuthenticationToken refreshAuthenticationToken(String refreshToken);

}
