package com.fintech.pangu.security.authentication.model;

import lombok.Data;

@Data
public class SecurityUser {
    /**
     * 用户名
     */
    private String username;
    /**
     * 用户密码
     */
    private String password;
    /**
     * 用户角色集合
     */
    private String[] roles = new String[]{"api"};
}
