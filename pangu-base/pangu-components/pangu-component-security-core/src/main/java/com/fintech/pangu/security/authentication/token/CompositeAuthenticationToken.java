package com.fintech.pangu.security.authentication.token;

/**
 * 复合的认证令牌（包含刷新令牌的认证令牌）
 *
 * @author Trust_FreeDom
 */
public interface CompositeAuthenticationToken extends ExpiringAuthenticationToken {

    /**
     * 获取刷新令牌
     * @return
     */
    AuthenticationRefreshToken getRefreshToken();

}
