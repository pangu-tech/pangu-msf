package com.fintech.pangu.security.authentication.token;

import org.springframework.security.core.Authentication;

/**
 * 认证令牌服务接口
 * 负责创建、获取认证令牌
 */
public interface AuthenticationTokenServices {

    /**
     * 创建认证令牌
     * @param authentication
     * @return
     */
    AuthenticationToken createAuthenticationToken(Authentication authentication);


    /**
     * 获取认证令牌
     * @param authentication
     * @return
     */
    AuthenticationToken getAuthenticationToken(Authentication authentication);

}
