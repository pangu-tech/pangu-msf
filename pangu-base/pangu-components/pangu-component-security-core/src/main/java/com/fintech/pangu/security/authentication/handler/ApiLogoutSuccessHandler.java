package com.fintech.pangu.security.authentication.handler;

import com.fintech.pangu.security.authentication.token.AuthenticationToken;
import com.fintech.pangu.security.authentication.token.AuthenticationTokenResponder;
import com.fintech.pangu.security.authentication.token.impl.DefaultAuthenticationToken;
import lombok.Setter;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * API登出成功处理器
 *
 * @author Trust_FreeDom
 */
@Setter
public class ApiLogoutSuccessHandler implements LogoutSuccessHandler {

    /**
     * 处理如何响应token
     */
    //@Autowired
    private AuthenticationTokenResponder authenticationTokenResponder;


    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        // 处理logout响应
        AuthenticationToken token = new DefaultAuthenticationToken();
        token.setValue(""); //清空认证Token
        authenticationTokenResponder.handlingResponse(token, response);
    }

}
