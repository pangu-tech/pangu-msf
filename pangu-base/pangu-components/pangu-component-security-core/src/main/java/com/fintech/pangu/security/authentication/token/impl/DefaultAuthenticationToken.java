package com.fintech.pangu.security.authentication.token.impl;

import com.fintech.pangu.security.authentication.token.AuthenticationToken;

import java.io.Serializable;
import java.util.Collections;
import java.util.Map;

/**
 * 默认认证Token
 */
public class DefaultAuthenticationToken implements Serializable, AuthenticationToken {

    /** 认证token值 */
    private String value;

    /** 过期时间 */
    //private Date expiration;

    // 暂不实现
    //private AuthenticationRefreshToken refreshToken;

    /** 附加信息 */
    private Map<String, Object> additionalInformation = Collections.emptyMap();


    public DefaultAuthenticationToken() {
    }

    public DefaultAuthenticationToken(String value) {
        this.value = value;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public Map<String, Object> getAdditionalInformation() {
        return additionalInformation;
    }

    @Override
    public void setAdditionalInformation(Map<String, Object> additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    /**
     * 是否过期
     * @return
     */
    //@Override
    //public boolean isExpired() {
    //    return expiration != null && expiration.before(new Date());
    //}

}
