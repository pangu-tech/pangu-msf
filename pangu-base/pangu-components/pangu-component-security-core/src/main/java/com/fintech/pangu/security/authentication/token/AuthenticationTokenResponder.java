package com.fintech.pangu.security.authentication.token;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 处理认证Token响应接口
 */
public interface AuthenticationTokenResponder {

    void handlingResponse(AuthenticationToken token, HttpServletResponse response) throws IOException;

}
