package com.fintech.pangu.web.http.converter;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class FastJsonProperties {

    private boolean enabled = true;
}
