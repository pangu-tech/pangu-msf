package com.fintech.pangu.web.config;

/**
 * @Description:
 */
public abstract class PanGuResourceHandler {

    abstract public String[] pathPatterns();

    abstract public String[] resourceLocations();
}
