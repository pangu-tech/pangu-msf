package com.fintech.pangu.web.http.converter;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description:
 */
public class FastJsonConverter {

    public void build(List<HttpMessageConverter<?>> converters){
            //1、定义一个convert转换消息的对象
            FastJsonHttpMessageConverter fastConverter = new FastJsonHttpMessageConverter();
            //2、添加fastjson的配置信息
            FastJsonConfig fastJsonConfig = new FastJsonConfig();
            fastJsonConfig.setDateFormat(JSONObject.DEFFAULT_DATE_FORMAT);
            fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat,SerializerFeature.WriteMapNullValue);
            //3、在convert中添加配置信息
            fastConverter.setFastJsonConfig(fastJsonConfig);

            List<MediaType> mediaTypeList=new ArrayList<>();
            mediaTypeList.add(MediaType.APPLICATION_JSON_UTF8);
            mediaTypeList.add(MediaType.APPLICATION_JSON);
            fastConverter.setSupportedMediaTypes(mediaTypeList);
            converters.add(0,fastConverter);
    }
}
