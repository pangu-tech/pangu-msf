package com.fintech.pangu.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

import java.util.List;
import java.util.Optional;

/**
 * @Description:
 */
public class PanGuWebMvcManager {

    @Autowired(required = false)
    private List<PanGuHandlerInterceptor> panGuHandlerInterceptors;

    @Autowired(required = false)
    private List<PanGuResourceHandler> panGuResourceHandlers;

    /**
     * 循环加载拦截器至spring容器中
     * @param registry
     */
    public void addPanGuHandlerInterceptor(InterceptorRegistry registry){
        Optional.ofNullable(panGuHandlerInterceptors).ifPresent(list -> list.stream().forEach(panGuHandlerInterceptor -> {
            registry.addInterceptor(panGuHandlerInterceptor)
                    .addPathPatterns(panGuHandlerInterceptor.pathPatterns())
                    .order(panGuHandlerInterceptor.interceptorOrder());
        }));
    }

    public void addPanGuResourceHandler(ResourceHandlerRegistry registry){
        Optional.ofNullable(panGuResourceHandlers).ifPresent(list -> list.stream().forEach(panGuResourceHandler -> {
            registry.addResourceHandler(panGuResourceHandler.pathPatterns())
                    .addResourceLocations(panGuResourceHandler.resourceLocations());
        }));
    }
}
