package com.fintech.pangu.web.util;


import com.alibaba.ttl.TransmittableThreadLocal;
import com.fintech.pangu.commons.model.UserDetail;

/**
 * @Description:
 */
public class UserInfoUtils {

    private static final TransmittableThreadLocal<UserDetail> threadLocal = new TransmittableThreadLocal<UserDetail>();

    public static UserDetail get() {
        return  threadLocal.get();
    }

    public static void set(UserDetail userDetail) {
        threadLocal.set(userDetail);
    }

    public static void remove() {
        threadLocal.remove();
    }
}
