package com.fintech.pangu.web.config;

import com.fintech.pangu.web.http.converter.FastJsonConverter;
import com.fintech.pangu.web.http.converter.FastJsonProperties;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * @Description:
 */
public class PanGuWebMvcConfigurer implements WebMvcConfigurer {

    private PanGuWebMvcManager panGuWebMvcManager;

    private FastJsonProperties fastJsonProperties;

    public PanGuWebMvcConfigurer(PanGuWebMvcManager panGuWebMvcManager,FastJsonProperties fastJsonProperties){
        this.panGuWebMvcManager = panGuWebMvcManager;
        this.fastJsonProperties = fastJsonProperties;
    }

    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        if (fastJsonProperties.isEnabled()){
            FastJsonConverter fastJsonConverter = new FastJsonConverter();
            fastJsonConverter.build(converters);
        }
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        panGuWebMvcManager.addPanGuHandlerInterceptor(registry);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        panGuWebMvcManager.addPanGuResourceHandler(registry);
    }
}
