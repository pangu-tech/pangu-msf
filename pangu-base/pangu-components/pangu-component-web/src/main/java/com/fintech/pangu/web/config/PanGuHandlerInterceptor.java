package com.fintech.pangu.web.config;

import org.springframework.web.servlet.HandlerInterceptor;

/**
 * @Description:
 */
public abstract class PanGuHandlerInterceptor implements HandlerInterceptor {

    /**
     * 设置拦截器顺序
     * @return
     */
    abstract public int interceptorOrder();

    /**
     * 设置拦截请求的path
     * @return
     */
    abstract public String pathPatterns();
}
