package com.fintech.pangu.elasticjob.autoconfigure;

import com.dangdang.ddframe.job.event.JobEventConfiguration;
import com.dangdang.ddframe.job.event.rdb.JobEventRdbConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperConfiguration;
import com.dangdang.ddframe.job.reg.zookeeper.ZookeeperRegistryCenter;
import com.fintech.pangu.elasticjob.annotation.EnableElasticJob;
import com.fintech.pangu.elasticjob.core.ElasticScheduledJobConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

/**
 * @author xujunqi
 * @date 2019/9/5 15:43
 * @since 1.0.0
 */
@Configuration
@ConditionalOnBean(annotation = EnableElasticJob.class)
@ConditionalOnProperty(prefix = "pangu.elasticjob", name = "enable", matchIfMissing = true)
@EnableConfigurationProperties(ElasticJobProperties.class)
public class ElasticJobAutoConfiguration {
    /**
     * 配置加载zookeeper注册中心
     */
    @Bean(initMethod = "init", name = "elasticJobRegCenter")
    @ConditionalOnClass(ZookeeperConfiguration.class)
    public ZookeeperRegistryCenter regCenter(ElasticJobProperties elasticJobProperties) {
        ZookeeperConfiguration zookeeperConfiguration = new ZookeeperConfiguration(elasticJobProperties.getServerLists(), elasticJobProperties.getNamespace());
        zookeeperConfiguration.setBaseSleepTimeMilliseconds(elasticJobProperties.getBaseSleepTimeMilliseconds());
        zookeeperConfiguration.setMaxSleepTimeMilliseconds(elasticJobProperties.getMaxSleepTimeMilliseconds());
        zookeeperConfiguration.setMaxRetries(elasticJobProperties.getMaxRetries());
        zookeeperConfiguration.setSessionTimeoutMilliseconds(elasticJobProperties.getSessionTimeoutMilliseconds());
        zookeeperConfiguration.setConnectionTimeoutMilliseconds(elasticJobProperties.getConnectionTimeoutMilliseconds());
        zookeeperConfiguration.setDigest(elasticJobProperties.getDigest());

        return new ZookeeperRegistryCenter(zookeeperConfiguration);
    }

    /**
     * 配置加载时间记录中心, 默认不开启
     * 需要依赖数据库，默认MySQL
     */
    @Bean
    @ConditionalOnClass(DataSource.class)
    @ConditionalOnBean(value = DataSource.class, name = "elasticJobDataSource")
    @ConditionalOnProperty(prefix = "pangu.elasticjob.jobevent", name = "enable", matchIfMissing = true)
    public JobEventConfiguration jobEventConfiguration(DataSource elasticJobDataSource) {
        return new JobEventRdbConfiguration(elasticJobDataSource);
    }

    @Bean
    @ConditionalOnClass(ElasticScheduledJobConfiguration.class)
    public ElasticScheduledJobConfiguration elasticScheduledJobConfiguration(ZookeeperRegistryCenter regCenter) {
        return new ElasticScheduledJobConfiguration(regCenter);
    }
}
