package com.fintech.pangu.elasticjob.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * elastic job 全局配置属性类
 *
 * @author xujunqi
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = "pangu.elasticjob.config")
public class ElasticJobProperties {

    /**
     * 连接Zookeeper服务器的列表
     * 包括IP地址和端口号
     * 多个地址用逗号分隔
     * 如: host1:2181,host2:2181
     * 必须传递参数
     */
    private String serverLists;

    /**
     * Zookeeper的命名空间
     * 必须传递参数
     */
    private String namespace;

    /**
     * 等待重试的间隔时间的初始值
     * 单位：毫秒，默认值：1000
     */
    private int baseSleepTimeMilliseconds = 1000;

    /**
     * 等待重试的间隔时间的最大值
     * 单位：毫秒，默认值：3000
     */
    private int maxSleepTimeMilliseconds = 3000;

    /**
     * 最大重试次数
     * 默认值：3
     */
    private int maxRetries = 3;

    /**
     * 会话超时时间
     * 单位：毫秒，默认值：60000
     */
    private int sessionTimeoutMilliseconds;

    /**
     * 连接超时时间
     * 单位：毫秒，默认值：15000
     */
    private int connectionTimeoutMilliseconds;

    /**
     * 连接Zookeeper的权限令牌
     * 缺省为不需要权限验证
     * 默认值：空
     */
    private String digest;

    public String getServerLists() {
        return serverLists;
    }

    public ElasticJobProperties setServerLists(String serverLists) {
        this.serverLists = serverLists;
        return this;
    }

    public String getNamespace() {
        return namespace;
    }

    public ElasticJobProperties setNamespace(String namespace) {
        this.namespace = namespace;
        return this;
    }

    public int getBaseSleepTimeMilliseconds() {
        return baseSleepTimeMilliseconds;
    }

    public ElasticJobProperties setBaseSleepTimeMilliseconds(int baseSleepTimeMilliseconds) {
        this.baseSleepTimeMilliseconds = baseSleepTimeMilliseconds;
        return this;
    }

    public int getMaxSleepTimeMilliseconds() {
        return maxSleepTimeMilliseconds;
    }

    public ElasticJobProperties setMaxSleepTimeMilliseconds(int maxSleepTimeMilliseconds) {
        this.maxSleepTimeMilliseconds = maxSleepTimeMilliseconds;
        return this;
    }

    public int getMaxRetries() {
        return maxRetries;
    }

    public ElasticJobProperties setMaxRetries(int maxRetries) {
        this.maxRetries = maxRetries;
        return this;
    }

    public int getSessionTimeoutMilliseconds() {
        return sessionTimeoutMilliseconds;
    }

    public ElasticJobProperties setSessionTimeoutMilliseconds(int sessionTimeoutMilliseconds) {
        this.sessionTimeoutMilliseconds = sessionTimeoutMilliseconds;
        return this;
    }

    public int getConnectionTimeoutMilliseconds() {
        return connectionTimeoutMilliseconds;
    }

    public ElasticJobProperties setConnectionTimeoutMilliseconds(int connectionTimeoutMilliseconds) {
        this.connectionTimeoutMilliseconds = connectionTimeoutMilliseconds;
        return this;
    }

    public String getDigest() {
        return digest;
    }

    public ElasticJobProperties setDigest(String digest) {
        this.digest = digest;
        return this;
    }
}
