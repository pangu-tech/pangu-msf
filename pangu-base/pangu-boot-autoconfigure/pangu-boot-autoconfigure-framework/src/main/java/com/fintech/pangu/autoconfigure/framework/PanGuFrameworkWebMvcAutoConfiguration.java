package com.fintech.pangu.autoconfigure.framework;

import com.fintech.pangu.framework.endpoint.PanGuFrameworkEndpointHandlerMapping;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * 盘古微服务框架内 Web Mvc 自动配置
 * @author Trust_FreeDom
 */
@Configuration
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
@ConditionalOnClass({ RequestMappingHandlerMapping.class })
public class PanGuFrameworkWebMvcAutoConfiguration {

    /**
     * 添加处理@PanGuFrameworkEndpoint端点的HandlerMapping
     * @return
     */
    @Bean
    public PanGuFrameworkEndpointHandlerMapping panGuFrameworkEndpointHandlerMapping(){
        PanGuFrameworkEndpointHandlerMapping panGuFrameworkEndpointHandlerMapping = new PanGuFrameworkEndpointHandlerMapping();
        panGuFrameworkEndpointHandlerMapping.setOrder(0); //与RequestMappingHandlerMapping排序相同，使其排在RequestMappingHandlerMapping之后
        return panGuFrameworkEndpointHandlerMapping;
    }


}
