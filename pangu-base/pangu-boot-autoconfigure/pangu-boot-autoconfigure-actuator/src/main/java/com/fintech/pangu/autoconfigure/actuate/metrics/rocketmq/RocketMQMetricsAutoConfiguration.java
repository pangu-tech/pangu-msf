package com.fintech.pangu.autoconfigure.actuate.metrics.rocketmq;

import com.fintech.pangu.actuate.metrics.rocketmq.MicrometerRocketMQMetricsCollector;
import com.fintech.pangu.actuate.metrics.rocketmq.RocketMQConsumeMetrics;
import com.fintech.pangu.actuate.metrics.rocketmq.RocketMQSendMetrics;
import com.fintech.pangu.rocketmq.autoconfigure.RocketMQAutoConfiguration;
import com.fintech.pangu.rocketmq.core.producer.RocketMQTemplate;
import com.fintech.pangu.rocketmq.core.producer.hook.SendMessageMetricHookImpl;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.metrics.export.simple.SimpleMetricsExportAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter({ MetricsAutoConfiguration.class, RocketMQAutoConfiguration.class,
        SimpleMetricsExportAutoConfiguration.class })
@ConditionalOnClass({ RocketMQTemplate.class, SendMessageMetricHookImpl.class })
@ConditionalOnBean({ RocketMQTemplate.class, MeterRegistry.class })
public class RocketMQMetricsAutoConfiguration {

    /**
     * RocketMQ用于注入MicrometerRocketMQMetricsCollector收集器的BeanPostProcessor
     * @param context
     * @return
     */
    @Bean
    public RocketMQMetricsPostProcessor rocketMQMetricsPostProcessor(ApplicationContext context){
        return new RocketMQMetricsPostProcessor(context);
    }

    @Bean
    public RocketMQSendMetrics rocketMQSendMetrics(){
        RocketMQSendMetrics sendMetrics = new RocketMQSendMetrics();
        return sendMetrics;
    }
    @Bean
    public RocketMQConsumeMetrics rocketMQConsumeMetrics(){
        RocketMQConsumeMetrics consumeMetrics = new RocketMQConsumeMetrics();
        return consumeMetrics;
    }
    @Bean
    public MicrometerRocketMQMetricsCollector micrometerRocketMQMetricsCollector(){
        MicrometerRocketMQMetricsCollector metricsCollector = new MicrometerRocketMQMetricsCollector(rocketMQSendMetrics(),rocketMQConsumeMetrics());
        return metricsCollector;
    }

}
