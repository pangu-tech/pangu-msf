package com.fintech.pangu.autoconfigure.actuate.metrics;

import com.fintech.pangu.actuate.metrics.aop.GaugeAspect;
import io.micrometer.core.aop.CountedAspect;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
import org.springframework.boot.actuate.autoconfigure.metrics.export.simple.SimpleMetricsExportAutoConfiguration;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter({ MetricsAutoConfiguration.class, SimpleMetricsExportAutoConfiguration.class })  //添加后可以使用@ConditionalOnBean({ MeterRegistry.class })
@ConditionalOnClass({ MeterRegistry.class })
@ConditionalOnBean({ MeterRegistry.class })
public class PanGuMetricsAutoConfiguration {

    /**
     * 设置common tags
     * @return
     */
    @Bean
    public MeterRegistryCustomizer commonTagsMeterRegistryCustomizer(){
        return new CommonTagsMeterRegistryCustomizer();
    }

    /**
     * Counted启动
     * @param meterRegistry
     * @return
     */
    @Bean
    public CountedAspect countedAspect(MeterRegistry meterRegistry){
        return new CountedAspect(meterRegistry);
    }

    /**
     * Gauge启动
     * @return
     */
    @Bean
    public GaugeAspect gaugeAspect(){
        return new GaugeAspect();
    }

}
