package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;

@Data
public class OAuth2SsoClientProperties {

    public static final String DEFAULT_LOGIN_PATH = "/login";

    /**
     * SSO Client 登录path
     */
    private String loginPath = DEFAULT_LOGIN_PATH;

    /**
     * 授权服务器访问地址
     */
    private String authServerUri;


    /**
     * 是否开启用户单点登录限制，默认false
     */
    private boolean signLoginEnabled = false;

}
