package com.fintech.pangu.autoconfigure.security.authentication.token;

import com.fintech.pangu.autoconfigure.security.properties.PanGuSecurityProperties;
import com.fintech.pangu.security.authentication.token.AuthenticationTokenResponder;
import com.fintech.pangu.security.authentication.token.AuthenticationTokenServices;
import com.fintech.pangu.security.authentication.token.AuthenticationTokenStore;
import com.fintech.pangu.security.authentication.token.impl.DefaultAuthenticationTokenResponder;
import com.fintech.pangu.security.authentication.token.impl.DefaultAuthenticationTokenServices;
import com.fintech.pangu.security.authentication.token.impl.InMemoryAuthenticationTokenStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.Assert;

/**
 * 认证令牌的默认自动配置
 */
@Configuration
@EnableConfigurationProperties(PanGuSecurityProperties.class)
@AutoConfigureAfter({ JwtAuthenticationTokenAutoConfiguration.class })  // 在所有其它AuthenticationTokenAutoConfiguration之后配置
// TODO 暂时没加什么时候开启认证令牌自动配置的条件
//@ConditionalOnProperty(value = "pangu.security.authentication.token.enable", havingValue = "true", matchIfMissing = false)
@ConditionalOnBean(AuthenticationTokenMarkerConfiguration.Marker.class)  // Marker被使用者标记
public class DefaultAuthenticationTokenAutoConfiguration {

    /**
     * PanGu安全相关所有配置
     */
    @Autowired
    private PanGuSecurityProperties panGuSecurityProperties;


    /**
     * 默认AuthenticationTokenStore - 基于内存的令牌存储
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(AuthenticationTokenStore.class)
    public AuthenticationTokenStore inMemoryAuthenticationTokenStore(){
        return new InMemoryAuthenticationTokenStore();
    }


    /**
     * 默认认证令牌服务 - 默认生成UUID类型令牌
     * @param tokenStore
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(AuthenticationTokenServices.class)
    public AuthenticationTokenServices defaultAuthenticationTokenServices(AuthenticationTokenStore tokenStore){
        DefaultAuthenticationTokenServices defaultAuthenticationTokenServices = new DefaultAuthenticationTokenServices();
        defaultAuthenticationTokenServices.setTokenStore(tokenStore);

        return defaultAuthenticationTokenServices;
    }


    /**
     * 默认认证令牌响应器
     * 无特殊需求可满足使用
     * @return
     */
    @Bean
    @ConditionalOnMissingBean(AuthenticationTokenResponder.class)
    public AuthenticationTokenResponder defaultAuthenticationTokenResponder(){
        DefaultAuthenticationTokenResponder defaultAuthenticationTokenResponder = new DefaultAuthenticationTokenResponder();

        defaultAuthenticationTokenResponder.setTokenReturnType(panGuSecurityProperties.getAuthentication().getToken().getResponse().getReturnType());

        // 如果以cookie返回token
        if("cookie".equals(defaultAuthenticationTokenResponder.getTokenReturnType())){
            Assert.hasText(panGuSecurityProperties.getAuthentication().getToken().getResponse().getCookieDomain(), "authentication token response by cookie, domain can not be empty!");

            defaultAuthenticationTokenResponder.setDomain(panGuSecurityProperties.getAuthentication().getToken().getResponse().getCookieDomain());
            defaultAuthenticationTokenResponder.setPath(panGuSecurityProperties.getAuthentication().getToken().getResponse().getCookiePath());
            defaultAuthenticationTokenResponder.setMaxAge(panGuSecurityProperties.getAuthentication().getToken().getResponse().getCookieMaxAgeInSeconds());
            defaultAuthenticationTokenResponder.setSecure(panGuSecurityProperties.getAuthentication().getToken().getResponse().isSecure());
        }

        return defaultAuthenticationTokenResponder;
    }

}
