package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;

/**
 * CSRF配置文件
 */
@Data
public class CsrfProperties {

    /**
     * 启用csrf，默认true
     */
    private boolean enable = true;

}
