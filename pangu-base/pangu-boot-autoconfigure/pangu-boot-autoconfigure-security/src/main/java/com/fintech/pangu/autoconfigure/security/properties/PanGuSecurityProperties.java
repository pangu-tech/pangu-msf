package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@Data
@ConfigurationProperties(prefix = "pangu.security")
public class PanGuSecurityProperties {

    /**
     * webSecurity配置
     */
    @NestedConfigurationProperty
    private WebSecurityProperties web = new WebSecurityProperties();

    /**
     * httpSecurity配置
     */
    @NestedConfigurationProperty
    private HttpSecurityProperties http = new HttpSecurityProperties();

    /**
     * 认证配置
     */
    @NestedConfigurationProperty
    private SecurityAuthenticationProperties authentication = new SecurityAuthenticationProperties();

    /**
     * 权限配置
     */
    @NestedConfigurationProperty
    private SecurityAuthorizeProperties authorize = new SecurityAuthorizeProperties();


    /**
     * oauth2配置
     */
    @NestedConfigurationProperty
    private OAuth2Properties oauth2 = new OAuth2Properties();

}
