package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;

@Data
public class FrameOptionsProperties {

    /**
     * 开启headers frameOptions，默认true
     * 会在Response中写入X-Frame-Options:DENY
     */
    private boolean enable = true;

}
