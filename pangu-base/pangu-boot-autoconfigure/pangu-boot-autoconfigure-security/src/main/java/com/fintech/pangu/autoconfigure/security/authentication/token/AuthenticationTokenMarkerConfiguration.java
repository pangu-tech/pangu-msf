package com.fintech.pangu.autoconfigure.security.authentication.token;

import org.springframework.context.annotation.Configuration;

/**
 * 用于标记需要AuthenticationToken相关自动配置
 * 用以激活如DefaultAuthenticationTokenAutoConfiguration等
 * 在需要AuthenticationToken配置的自动配置类上需要通过这个Marker开启AuthenticationToken的自动配置
 */
@Configuration
public class AuthenticationTokenMarkerConfiguration {

    /**
     * 转移到引入AuthenticationToken配置的地方注入标记到Spring
     */
    //@Bean
    //public Marker authenticationTokenMarkerBean() {
    //    return new Marker();
    //}

    public static class Marker {
    }

}
