package com.fintech.pangu.autoconfigure.security.authorize;

import com.fintech.pangu.autoconfigure.security.properties.PanGuSecurityProperties;
import com.fintech.pangu.security.authorize.impl.ConfigableAuthorizeConfigProvider;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * 可配置的授权配置提供者，通过springboot的Properties类获取配置
 * 由自动配置类注入Spring容器
 */
public class AutoConfigableAuthorizeConfigProvider extends ConfigableAuthorizeConfigProvider {

    @Autowired
    public PanGuSecurityProperties panGuSecurityProperties;

    /**
     * 实现从Properties获取配置
     * @return
     */
    @Override
    protected List<String> permitAllUrls() {
        return panGuSecurityProperties.getHttp().getAuthorizeRequests().getPermitAllUrls();
    }


    /**
     * 实现从Properties获取默认permitAllUrls配置
     * @return
     */
    @Override
    protected List<String> defaultPermitAllUrls() {
        return panGuSecurityProperties.getHttp().getAuthorizeRequests().getDefaultPermitAllUrls();
    }

}
