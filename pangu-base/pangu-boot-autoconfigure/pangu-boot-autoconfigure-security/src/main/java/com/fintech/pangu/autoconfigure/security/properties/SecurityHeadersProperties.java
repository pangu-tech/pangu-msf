package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@Data
public class SecurityHeadersProperties {

    /**
     * frameOptions配置
     */
    @NestedConfigurationProperty
    private FrameOptionsProperties frameOptions = new FrameOptionsProperties();

}
