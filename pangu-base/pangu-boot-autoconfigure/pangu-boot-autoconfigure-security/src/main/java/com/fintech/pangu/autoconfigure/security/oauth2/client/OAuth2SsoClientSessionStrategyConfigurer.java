package com.fintech.pangu.autoconfigure.security.oauth2.client;

import com.fintech.pangu.autoconfigure.security.properties.PanGuSecurityProperties;
import com.fintech.pangu.security.oauth2.sso.client.session.CustomSessionInformationExpiredStrategy;
import lombok.Setter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;

/**
 * OAuth2 Client SSO Session策略子配置
 */
@Setter
public class OAuth2SsoClientSessionStrategyConfigurer {

    /**
     * PanGu安全相关的所有配置
     */
    private PanGuSecurityProperties panGuSecurityProperties;

    public OAuth2SsoClientSessionStrategyConfigurer(PanGuSecurityProperties panGuSecurityProperties) {
        this.panGuSecurityProperties = panGuSecurityProperties;
    }

    /**
     * OAuth2 SSO子配置入口
     * @param http
     * @throws Exception
     */
    public void configure(HttpSecurity http) throws Exception {
        // 判断是否开启单点登录限制
        if (panGuSecurityProperties.getOauth2().getSsoClient().isSignLoginEnabled()){
            http.sessionManagement()
                    .maximumSessions(1)
                    .expiredSessionStrategy(customSessionInformationExpiredStrategy()); // session过期策略
        }
    }

    /**
     * 自定义Session过期策略
     */
    public CustomSessionInformationExpiredStrategy customSessionInformationExpiredStrategy(){
        CustomSessionInformationExpiredStrategy customSessionInformationExpiredStrategy = new CustomSessionInformationExpiredStrategy();
        customSessionInformationExpiredStrategy.setAuthServerLogoutUri(panGuSecurityProperties.getOauth2().getSsoClient().getAuthServerUri());
        return customSessionInformationExpiredStrategy;
    }

}
