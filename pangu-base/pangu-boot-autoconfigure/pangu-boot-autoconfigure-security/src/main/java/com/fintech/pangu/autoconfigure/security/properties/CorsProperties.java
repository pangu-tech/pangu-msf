package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;

import java.util.List;

/**
 * CORS配置文件
 */
@Data
public class CorsProperties {

    //private final static List<String> CORS_ALLOWED_HEADERS = Arrays.asList(CorsConfiguration.ALL);

    //private final static List<String> CORS_ALLOWED_METHODS = Arrays.asList(CorsConfiguration.ALL);

    //private final static List<String> CORS_ALLOWED_ORIGINS = Arrays.asList(CorsConfiguration.ALL);


    private String path;

    private boolean allowCredentials;

    private List<String> allowedHeaders;

    private List<String> allowedMethods;

    private List<String> allowedOrigins;


    /**
     * 无参构造
     * 没有默认值，path有值代表开启cors
     */
    public CorsProperties() {
        //this.path = "/**";
        //this.allowCredentials = true;
        //this.allowedHeaders = CORS_ALLOWED_HEADERS;
        //this.allowedMethods = CORS_ALLOWED_METHODS;
        //this.allowedOrigins = CORS_ALLOWED_ORIGINS;
    }

    //public String getPath() {
    //    return path;
    //}
    //
    //public void setPath(String path) {
    //    this.path = path;
    //}
    //
    //public boolean isAllowCredentials() {
    //    return StringUtils.hasText(getPath()) ? allowCredentials : false;
    //}
    //
    //public void setAllowCredentials(boolean allowCredentials) {
    //    this.allowCredentials = allowCredentials;
    //}
    //
    //public List<String> getAllowedHeaders() {
    //    return StringUtils.hasText(getPath()) ? ((allowedHeaders!=null && allowedHeaders.size()>0) ? allowedHeaders : CORS_ALLOWED_METHODS) : null;
    //}
    //
    //public void setAllowedHeaders(List<String> allowedHeaders) {
    //    this.allowedHeaders = allowedHeaders;
    //}
    //
    //public List<String> getAllowedMethods() {
    //    return StringUtils.hasText(getPath()) ? ((allowedMethods!=null && allowedMethods.size()>0) ? allowedMethods : CORS_ALLOWED_HEADERS) : null;
    //}
    //
    //public void setAllowedMethods(List<String> allowedMethods) {
    //    this.allowedMethods = allowedMethods;
    //}
    //
    //public List<String> getAllowedOrigins() {
    //    return StringUtils.hasText(getPath()) ? ((allowedOrigins!=null && allowedOrigins.size()>0) ? allowedOrigins : CORS_ALLOWED_ORIGINS) : null;
    //}
    //
    //public void setAllowedOrigins(List<String> allowedOrigins) {
    //    this.allowedOrigins = allowedOrigins;
    //}

}
