package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;

/**
 * webSecurity配置文件
 */
@Data
public class WebSecurityProperties {

    public static final String[] DEFAULT_IGNORING_URLS = new String[]{
            /** 静态资源 */
            "/**/js/**",
            "/**/css/**",
            "/**/img/**",
            "/**/doc.html",
            "/**/favicon.ico",
            "/META-INF/resources/webjars/**",
            "/**/webjars/**",

            /** swagger */
            "/**/v2/api-docs",
            "/**/api-docs-ext",
            "/**/swagger-ui.html",
            "/**/swagger-resources/**",

            /** actuator */
            "/**/actuator/**"
    };


    /**
     * 排除路径列表，不经过security filter
     */
    private String[] ignoringUrls;


    /**
     * 默认排除路径列表，已包含swagger和actuator路径，不满足可覆盖
     */
    private String[] defaultIgnoringUrls = DEFAULT_IGNORING_URLS;


}
