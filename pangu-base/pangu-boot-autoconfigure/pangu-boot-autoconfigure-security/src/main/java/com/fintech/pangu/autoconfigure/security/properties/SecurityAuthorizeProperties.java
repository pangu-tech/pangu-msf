package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@Data
public class SecurityAuthorizeProperties {

    @NestedConfigurationProperty
    private CacheSync cacheSync = new CacheSync();

    @Data
    public static class CacheSync {
        /**
         * 同步启动延时(ms)，默认10s
         */
        private int initialDelayMs = 10 * 1000;

        /**
         * 同步执行间隔(ms)，默认30s
         */
        private int intervalMs = 30 * 1000;
    }

}
