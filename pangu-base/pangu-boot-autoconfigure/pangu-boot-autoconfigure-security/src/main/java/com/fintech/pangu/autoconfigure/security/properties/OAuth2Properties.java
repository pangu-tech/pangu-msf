package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * OAuth2相关配置
 */
@Data
public class OAuth2Properties {

    /**
     * OAuth2 sso client配置
     */
    @NestedConfigurationProperty
    private OAuth2SsoClientProperties ssoClient = new OAuth2SsoClientProperties();

}
