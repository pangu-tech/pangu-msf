package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

@Data
public class HttpSecurityProperties {

    /**
     * csrf配置
     */
    @NestedConfigurationProperty
    private CsrfProperties csrf = new CsrfProperties();


    /**
     * cors配置
     */
    @NestedConfigurationProperty
    private CorsProperties cors = new CorsProperties();

    /**
     * headers配置，如frameOptions
     */
    @NestedConfigurationProperty
    private SecurityHeadersProperties headers = new SecurityHeadersProperties();


    /**
     * AuthorizeRequests授权请求配置
     */
    @NestedConfigurationProperty
    private AuthorizeRequestsProperties authorizeRequests = new AuthorizeRequestsProperties();

}
