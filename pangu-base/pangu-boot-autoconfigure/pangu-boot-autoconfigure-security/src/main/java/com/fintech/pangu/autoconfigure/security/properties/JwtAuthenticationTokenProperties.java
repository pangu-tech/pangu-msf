package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;

/**
 * JWT认证令牌配置
 */
@Data
public class JwtAuthenticationTokenProperties {

    /**
     * jwt密匙
     */
    private String secret;

    /**
     * jwt过期时间(s)，默认30分钟
     */
    private int expireInSeconds = 30 * 60;


    /**
     * 是否开启jwt认证令牌校验恢复登录状态
     */
    private boolean checkEnable = false;


    /**
     * 是否开启jwt黑名单校验
     */
    private boolean blackListEnable = false;

}
