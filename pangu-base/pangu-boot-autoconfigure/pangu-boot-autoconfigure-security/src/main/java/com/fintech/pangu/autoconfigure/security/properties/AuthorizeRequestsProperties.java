package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public class AuthorizeRequestsProperties {

    public static List<String> DEFAULT_PERMIT_ALL_URLS = Arrays.asList(
            "/**/error"
    );

    /**
     * permitAll路径
     */
    private List<String> permitAllUrls;


    /**
     * 默认permitAll路径
     */
    private List<String> defaultPermitAllUrls = DEFAULT_PERMIT_ALL_URLS;

}
