package com.fintech.pangu.autoconfigure.security.oauth2.client;


import com.fintech.pangu.security.oauth2.sso.client.zuul.JWTTransferZuulFilter;
import com.netflix.zuul.ZuulFilter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 在zuul网关场景下使用
 */
@Configuration
@ConditionalOnWebApplication
@ConditionalOnClass({ZuulFilter.class, EnablePanGuOAuth2SsoClient.class})
public class OAuth2SsoClientZuulProxyAutoConfiguration {

    /**
     * 从session中获取用户信息JWT，放到转发后的请求头中的Filter
     * @return
     */
    @Bean
    public JWTTransferZuulFilter jwtTransferFilter() {
        return new JWTTransferZuulFilter();
    }

}
