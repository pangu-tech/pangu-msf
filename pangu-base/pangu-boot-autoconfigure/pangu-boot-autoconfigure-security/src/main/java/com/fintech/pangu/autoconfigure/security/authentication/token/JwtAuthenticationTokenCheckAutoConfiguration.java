package com.fintech.pangu.autoconfigure.security.authentication.token;

import com.fintech.pangu.autoconfigure.security.properties.PanGuSecurityProperties;
import com.fintech.pangu.security.jwt.client.authentication.config.JwtAuthecticationTokenPanGuHttpSecurityConfig;
import com.fintech.pangu.security.jwt.client.authentication.config.JwtBlackListPanGuHttpSecurityConfig;
import com.fintech.pangu.security.jwt.client.authentication.endpoint.JwtUserInfoEndpoint;
import com.fintech.pangu.security.jwt.client.authentication.filter.JwtAuthenticationFilter;
import com.fintech.pangu.security.jwt.client.authentication.filter.JwtBlackListFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 客户端添加JWT认证令牌校验的自动配置
 * 添加JwtAuthenticationFilter用于使用jwt认证令牌恢复登录状态
 */
@Configuration
@EnableConfigurationProperties(PanGuSecurityProperties.class)
@ConditionalOnClass(JwtAuthenticationFilter.class)
@ConditionalOnProperty(value = "pangu.security.authentication.token.jwt.check-enable", havingValue = "true", matchIfMissing = false)
public class JwtAuthenticationTokenCheckAutoConfiguration {

    /**
     * PanGu安全相关所有配置
     */
    @Autowired
    private PanGuSecurityProperties panGuSecurityProperties;


    /**
     * 解析JWT，还原Authentication的Filter
     * @return
     */
    @Bean
    public JwtAuthenticationFilter jwtAuthenticationFilter() {
        JwtAuthenticationFilter jwtAuthenticationFilter = new JwtAuthenticationFilter();
        jwtAuthenticationFilter.setSecret(panGuSecurityProperties.getAuthentication().getToken().getJwt().getSecret());
        jwtAuthenticationFilter.setTokenReturnType(panGuSecurityProperties.getAuthentication().getToken().getResponse().getReturnType());

        return jwtAuthenticationFilter;
    }

    /**
     * JwtAuthenticationFilter子配置
     * 向Spring Security过滤器链注入JwtAuthenticationFilter
     * @return
     */
    @Bean
    public JwtAuthecticationTokenPanGuHttpSecurityConfig jwtAuthecticationTokenPanGuHttpSecurityConfig(){
        JwtAuthecticationTokenPanGuHttpSecurityConfig config = new JwtAuthecticationTokenPanGuHttpSecurityConfig();
        config.setJwtAuthenticationFilter(jwtAuthenticationFilter());
        return config;
    }


    /**
     * JWT黑名单Filter
     * @return
     */
    @Bean
    @ConditionalOnProperty(value = "pangu.security.authentication.token.jwt.black-list-enable", havingValue = "true", matchIfMissing = false)
    public JwtBlackListFilter jwtBlackListFilter() {
        JwtBlackListFilter jwtBlackListFilter = new JwtBlackListFilter();

        return jwtBlackListFilter;
    }

    /**
     * JwtBlackListFilter子配置
     * 向Spring Security过滤器链注入JwtBlackListFilter
     * @return
     */
    @Bean
    @ConditionalOnProperty(value = "pangu.security.authentication.token.jwt.black-list-enable", havingValue = "true", matchIfMissing = false)
    public JwtBlackListPanGuHttpSecurityConfig jwtBlackListPanGuHttpSecurityConfig() {
        JwtBlackListPanGuHttpSecurityConfig config = new JwtBlackListPanGuHttpSecurityConfig();
        config.setJwtBlackListFilter(jwtBlackListFilter());
        return config;
    }

    /**
     * 通过JWT获取用户信息端点
     * @return
     */
    @Bean
    public JwtUserInfoEndpoint jwtUserInfoEndpoint(){
        return new JwtUserInfoEndpoint();
    }

}
