package com.fintech.pangu.autoconfigure.security;

import com.fintech.pangu.autoconfigure.security.authorize.AutoConfigableAuthorizeConfigProvider;
import com.fintech.pangu.autoconfigure.security.properties.PanGuSecurityProperties;
import com.fintech.pangu.security.authorize.AuthorizeConfigManager;
import com.fintech.pangu.security.authorize.AuthorizeConfigProvider;
import com.fintech.pangu.security.authorize.impl.DefaultAuthorizeConfigManager;
import com.fintech.pangu.security.config.PanGuAuthenticationManagerBuilderConfigurer;
import com.fintech.pangu.security.config.PanGuAuthenticationManagerBuilderManager;
import com.fintech.pangu.security.config.PanGuHttpSecurityConfigurer;
import com.fintech.pangu.security.config.PanGuHttpSecurityManager;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;

/**
 * 注入一些基础安全组件
 * 不将其放在PanGuWebSecurityConfigurationAdapter及其父类中，以免因为顺序及@Conditional问题导致找不到BeanDefinition
 */
@Configuration
@EnableConfigurationProperties(PanGuSecurityProperties.class)
public class PanGuWebSecurityAutoConfiguration {


    @Bean
    @ConditionalOnMissingBean(PasswordEncoder.class)
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    /**
     * 权限配置管理器
     */
    @Bean
    public AuthorizeConfigManager authorizeConfigManager(ObjectProvider<List<AuthorizeConfigProvider>> authorizeConfigProviders){
        DefaultAuthorizeConfigManager defaultAuthorizeConfigManager = new DefaultAuthorizeConfigManager();
        defaultAuthorizeConfigManager.setAuthorizeConfigProviders(authorizeConfigProviders.getIfAvailable());
        return defaultAuthorizeConfigManager;
    }


    /**
     * PanGuHttpSecurityConfigurer配置管理器
     */
    @Bean
    public PanGuHttpSecurityManager panGuHttpSecurityManager(ObjectProvider<List<PanGuHttpSecurityConfigurer>> panGuHttpSecurityConfigurers){
        PanGuHttpSecurityManager panGuHttpSecurityManager = new PanGuHttpSecurityManager();
        panGuHttpSecurityManager.setPanGuHttpSecurityConfigurers(panGuHttpSecurityConfigurers.getIfAvailable());
        return panGuHttpSecurityManager;
    }

    /**
     * PanGuAuthenticationManagerBuilderConfigurer配置管理器
     */
    @Bean
    public PanGuAuthenticationManagerBuilderManager panGuAuthenticationManagerBuilderManager(ObjectProvider<List<PanGuAuthenticationManagerBuilderConfigurer>> panGuAuthenticationManagerBuilderConfigurers) {
        PanGuAuthenticationManagerBuilderManager panGuAuthenticationManagerBuilderManager = new PanGuAuthenticationManagerBuilderManager();
        panGuAuthenticationManagerBuilderManager.setPanGuAuthenticationManagerBuilderConfigurers(panGuAuthenticationManagerBuilderConfigurers.getIfAvailable());
        return panGuAuthenticationManagerBuilderManager;
    }


    /**
     * 可配置的权限配置提供者
     * @return
     */
    @Bean
    public AuthorizeConfigProvider autoConfigableAuthorizeConfigProvider(){
        return new AutoConfigableAuthorizeConfigProvider();
    }

}
