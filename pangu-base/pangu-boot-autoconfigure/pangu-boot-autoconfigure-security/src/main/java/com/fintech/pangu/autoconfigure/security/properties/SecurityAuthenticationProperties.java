package com.fintech.pangu.autoconfigure.security.properties;

import com.fintech.pangu.security.authentication.model.SecurityUser;
import lombok.Data;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

import java.util.List;

/**
 * 认证配置
 */
@Data
public class SecurityAuthenticationProperties {

    /**
     * 是否开启api login
     */
    private boolean apiLoginEnable = false;

    /**
     * 是否启用默认用户，默认true
     */
    private boolean defaultUsersEnable = true;

    /**
     * 配置内存方式的用户列表
     */
    private List<SecurityUser> users;


    /**
     * 认证令牌
     */
    @NestedConfigurationProperty
    private AuthenticationTokenProperties token = new AuthenticationTokenProperties();

}
