package com.fintech.pangu.autoconfigure.security.properties;

import lombok.Data;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * 认证令牌配置
 */
@Data
public class AuthenticationTokenProperties {

    /**
     * 令牌类型，默认uuid
     * （弃用） 通过引入的组件判断认证令牌类型
     */
    //private String type = "uuid";

    /**
     * JWT认证令牌
     */
    @NestedConfigurationProperty
    private JwtAuthenticationTokenProperties jwt = new JwtAuthenticationTokenProperties();

    /**
     * token响应
     */
    @NestedConfigurationProperty
    private TokenResponse response = new TokenResponse();


    @Data
    public static class TokenResponse {
        /**
         * token返回类型(cookie、response)，默认cookie
         */
        private String returnType = "cookie";

        /**
         * cookie domain
         */
        private String cookieDomain;

        /**
         * cookie path，默认/
         */
        private String cookiePath = "/";

        /**
         * cookie有效期，默认8小时
         */
        private int cookieMaxAgeInSeconds = 8 * 60 * 60;

        private boolean secure = false;
    }

}
