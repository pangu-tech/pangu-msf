package com.fintech.pangu.autoconfigure.web.properties;

import lombok.Data;

import java.util.List;

/**
 * @Description:
 */
@Data
public class PanGuHttpCorsProperties {

    private boolean enabled = false;

    private String path;

    private boolean allowCredentials;

    private List<String> allowedHeaders;

    private List<String> allowedMethods;

    private List<String> allowedOrigins;
}
