package com.fintech.pangu.autoconfigure.web.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @Description:
 */
@Data
@ConfigurationProperties(prefix = "pangu.http")
public class PanGuHttpProperties {

    @NestedConfigurationProperty
    private PanGuHttpConvertProperties converter = new PanGuHttpConvertProperties();

    @NestedConfigurationProperty
    private PanGuHttpCorsProperties cors = new PanGuHttpCorsProperties();

}
