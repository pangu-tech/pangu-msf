package com.fintech.pangu.autoconfigure.web.datasource.druid;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @Description:
 */
@Data
@ConfigurationProperties(prefix = "spring.datasource.druid")
public class DruidProperties {

    @NestedConfigurationProperty
    private LoginInfo login = new LoginInfo();

    @NestedConfigurationProperty
    private DataSource dataSource = new DataSource();
}

@Data
class LoginInfo{

    public LoginInfo(){
        this.enable = true;
        this.username = "druid";
        this.password  = "druid";
    }

    private boolean enable;

    private String username ;

    private String password;
}

@Data
class DataSource{
    public DataSource(){
        this.initialSize = 20;
        this.minIdle = 30;
        maxActive = 100;
        maxWait = 60000;
        timeBetweenEvictionRunsMillis = 60000;
        minEvictableIdleTimeMillis = 300000;
        validationQuery = "SELECT 'x'";
        testWhileIdle = true;
        testOnBorrow = true;
        testOnReturn = true;
        poolPreparedStatements = true;
        maxPoolPreparedStatementPerConnectionSize = 20;
        filters = "stat";
        connectionProperties = "druid.stat.slowSqlMillis = 5000";
    }

    private int initialSize;

    private int minIdle;

    private int maxActive;

    private int maxWait;

    private int timeBetweenEvictionRunsMillis;

    private int minEvictableIdleTimeMillis;

    private String validationQuery;

    private boolean testWhileIdle;

    private boolean testOnBorrow;

    private boolean testOnReturn;

    private boolean poolPreparedStatements;

    private int maxPoolPreparedStatementPerConnectionSize;

    private String filters;

    private String connectionProperties;
}
