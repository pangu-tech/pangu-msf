package com.fintech.pangu.autoconfigure.web.swagger;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Description:
 */
@Data
@ConfigurationProperties(prefix = "pangu.swagger2")
public class Swagger2Properties {

    private String title;

    private String description;

    private String termsOfServiceUrl;

    private String selectBasePackage;

    private boolean enabled;

    public Swagger2Properties() {
        this.title = "标题：XXXX系统_接口文档";
        this.description = "描述：XXXX";
        this.termsOfServiceUrl = "http://localhost:8080/";
        this.selectBasePackage = "com.fintech";
        this.enabled = false;
    }
}
