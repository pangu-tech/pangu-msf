package com.fintech.pangu.autoconfigure.web;

import com.fintech.pangu.autoconfigure.web.properties.PanGuHttpProperties;
import com.fintech.pangu.web.config.PanGuWebMvcConfigurer;
import com.fintech.pangu.web.config.PanGuWebMvcManager;
import com.fintech.pangu.web.config.SwaggerResourceHandler;
import com.fintech.pangu.web.config.UserInfoJWTHeaderInterceptor;
import com.fintech.pangu.web.exception.handler.GlobalExceptionHandler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * @Description:
 */
@Configuration
@EnableConfigurationProperties(value = {PanGuHttpProperties.class})
public class WebAutoConfiguration {

    private PanGuHttpProperties panGuHttpProperties;

    public WebAutoConfiguration(PanGuHttpProperties panGuHttpProperties){
        this.panGuHttpProperties = panGuHttpProperties;
    }

    @Bean
    public PanGuWebMvcConfigurer panGuWebMvcConfigurer(){
        return new PanGuWebMvcConfigurer(panGuWebMvcManager(),panGuHttpProperties.getConverter().getFastJson());
    }

    @Bean
    @Primary
    public GlobalExceptionHandler globalExceptionHandler(){
        return new GlobalExceptionHandler();
    }

    /**
     * 默认的manager处理器
     * @return
     */
    @Bean
    public PanGuWebMvcManager panGuWebMvcManager(){
        return new PanGuWebMvcManager();
    }

    /**
     * 基于jwt传递的用户信息获取的interceptor
     * @return
     */
    @Bean
    public UserInfoJWTHeaderInterceptor userInfoJWTHeaderInterceptor(){
        return new UserInfoJWTHeaderInterceptor();
    }

    @Bean
    public SwaggerResourceHandler swaggerResourceHandler(){
        return new SwaggerResourceHandler();
    }

    @Bean
    @ConditionalOnProperty(value = "pangu.http.cors.enabled",havingValue = "true")
    public FilterRegistrationBean corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration corsConfiguration = new CorsConfiguration();
        corsConfiguration.setAllowCredentials(panGuHttpProperties.getCors().isAllowCredentials());
        corsConfiguration.setAllowedOrigins(panGuHttpProperties.getCors().getAllowedOrigins());
        corsConfiguration.setAllowedHeaders(panGuHttpProperties.getCors().getAllowedHeaders());
        corsConfiguration.setAllowedMethods(panGuHttpProperties.getCors().getAllowedMethods());
        source.registerCorsConfiguration(panGuHttpProperties.getCors().getPath(), corsConfiguration);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
        return bean;
    }
}
