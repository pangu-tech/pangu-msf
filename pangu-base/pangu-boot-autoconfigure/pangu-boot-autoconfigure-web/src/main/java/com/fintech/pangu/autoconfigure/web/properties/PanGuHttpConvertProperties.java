package com.fintech.pangu.autoconfigure.web.properties;

import com.fintech.pangu.web.http.converter.FastJsonProperties;
import lombok.Data;
import org.springframework.boot.context.properties.NestedConfigurationProperty;

/**
 * @Description:
 */
@Data
public class PanGuHttpConvertProperties {

    @NestedConfigurationProperty
    private FastJsonProperties fastJson = new FastJsonProperties();

}
