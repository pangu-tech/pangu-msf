package com.fintech.pangu.autoconfigure.web.datasource.druid;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@Slf4j
@ConditionalOnClass(DruidDataSource.class)
@EnableConfigurationProperties({DruidProperties.class, DataSourceProperties.class})
public class DruidAutoConfiguration {

    @Autowired
    private DruidProperties druidProperties;

    @Autowired
    private DataSourceProperties dataSourceProperties;


    @Bean
    public ServletRegistrationBean druidServlet() {
        ServletRegistrationBean reg = new ServletRegistrationBean();
        reg.setServlet(new StatViewServlet());
        reg.addUrlMappings("/druid/*");

        //web访问是否需要登录
        if(druidProperties.getLogin().isEnable()) {
            reg.addInitParameter("loginUsername", druidProperties.getLogin().getUsername());
            reg.addInitParameter("loginPassword", druidProperties.getLogin().getPassword());
            reg.addInitParameter("resetEnable","false");// 是否能够重置数据
        }
        return reg;
    }

    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        filterRegistrationBean.setFilter(new WebStatFilter());
        filterRegistrationBean.addUrlPatterns("/*");
        filterRegistrationBean.addInitParameter("exclusions", "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*");
        filterRegistrationBean.addInitParameter("profileEnable", "true");
        filterRegistrationBean.addInitParameter("principalCookieName", "USER_COOKIE");
        filterRegistrationBean.addInitParameter("principalSessionName", "USER_SESSION");
        return filterRegistrationBean;
    }

    @Bean
    @Primary
    public DataSource druidDataSource() {
        log.info("开始配置druidDataSource");
        DruidDataSource datasource = new DruidDataSource();

        datasource.setUrl(dataSourceProperties.getUrl());
        datasource.setUsername(dataSourceProperties.getUsername());
        datasource.setPassword(dataSourceProperties.getPassword());
        datasource.setDriverClassName(dataSourceProperties.getDriverClassName());

        datasource.setInitialSize(druidProperties.getDataSource().getInitialSize());
        datasource.setMinIdle(druidProperties.getDataSource().getMinIdle());
        datasource.setMaxActive(druidProperties.getDataSource().getMaxActive());
        datasource.setMaxWait(druidProperties.getDataSource().getMaxWait());
        datasource.setTimeBetweenEvictionRunsMillis(druidProperties.getDataSource().getTimeBetweenEvictionRunsMillis());
        datasource.setMinEvictableIdleTimeMillis(druidProperties.getDataSource().getMinEvictableIdleTimeMillis());
        datasource.setValidationQuery(druidProperties.getDataSource().getValidationQuery());
        datasource.setTestWhileIdle(druidProperties.getDataSource().isTestWhileIdle());
        datasource.setTestOnBorrow(druidProperties.getDataSource().isTestOnBorrow());
        datasource.setTestOnReturn(druidProperties.getDataSource().isTestOnReturn());
        datasource.setPoolPreparedStatements(druidProperties.getDataSource().isPoolPreparedStatements());
        datasource.setConnectionProperties(druidProperties.getDataSource().getConnectionProperties());
        datasource.setMaxPoolPreparedStatementPerConnectionSize(druidProperties.getDataSource().getMaxPoolPreparedStatementPerConnectionSize());
        try {
            datasource.setFilters(druidProperties.getDataSource().getFilters());
        } catch (SQLException e) {
            log.error("druid configuration initialization filter", e);
        }
        log.info("druidDataSource配置成功");
        return datasource;
    }

}