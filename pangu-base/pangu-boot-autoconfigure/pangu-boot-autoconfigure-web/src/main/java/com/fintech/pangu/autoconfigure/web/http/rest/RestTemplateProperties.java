package com.fintech.pangu.autoconfigure.web.http.rest;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "pangu.rest-template.httpclient")
@Data
public class RestTemplateProperties {

    /**
     * request请求配置
     */
    private final RequestConfig requestConfig = new RequestConfig();

    /**
     * Http连接池配置
     */
    private final Pool pool = new Pool();


    /**
     * request请求配置
     */
    @Data
    public static class RequestConfig {
        /**
         * 连接超时（默认值 ：-1）
         */
        private Integer connectTimeout = -1;

        /**
         * 读超时（默认值 ：-1）
         */
        private Integer readTimeout = -1;

        /**
         * 连接不够用的等待时间（默认值 ：-1）
         */
        private Integer connectionRequestTimeout = -1;
    }


    /**
     * Http连接池配置
     */
    @Data
    public static class Pool {
        /**
         * 最大连接数（默认值 ：10）
         */
        private Integer maxTotal = 10;

        /**
         * 每个路由的最大连接数（默认值 ：5）
         */
        private Integer defaultMaxPerRoute = 5;
    }

}
