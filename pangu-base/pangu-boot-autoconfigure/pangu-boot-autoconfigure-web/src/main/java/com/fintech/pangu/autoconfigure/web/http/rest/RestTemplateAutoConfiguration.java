package com.fintech.pangu.autoconfigure.web.http.rest;

import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;


@Configuration
@EnableConfigurationProperties(RestTemplateProperties.class)
@ConditionalOnClass({RestTemplate.class, HttpClient.class})
@Slf4j
public class RestTemplateAutoConfiguration {

    @Autowired
    private RestTemplateProperties restTemplateProperties;

    @Bean
    @ConditionalOnMissingBean
    public RestTemplate restTemplate() {
        log.info("自动配置restTemplate开始");
        RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory());
        log.info("自动配置restTemplate结束");
        return restTemplate;
    }

    @Bean
    @ConditionalOnMissingBean
    public HttpComponentsClientHttpRequestFactory clientHttpRequestFactory() {
        try {
            HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();

            //开始设置连接池
            PoolingHttpClientConnectionManager poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager();
            poolingHttpClientConnectionManager.setMaxTotal(restTemplateProperties.getPool().getMaxTotal());  //最大连接数，默认10
            poolingHttpClientConnectionManager.setDefaultMaxPerRoute(restTemplateProperties.getPool().getDefaultMaxPerRoute());  //同路由并发数，默认5
            httpClientBuilder.setConnectionManager(poolingHttpClientConnectionManager);

            HttpClient httpClient = httpClientBuilder.build();
            // httpClient连接配置
            HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

            //连接超时
            if (restTemplateProperties.getRequestConfig().getConnectTimeout() > 0) {
                clientHttpRequestFactory.setConnectTimeout(restTemplateProperties.getRequestConfig().getConnectTimeout());
                log.info("配置restTemplate-connectTimeout: {}", restTemplateProperties.getRequestConfig().getConnectTimeout());
            }
            //数据读取超时时间
            if (restTemplateProperties.getRequestConfig().getReadTimeout() > 0) {
                clientHttpRequestFactory.setReadTimeout(restTemplateProperties.getRequestConfig().getReadTimeout());
                log.info("配置restTemplate-readTimeout: {}", restTemplateProperties.getRequestConfig().getReadTimeout());
            }
            //连接不够用的等待时间
            if (restTemplateProperties.getRequestConfig().getConnectionRequestTimeout() > 0) {
                clientHttpRequestFactory.setConnectionRequestTimeout(restTemplateProperties.getRequestConfig().getConnectionRequestTimeout());
                log.info("配置restTemplate-connectionRequestTimeout: {}", restTemplateProperties.getRequestConfig().getConnectionRequestTimeout());
            }

            return clientHttpRequestFactory;
        } catch (Exception e) {
            log.error("初始化clientHttpRequestFactory出错", e);
        }

        return null;
    }

}