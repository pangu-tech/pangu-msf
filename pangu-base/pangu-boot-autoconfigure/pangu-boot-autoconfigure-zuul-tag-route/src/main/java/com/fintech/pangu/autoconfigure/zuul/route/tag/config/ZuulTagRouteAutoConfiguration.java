package com.fintech.pangu.autoconfigure.zuul.route.tag.config;

import com.fintech.pangu.zuul.route.tag.filter.ExtractTagFilter;
import com.fintech.pangu.zuul.route.tag.filter.GrayPostZuulFilter;
import com.fintech.pangu.zuul.route.tag.filter.GrayPreZuulFilter;
import com.fintech.pangu.zuul.route.tag.ribbon.TagMetadataPredicate;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.filters.ZuulServletFilter;
import com.netflix.zuul.http.ZuulServlet;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass({ZuulServlet.class, ZuulServletFilter.class, TagMetadataPredicate.class})  // zuul环境 + tag-route引用 启用
@ConditionalOnProperty(prefix = "pangu.zuul.route.tag", name = "enabled", havingValue = "true", matchIfMissing = false) // 需要pangu.zuul.route.tag.enabled=true
@EnableConfigurationProperties(ZuulTagRouteProperties.class)
@RibbonClients( defaultConfiguration = { TagRouteDefaultRibbonClientsConfiguration.class } )  // 指定RibbonClients默认配置
public class ZuulTagRouteAutoConfiguration {

    /**
     * 提取请求头中的标签
     * @return
     */
    @Bean
    public ZuulFilter extractTagFilter(){
        return new ExtractTagFilter();
    }

    @Bean
    public ZuulFilter grayPreZuulFilter(){
        return new GrayPreZuulFilter();
    }

    @Bean
    public ZuulFilter grayPostZuulFilter(){
        return new GrayPostZuulFilter();
    }

}
