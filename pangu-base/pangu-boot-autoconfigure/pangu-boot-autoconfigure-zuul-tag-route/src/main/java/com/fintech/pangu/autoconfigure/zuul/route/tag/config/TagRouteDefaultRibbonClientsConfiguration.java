package com.fintech.pangu.autoconfigure.zuul.route.tag.config;

import com.fintech.pangu.zuul.route.tag.ribbon.TagMetadataRule;
import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.IRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.ribbon.PropertiesFactory;
import org.springframework.context.annotation.Bean;

// 使用基于tag路由的RibbonClients的默认配置，将默认的IRule实现类设置为TagMetadataRule
public class TagRouteDefaultRibbonClientsConfiguration {

    @Value("${ribbon.client.name}")
    private String name = "client";

    @Autowired
    private PropertiesFactory propertiesFactory;

    @Bean
    public IRule ribbonRule(IClientConfig config) {
        if (this.propertiesFactory.isSet(IRule.class, name)) {
            return this.propertiesFactory.get(IRule.class, config, name);
        }

        TagMetadataRule rule = new TagMetadataRule();

        // 默认配置
        rule.initWithNiwsConfig(config);

        return rule;
    }

}
