package com.fintech.pangu.autoconfigure.zuul.route.tag.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties( prefix = "pangu.zuul.route.tag" )
@Getter
@Setter
public class ZuulTagRouteProperties {

    /**
     * 是否启用zuul基于tag的路由
     */
    private boolean enabled = false;


}
