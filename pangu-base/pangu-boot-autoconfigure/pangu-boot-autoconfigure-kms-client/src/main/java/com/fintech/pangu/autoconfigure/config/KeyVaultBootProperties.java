package com.fintech.pangu.autoconfigure.config;

import com.fintech.pangu.keyvault.KeyVaultProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @Description:
 */
@ConfigurationProperties(prefix = "pangu.keyvault")
public class KeyVaultBootProperties extends KeyVaultProperties {


}
