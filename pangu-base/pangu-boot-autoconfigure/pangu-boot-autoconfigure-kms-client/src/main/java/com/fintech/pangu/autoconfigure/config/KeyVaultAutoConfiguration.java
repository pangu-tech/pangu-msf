package com.fintech.pangu.autoconfigure.config;


import com.fintech.pangu.keyvault.DefaultKeyVaultObtain;
import com.fintech.pangu.keyvault.KeyVaultObtain;
import com.fintech.pangu.keyvault.RemoteKeyVaultObtain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

/**
 * @Description:
 */

@Configuration
@ConditionalOnClass(KeyVaultObtain.class)
@EnableConfigurationProperties({KeyVaultBootProperties.class})
public class KeyVaultAutoConfiguration {

    @Autowired
    private KeyVaultBootProperties keyVaultBootProperties;


    @Bean
    @ConditionalOnMissingBean(KeyVaultObtain.class)
    public KeyVaultObtain defaultKeyVaultObtain(){
        if (StringUtils.hasText(keyVaultBootProperties.getServerUri())){
            return new RemoteKeyVaultObtain(keyVaultBootProperties);
        }
        return new DefaultKeyVaultObtain(keyVaultBootProperties);
    }
}
