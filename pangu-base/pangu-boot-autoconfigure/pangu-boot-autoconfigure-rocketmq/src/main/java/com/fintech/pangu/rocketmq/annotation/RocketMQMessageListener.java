package com.fintech.pangu.rocketmq.annotation;

import com.alibaba.rocketmq.common.consumer.ConsumeFromWhere;
import com.alibaba.rocketmq.common.protocol.heartbeat.MessageModel;
import com.fintech.pangu.rocketmq.enums.ConsumeMode;
import com.fintech.pangu.rocketmq.enums.SelectorType;
import org.springframework.stereotype.Service;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author dell
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Service
public @interface RocketMQMessageListener {

    /**
     * 消费组
     */
    String consumerGroup();

    /**
     * Topic
     */
    String topic();

    /**
     * 从哪儿开始消费，默认CONSUME_FROM_LAST_OFFSET
     */
    ConsumeFromWhere consumeFromWhere() default ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET;

    /**
     * 过滤消息类型，只有TAG方式，4.1.0-incubating开始支持SQL92
     */
    SelectorType selectorType() default SelectorType.TAG;

    /**
     * 过滤消息表达式，默认*
     */
    String selectorExpress() default "*";

    /**
     * 消费模式，默认是并发消费CONCURRENTLY
     */
    ConsumeMode consumeMode() default ConsumeMode.CONCURRENTLY;

    /**
     * 消息模式，默认集群模式，还有BROADCASTING广播模式
     */
    MessageModel messageModel() default MessageModel.CLUSTERING;

    /**
     * 最小消费线程数，默认20
     */
    String consumeThreadMin() default "20";

    /**
     * 最大消费线程数，默认64
     */
    String consumeThreadMax() default "64";

    /**
     * 最大批量消费大小，默认1
     */
    String consumeMessageBatchMaxSize() default "1";

    /**
     * 最大重复消费次数，默认3
     */
    String maxReconsumeTime() default "3";

    /**
     * nameserver地址，默认取公共的配置，允许自定义
     */
    String nameServer() default "";

    /**
     * 是否加载当前消息监听，默认是
     */
    boolean enable() default true;
}
