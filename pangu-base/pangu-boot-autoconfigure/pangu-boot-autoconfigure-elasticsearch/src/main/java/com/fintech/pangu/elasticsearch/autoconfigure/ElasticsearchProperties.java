package com.fintech.pangu.elasticsearch.autoconfigure;

import com.google.common.collect.Maps;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * @author xujunqi
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = "pangu.elasticsearch.client")
public class ElasticsearchProperties {
    /**
     * transport连接地址
     * 格式: IP:PORT,IP:PORT...
     */
    private String[] hosts;

    /**
     * settings属性列表
     * 属性名都以elasticsearch.client.settings[cluster.name]=ES_UAT_cluster配置
     */
    private Map<String, Object> settings = Maps.newLinkedHashMap();

    public String[] getHosts() {
        if (hosts == null) {
            return null;
        }
        return hosts.clone();
    }

    public void setHosts(String[] hosts) {
        if (hosts == null) {
            this.hosts = null;
        } else {
            this.hosts = hosts.clone();
        }
    }

    public Map<String, Object> getSettings() {
        return settings;
    }

    public void setSettings(Map<String, Object> settings) {
        this.settings = settings;
    }
}
