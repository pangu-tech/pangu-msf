package com.fintech.pangu.elasticsearch.autoconfigure;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * ElasticSearch批量处理服务配置属性
 *
 * @author xujunqi
 * @since 1.0.0
 */
@ConfigurationProperties(prefix = "pangu.elasticsearch.bulk-processor")
public class BulkProcessorProperties {
    /**
     * 每添加1000个request，执行一次bulk操作
     * 默认：1000个
     */
    private int bulkActions = 1000;

    /**
     * 每达到5M的请求size时，执行一次bulk操作
     * 默认：5M
     */
    private int bulkSize = 5;

    /**
     * 每5s执行一次bulk操作
     * 默认：5s
     */
    private int flushInterval = 5;

    /**
     * 设置并发请求的数量
     * 默认：1； 表示积累bulk requests和发送bulk是异步的，其数值表示发送bulk的并发线程数，设置为0表示二者同步的
     */
    private int concurrentRequests = 1;

    private BackoffPolicyProperties backoffPolicy = new BackoffPolicyProperties();

    public int getBulkActions() {
        return bulkActions;
    }

    public BulkProcessorProperties setBulkActions(int bulkActions) {
        this.bulkActions = bulkActions;
        return this;
    }

    public int getBulkSize() {
        return bulkSize;
    }

    public BulkProcessorProperties setBulkSize(int bulkSize) {
        this.bulkSize = bulkSize;
        return this;
    }

    public int getFlushInterval() {
        return flushInterval;
    }

    public BulkProcessorProperties setFlushInterval(int flushInterval) {
        this.flushInterval = flushInterval;
        return this;
    }

    public int getConcurrentRequests() {
        return concurrentRequests;
    }

    public BulkProcessorProperties setConcurrentRequests(int concurrentRequests) {
        this.concurrentRequests = concurrentRequests;
        return this;
    }

    public BackoffPolicyProperties getBackoffPolicy() {
        return backoffPolicy;
    }

    public BulkProcessorProperties setBackoffPolicy(BackoffPolicyProperties backoffPolicy) {
        this.backoffPolicy = backoffPolicy;
        return this;
    }

    class BackoffPolicyProperties {
        /**
         * 设置自定义重复请求机制，最开始等待毫秒数，之后成倍更加，重试3次
         * 默认：100毫秒
         */
        private int initialDelayTime = 100;

        /**
         * 重试次数
         * 默认：3次
         */
        private int maxNumberOfRetries = 3;

        public int getInitialDelayTime() {
            return initialDelayTime;
        }

        public BackoffPolicyProperties setInitialDelayTime(int initialDelayTime) {
            this.initialDelayTime = initialDelayTime;
            return this;
        }

        public int getMaxNumberOfRetries() {
            return maxNumberOfRetries;
        }

        public BackoffPolicyProperties setMaxNumberOfRetries(int maxNumberOfRetries) {
            this.maxNumberOfRetries = maxNumberOfRetries;
            return this;
        }
    }
}
