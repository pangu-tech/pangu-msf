package com.fintech.pangu.elasticsearch.autoconfigure;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 自动加载配置elasticsearch
 *
 * @author xujunqi
 * @since 1.0.0
 */
@Configuration
@Import({ElasticsearchConfiguration.class, BulkProcessorConfiguration.class})
public class ElasticsearchAutoConfiguration {

}
