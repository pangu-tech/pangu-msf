package com.fintech.pangu.commons.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.sql.Timestamp;


/**
 * @Description:
 */
@Data
public class PanguBaseEntity {

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Long id;

    /**
     * 租户ID，非空
     */
    @TableField("TENANT_ID")
    private String tenantId;

    /**
     * 有效性状态，1有效，0无效，默认1
     */
    @TableField(value = "VALIDATE_STATE",fill = FieldFill.INSERT)
    private String validateState;

    /**
     * 创建时间
     */
    @TableField(value = "CREATED_TIME",fill = FieldFill.INSERT)
    private Timestamp createdTime;

    /**
     * 更新时间
     */
    @TableField(value = "UPDATED_TIME",fill = FieldFill.INSERT_UPDATE)
    private Timestamp updatedTime;
}