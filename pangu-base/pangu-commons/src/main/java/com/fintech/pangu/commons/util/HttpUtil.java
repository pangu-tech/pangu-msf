package com.fintech.pangu.commons.util;

import com.alibaba.fastjson.JSON;
import com.fintech.pangu.commons.api.BaseResponse;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * @Description:
 */
@Slf4j
public class HttpUtil {


    public static void responseForJson(HttpServletResponse response, BaseResponse baseResponse){
        PrintWriter out = null;
        try {
            response.setStatus(HttpServletResponse.SC_OK);
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json;charset=UTF-8");
            out = response.getWriter();
            out.write(JSON.toJSONString(baseResponse));
            out.flush();
        }catch (Exception e){
            log.error("responseForJson error,message=[{}]",e.getMessage());
        }finally {
            if (out != null){
                out.close();
            }
        }
    }
}