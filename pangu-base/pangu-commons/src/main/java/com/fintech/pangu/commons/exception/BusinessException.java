package com.fintech.pangu.commons.exception;

/**
 * @Description:
 */
public class BusinessException extends BaseException {

    public BusinessException(IResponse iResponse, String message) {
        super(iResponse, message);
    }

    public BusinessException(IResponse iResponse, String message, Throwable cause) {
        super(iResponse, message, cause);
    }
}
