package com.fintech.pangu.commons.api;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * @Description:
 */
public class PageUtil {

    public static IPage builder(List list, IPage page){
        Page pageInfo = new Page();
        pageInfo.setRecords(list);
        pageInfo.setTotal(page.getTotal());
        pageInfo.setCurrent(page.getCurrent());
        pageInfo.setSize(page.getSize());
        return pageInfo;
    }
}
