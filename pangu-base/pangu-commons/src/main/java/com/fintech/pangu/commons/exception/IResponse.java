package com.fintech.pangu.commons.exception;

/**
 * @Description:
 */
public interface IResponse {

    String getCode();

    String getMessage();
}
