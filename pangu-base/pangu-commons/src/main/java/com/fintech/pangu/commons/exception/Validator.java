package com.fintech.pangu.commons.exception;

/**
 * @Description:
 */
public interface Validator {

    BaseException newException(Object... args);

    BaseException newException(Throwable t, Object... args);

    default void checkNotNull(Object obj) {
        if (obj == null) {
            throw newException(obj);
        }
    }

    default void checkNotNull(Object obj, Object... args) {
        if (obj == null) {
            throw newException(args);
        }
    }

    default void throwException(Object... args){
        throw newException(args);
    }
}
