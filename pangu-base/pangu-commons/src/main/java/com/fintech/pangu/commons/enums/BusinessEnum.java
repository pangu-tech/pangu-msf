package com.fintech.pangu.commons.enums;

import com.fintech.pangu.commons.exception.BusinessExceptionValidator;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description:
 */
@Getter
@AllArgsConstructor
public enum  BusinessEnum implements BusinessExceptionValidator {

    SUCCESS("0000","成功"),
    FAILURE("0001","系统异常"),
    SSO_USER_LOGOUT("0100","当前用户已在别处登录，请重新登录！"),
    UN_AUTHORIZED_ERROR("0101","会话失效,请重新登录!"),
    USERNAME_PASSWORD_ERROR("0102","用户名或密码错误"),
    USERINFO_AUTH_ERROR("0103","用户信息认证失败"),
    MODIFY_PASSWORD_ERROR("0104","修改密码失败"),
    GETUSERDETAIL_ERROR("0105","获取用户详情信息异常"),
    VALID_ERROR("0106","参数校验异常")
    ;

    /**
     * 返回码
     */
    private String code;
    /**
     * 返回消息
     */
    private String message;
}
