package com.fintech.pangu.commons.exception;

import java.text.MessageFormat;

/**
 * @Description:
 */
public interface BusinessExceptionValidator extends IResponse, Validator {

    @Override
    default BaseException newException(Object... args) {
        String msg = MessageFormat.format(this.getMessage(), args);

        return new BusinessException(this,msg);
    }

    @Override
    default BaseException newException(Throwable t, Object... args) {
        String msg = MessageFormat.format(this.getMessage(), args);

        return new BusinessException(this, msg, t);
    }
}
