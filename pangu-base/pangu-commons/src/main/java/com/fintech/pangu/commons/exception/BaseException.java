package com.fintech.pangu.commons.exception;

/**
 * @Description:
 */
public class BaseException extends RuntimeException{

    private IResponse iResponse;

    public BaseException(IResponse iResponse, String message) {
        super(message);
        this.iResponse = iResponse;
    }

    public BaseException(IResponse iResponse, String message, Throwable cause) {
        super(message,cause);
        this.iResponse = iResponse;
    }

    public IResponse getiResponse() {
        return iResponse;
    }

    public void setiResponse(IResponse iResponse) {
        this.iResponse = iResponse;
    }
}
