package com.fintech.pangu.commons.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fintech.pangu.commons.enums.BusinessEnum;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @Description:
 */
@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse<T> {

    @ApiModelProperty(value = "返回描述信息",name = "retDesc",required = true)
    private String retDesc;

    @ApiModelProperty(value = "返回状态码(0000-表示成功，其余为失败)",name = "retCode",required = true)
    private String retCode;

    private T responseBody;

    public BaseResponse(){
        this.retCode = BusinessEnum.SUCCESS.getCode();
        this.retDesc = BusinessEnum.SUCCESS.getMessage();
    }

    public BaseResponse(T responseBody){
        this.retCode = BusinessEnum.SUCCESS.getCode();
        this.retDesc = BusinessEnum.SUCCESS.getMessage();
        this.responseBody = responseBody;
    }

    public static BaseResponse success(){
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setRetCode(BusinessEnum.SUCCESS.getCode());
        baseResponse.setRetDesc(BusinessEnum.SUCCESS.getMessage());
        return baseResponse;
    }

    public static BaseResponse failure(){
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setRetCode(BusinessEnum.FAILURE.getCode());
        baseResponse.setRetDesc(BusinessEnum.FAILURE.getMessage());
        return baseResponse;
    }

    public static BaseResponse failure(BusinessEnum businessEnum){
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setRetCode(businessEnum.getCode());
        baseResponse.setRetDesc(businessEnum.getMessage());
        return baseResponse;
    }

    public static BaseResponse failure(String code,String  message){
        BaseResponse baseResponse = new BaseResponse();
        baseResponse.setRetCode(code);
        baseResponse.setRetDesc(message);
        return baseResponse;
    }
}
