package com.fintech.pangu.commons.model;

import lombok.Data;

/**
 * @Description:
 */
@Data
public class UserDetail {

    private Long userId;

    private String username;

    private String userNo;

    private String loginName;

    private Long orgId;

    private String orgName;

    private String mobile;

    private String email;

    private String orgParentId;//父组织ID

    private String sysCode;//系统标识

}
