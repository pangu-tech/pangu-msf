# 简介
1. 增加rocketmq组件集成，包括
    component、autoconfigure、starter、demo
2. 借鉴自孙力提供的springboot-samples及apache的rocket-spring-all
3. 鉴于rocketmq存在两个阶段，引入是3.5.8版本，故命名为ali-rocketmq，区分apache-rocketmq，可以并存两个版本

# 集成说明
## 引入maven dependency
    # 修改为对应引入的版本
    <dependency>
        <groupId>com.fintech.platform</groupId>
        <artifactId>pangu-ali-rocketmq-starter</artifactId>
        <version>${project.version}</version>
    </dependency>
    
## 添加EnableRocketMQ注解
    @EnableRocketMQ
    @SpringBootApplication
    public class AliRocketMQDemoApplication {
        public static void main(String[] args) {
            SpringApplication.run(AliRocketMQDemoApplication.class, args);
        }
    }


## 添加启动公共属性信息
    # 必须填写的属性信息
    pangu.rocketmq.name-server=10.50.180.161:9876;10.50.180.183:9876
    pangu.rocketmq.producer.group=test-demo # 默认生产者配置，名称自定义，需有区分，见名知意
    
    # 选择填写的属性信息
    pangu.rocketmq.enable=false # 自定义是否启动rocketmq，默认为true，可以不添加
    pangu.rocketmq.consumer.enable=false # 自定义是否启动rocketmq 消费者信息，默认为true，可以不添加
    
## 默认创建生产者实例，直接引入即可使用
    @Autowired
    #@Lazy
    private RocketMQTemplate rocketMQTemplate;

## 编写消费者监听实现
    在消费者监听增加注解RocketMQMessageListener
    消费者监听实现接口RocketMQListener，并写明消息的泛型类

    # 字符串类型String
    @RocketMQMessageListener(topic = "test-demo", consumerGroup = "cgid-test-demo")
    public class TestConsumer implements RocketMQListener<String> {
        private final static Logger logger = LoggerFactory.getLogger(TestConsumer.class);
    
        @Override
        public void onMessage(String message) {
            logger.info("TestConsumer获取消息：{}", message);
        }
    }
    
    # 阿里消息类MessageExt
    @RocketMQMessageListener(topic = "test-demo", consumerGroup = "cgid-test-demo")
    public class TestConsumer implements RocketMQListener<MessageExt> {
        private final static Logger logger = LoggerFactory.getLogger(TestConsumer.class);
    
        @Override
        public void onMessage(MessageExt message) {
            logger.info("TestConsumer获取消息：{}", message);
        }
    }

## 其他说明
    RocketMQMessageListener还有些默认参数
        如：
            nameServer：可以填写其他的mq服务发现地址
            enable：可以控制当前消费者是否启动，便于灵活控制