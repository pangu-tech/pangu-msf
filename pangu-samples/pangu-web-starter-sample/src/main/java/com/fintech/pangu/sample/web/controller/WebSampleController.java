package com.fintech.pangu.sample.web.controller;

import com.fintech.pangu.commons.api.BaseResponse;
import com.fintech.pangu.sample.web.dto.SampleDto;
import com.fintech.pangu.sample.web.mapper.WebSampleMapper;
import com.fintech.pangu.sample.web.qo.SampleRequest;
import com.fintech.pangu.sample.web.service.WebSampleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * web组件样例
 *
 * @author xujunqi
 * @since 1.0.0
 */
@Slf4j
@RestController
public class WebSampleController {

    @Autowired
    private WebSampleMapper webSampleMapper;

    @Autowired
    private WebSampleService webSampleService;

    @PostMapping(value = "/api/v1/web/sample")
    public BaseResponse create(@Validated @RequestBody SampleRequest sampleRequest) {
        SampleDto sampleDto = webSampleMapper.fetchReq2Dto(sampleRequest);
        webSampleService.create(sampleDto);
        return new BaseResponse();
    }
}
