package com.fintech.pangu.sample.web.service;

import com.fintech.pangu.sample.web.dto.SampleDto;

/**
 * 样例服务定义接口类
 *
 * @author xujunqi
 * @since 1.0.0
 */
public interface WebSampleService {

    /**
     * 创建一个样例对象
     *
     * @param sampleDto 样例对象
     */
    void create(SampleDto sampleDto);
}
