package com.fintech.pangu.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * web组件使用样例工程 主入口
 *
 * @author xujunqi
 * @since 1.0.0
 */
@SpringBootApplication
public class WebSampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(WebSampleApplication.class, args);
    }
}
