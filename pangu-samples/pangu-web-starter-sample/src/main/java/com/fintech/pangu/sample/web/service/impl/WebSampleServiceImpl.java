package com.fintech.pangu.sample.web.service.impl;

import com.fintech.pangu.sample.web.dto.SampleDto;
import com.fintech.pangu.sample.web.service.WebSampleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * 样例服务实现类
 *
 * @author xujunqi
 * @since 1.0.0
 */
@Slf4j
@Service
public class WebSampleServiceImpl implements WebSampleService {
    @Override
    public void create(SampleDto sampleDto) {
        log.info("接收到业务传输对象：{}", sampleDto);


    }
}
