package com.fintech.pangu.sample.web.qo;

import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class SampleRequest {
    /**
     * 请求编码
     */
    @NotBlank(message = "请求编码不存在")
    private String reqNo;

    /**
     * 姓名
     */
    @NotNull(message = "姓名为空")
    private String name;

    /**
     * 年龄
     */
    @Range(min = 18, max = 60, message = "年龄不合法")
    private Byte age;
}
