package com.fintech.pangu.sample.web.dto;

import lombok.Data;

@Data
public class SampleDto {
    /**
     * 姓名
     */
    private String name;

    /**
     * 年龄
     */
    private Byte age;
}
