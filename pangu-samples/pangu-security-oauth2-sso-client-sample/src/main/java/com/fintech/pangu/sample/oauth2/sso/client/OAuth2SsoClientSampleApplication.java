package com.fintech.pangu.sample.oauth2.sso.client;

import com.fintech.pangu.autoconfigure.security.oauth2.client.EnablePanGuOAuth2SsoClient;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@EnablePanGuOAuth2SsoClient
@SpringBootApplication
public class OAuth2SsoClientSampleApplication {


    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(OAuth2SsoClientSampleApplication.class, args);
    }

}
