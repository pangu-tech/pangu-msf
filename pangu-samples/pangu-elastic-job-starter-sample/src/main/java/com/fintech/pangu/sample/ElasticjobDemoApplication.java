package com.fintech.pangu.sample;

import com.fintech.pangu.elasticjob.annotation.EnableElasticJob;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 定时任务样例演示启动入口
 *
 * @author xujunqi
 * @since 1.0.0
 */
@EnableElasticJob
@SpringBootApplication
public class ElasticjobDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(ElasticjobDemoApplication.class, args);
    }
}
