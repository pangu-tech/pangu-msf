package com.fintech.pangu.sample.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.dataflow.DataflowJob;
import com.fintech.pangu.elasticjob.annotation.ElasticScheduledJob;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xujunqi
 * @since 1.0.0
 */
@Slf4j
@ElasticScheduledJob(cron = "${sample.job.dataflowjobdemo.cron}", disabled = true)
public class DataflowJobDemo implements DataflowJob<String> {

    @Override
    public List<String> fetchData(ShardingContext shardingContext) {
        List<String> dataList = null;

        if (System.currentTimeMillis() % 2 == 0) {
            dataList = new ArrayList<>();
            dataList.add("有数据");
        }

        if (null != dataList && !dataList.isEmpty()) {
            log.warn("暂时无数据，停止本次执行");
        } else {
            log.info("存在顺序流程数据，执行数据处理操作");
        }

        return dataList;
    }

    @Override
    public void processData(ShardingContext shardingContext, List<String> list) {
        list.forEach(str -> log.info("执行任务处理，获取到数据：{}", str));

    }
}
