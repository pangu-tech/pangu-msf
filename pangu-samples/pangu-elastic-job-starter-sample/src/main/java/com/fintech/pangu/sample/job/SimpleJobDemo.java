package com.fintech.pangu.sample.job;

import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.fintech.pangu.elasticjob.annotation.ElasticScheduledJob;
import lombok.extern.slf4j.Slf4j;

/**
 * 样例
 *
 * @author xujunqi
 * @since 1.0.0
 */
@Slf4j
@ElasticScheduledJob(cron = "${sample.job.simplejobdemo.cron}")
public class SimpleJobDemo implements SimpleJob {
    @Override
    public void execute(ShardingContext shardingContext) {
        log.info("执行定时任务, 上下文信息： {}", shardingContext);
    }
}
