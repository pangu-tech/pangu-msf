package com.fintech.pangu.sample.elasticsearch;

import com.fintech.pangu.elasticsearch.annotation.EnableElasticSearch;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 样例主入口
 *
 * @author xujunqi
 * @since 1.0.0
 */
@EnableElasticSearch
@SpringBootApplication
public class ElasticsearchSampleApplication {
    public static void main(String[] args) {
        SpringApplication.run(ElasticsearchSampleApplication.class, args);
    }
}
