package com.fintech.pangu.sample.rocketmq;

import com.alibaba.rocketmq.common.message.Message;
import com.fintech.pangu.sample.rocketmq.producer.ProducerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

/**
 * @author xujunqi
 * @date 2019/9/4 18:02
 * @since 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProducerServiceTest {
    private final static Logger LOGGER = LoggerFactory.getLogger(ProducerServiceTest.class);

    @Autowired
    private ProducerService producerService;

    @Test
    public void testSendString() {
        String message = "hello";
        String topic = "test-demo";
        String tag = "tag-simple-consumer-demo";
        int counter = 0;
        do {
            producerService.send(topic, tag, message);
            counter++;
        } while (counter < 100);
    }

    @Test
    public void testSendMesage() {
        String topic = "test-demo";
        String tag = "tag-message-consumer-demo";
        int counter = 0;
        do {
            Message message = new Message();
            message.setBody("hello".getBytes());
            message.setKeys(UUID.randomUUID().toString());
            producerService.send(topic, tag, message);
            counter++;
        } while (counter < 10);
    }
}
