package com.fintech.pangu.sample.rocketmq.producer;

import com.alibaba.rocketmq.client.producer.SendResult;
import com.alibaba.rocketmq.common.message.Message;
import com.fintech.pangu.rocketmq.core.producer.RocketMQTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * @author xujunqi
 * @date 2019/9/4 17:58
 * @since 1.0.0
 */
@Service
public class ProducerService {
    private final static Logger LOGGER = LoggerFactory.getLogger(ProducerService.class);

    @Autowired
    @Lazy
    private RocketMQTemplate rocketMQTemplate;

    public SendResult send(String topic, String tag, String message) {
        SendResult sendResult = rocketMQTemplate.send(topic, tag, message);
        return sendResult;
    }

    public SendResult send(String topic, String tag, Message message) {
        SendResult sendResult = rocketMQTemplate.send(topic, tag, message);
        return sendResult;
    }

    public SendResult send(String topic, String tag, Object message) {
        SendResult sendResult = rocketMQTemplate.send(topic, tag, message);
        return sendResult;
    }
}
