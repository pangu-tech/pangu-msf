package com.fintech.pangu.sample.rocketmq;

import com.fintech.pangu.rocketmq.annotation.EnableRocketMQ;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author xujunqi
 * @date 2019/9/4 16:42
 * @since 1.0.0
 */
@EnableRocketMQ
@SpringBootApplication
public class AliRocketMQDemoApplication {
    public static void main(String[] args) {
        SpringApplication.run(AliRocketMQDemoApplication.class, args);
    }
}
