package com.fintech.pangu.sample.rocketmq.consumer;

import com.alibaba.rocketmq.common.message.MessageExt;
import com.fintech.pangu.rocketmq.annotation.RocketMQMessageListener;
import com.fintech.pangu.rocketmq.core.consumer.RocketMQListener;
import lombok.extern.slf4j.Slf4j;

/**
 * @author xujunqi
 * @date 2019/9/4 17:56
 * @since 1.0.0
 */
@Slf4j
@RocketMQMessageListener(topic = "test-demo", consumerGroup = "cgid-message-consumer-demo", consumeMessageBatchMaxSize = "20")
public class MessageExtConsumerTest implements RocketMQListener {

    @Override
    public void onMessage(MessageExt message) {
        log.info("SimpleConsumerTest获取消息：{}", message);
    }
}
