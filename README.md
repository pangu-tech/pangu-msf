<p align="center">
    <a href="https://gitee.com/pangu-tech/pangu-msf-docs">
        <img src="https://img.shields.io/badge/link-官方文档-blue.svg?style=flat-square" alt="盘古微服务文档">
    </a>
    <a href="#">
    	<img src="https://img.shields.io/badge/License-Apache%202.0-green.svg" alt="Apache License">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/Maven-3.5.x-green.svg" alt="JDK Version">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/JDK-1.8+-green.svg" alt="JDK Version">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/SpringBoot-2.0.x-green.svg" alt="SpringBoot Version">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/SpringCloud-Finchley.x-green.svg" alt="SpringCloud Version">
    </a>
</p>



盘古微服务框架是基于Vue + SpringBoot + SpringCloud搭建的前后端分离的企业级架构，充分利用 Spring Boot 的开发便利性，简化了分布式系统基础设施搭建的成本，使开发者可以专注于构建业务逻辑，为企业内部IT技术的的升级改造提供便利

- 企业统一门户，多种SSO单点登录方式
- 统一认证 和 基于RBAC、OAuth2的统一权限管理
- 实现基于JWT的无状态认证鉴权 + 基于OAuth2授权码模式的有状态认证鉴权，供不同场景下使用
- 前后端开发规范及相应的代码生成
- 丰富的开箱即用组件，组件自带监控指标，开始使用即可监控
- 企业级多租户解决方案
- 封装抽象Spring Security，实现简单功能可配置，多种认证、鉴权方式可插拔式的自由组合

<br>

## 快速部署体验

本地快速部署体验盘古微服务 [Quick Start](https://gitee.com/pangu-tech/pangu-msf-docs/blob/master/%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97/QuickStart%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97.md)

<br>


## 项目结构

```
. pangu-msf
├── pangu-base
    ├── pangu-boot-autoconfigure      # springboot自动配置
    ├── pangu-commons                 # 公共工具
    ├── pangu-components              # 组件
    ├── pangu-dependencies            # 依赖管理
    ├── pangu-parent                  # 盘古父项目
    └── pangu-starters                # springboot starter场景启动器
├── pangu-services
	├── pangu-authorization-server            # 授权服务器
	├── pangu-gateway                         # 网关
	├── pangu-generator                       # 代码生成后端服务
	├── pangu-grayrelease-sever               # Apollo openapi封装服务
	├── pangu-portal                          # 门户
	├── pangu-service-registry                # 注册中心
	├── pangu-unified-authentication-server   # 统一认证服务
	└── pangu-upms                            # 用户权限管理
└── pangu-samples      # 组件使用参考样例
```

<br>

## 源码方式构建

盘古微服务框架暂时没有发布到Maven Center，如果你想使用最新版本的pangu-msf，可以直接通过源码的方式进行构建安装到本地使用，前提条件如下所示：

- 本地需要安装Git并配置环境变量，[Git下载地址](https://git-scm.com/downloads)

- 本地需要安装Maven并配置环境变量，建议[下载3.5.x及之后版本](https://maven.apache.org/download.cgi)

- 本地需要安装JDK并配置环境变量，[JDK1.8下载地址](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

```
# 下载master分支源码到本地
➜ git clone https://gitee.com/pangu-tech/pangu-msf.git

# 进入pangu-msf源码根目录
➜ cd pangu-msf

# 执行安装
➜ mvn clean install
```

<br>

## 本地部署运行

在通过源码方式构建后，可以尝试在本地IDE中运行盘古微服务，由于盘古微服务提供了很多组件及服务，如下通过不同的使用场景区分为不同的本地部署运行步骤：

- [盘古微服务基于JWT无状态认证本地部署运行指南](https://gitee.com/pangu-tech/pangu-msf-docs/blob/master/%E9%83%A8%E7%BD%B2%E6%8C%87%E5%8D%97/%E5%9F%BA%E4%BA%8EJWT%E7%9A%84%E6%97%A0%E7%8A%B6%E6%80%81%E5%8D%95%E7%82%B9%E7%99%BB%E5%BD%95%E8%AE%A4%E8%AF%81%E6%9C%AC%E5%9C%B0%E9%83%A8%E7%BD%B2%E8%BF%90%E8%A1%8C%E6%8C%87%E5%8D%97.md)

<br>

## 功能演示

### 单点登录

<img src="https://dl-all-test.oss-cn-beijing.aliyuncs.com/pangu/demo-pangu-sso.gif"/> 

<br>

### 代码生成

<img src="https://dl-all-test.oss-cn-beijing.aliyuncs.com/pangu/demo-pangu-code-generate.gif"/> 

<br>

### 多租户

<img src="https://dl-all-test.oss-cn-beijing.aliyuncs.com/pangu/demo-pangu-multi-tenant.gif"/> 





